<?php

use App\Etat;
use Illuminate\Database\Seeder;

class EtatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('etat')->delete();

        $id = [1, 2, 3, 4];
        $etats = ['Neuf', 'Trés bon état', 'Bon état', 'Satisfaisant'];

        for ($i=0; $i <count($id) ; $i++)
        {
            Etat::create(['id' => $id[$i], 'name' => $etats[$i]]);
        }
    }
}
