<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();

        $id = [1, 2, 3, 4, 5, 6, 7];
        $categorie = ['Informatique', 'Image & son', 'Téléphonie', 'Jeux-vidéo', 'Objet connecté', 'Livre', 'Divers'];
        $slug = ['informatique', 'image-&-son', 'telephonie', 'jeux-video', 'objet-connecte', 'livre', 'divers'];

        for ($i=0; $i <count($categorie) ; $i++)
        {
            Category::create(['id' => $id[$i], 'name' => $categorie[$i], 'slug' => $slug[$i]]);
        }
    }
}
