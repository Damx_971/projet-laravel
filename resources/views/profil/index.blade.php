@extends('layouts.base')

@section('title') Mon profil @endsection

@section('content')

<h6 style="text-align: center;">Information générales :</h6>

@if(session()->has('success'))
  <div class="alert alert-success" role="alert">
      {{ session()->get('success') }}
  </div>
@endif
<br>

  @foreach($users as $user)
  <div class="form-group">
    <label>Nom :</label>
    <input type="text" value="{{$user->name}}" class="form-control" disabled>
  </div>

  <div class="form-group">
    <label>Prénom :</label>
    <input type="text" value="{{$user->surname}}" class="form-control" disabled>
  </div>

  <div class="form-group">
    <label>Téléphone :</label>
    <input type="tel" value="{{$user->phone}}" class="form-control" disabled>
  </div>

  
  <div class="form-group">
    <label>Email :</label>
    <input type="email" value="{{$user->email}}" class="form-control" disabled>
  </div>

  <h6 style="text-align: center;">Adresse :</h6>

  <div class="form-group">
    <label>N° et nom de rue :</label>
    <input type="text" value="{{$user->adresse}}" class="form-control" disabled>
  </div>

  <div class="form-group">
    <label>Ville :</label>
    <select class="form-control" disabled>
      <option value="{{$user->city}}">{{$user->ville_name}} {{$user->ville_cp}}</option>
    </select>
  </div>
@endforeach
 <div class="text-center"> 
    <button class="btn btn-primary" data-toggle="modal" data-target="#modal">Modifier mes informations</button>
 </div>

<!-- Modal -->
<div class="modal" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
        
      <!-- Formulaire -->
      <form method="POST" action="{{route('update_profil')}}">
      @csrf
      @method('put')
        <div class="form-group">
          <label>Nom :</label>
          <input type="text" name="name" value="{{$user->name}}" class="form-control" required>
          @error('name')
            <span class="alert alert-danger" role="alert">
                {{ $message }}
            </span>
          @enderror
        </div>

        <div class="form-group">
          <label>Prénom :</label>
          <input type="text" name="surname" value="{{$user->surname}}" class="form-control" required>
          @error('surname')
            <span class="alert alert-danger" role="alert">
                {{ $message }}
            </span>
          @enderror
        </div>

        <div class="form-group">
          <label>Téléphone :</label>
          <input type="text" name="phone" value="{{$user->phone}}" class="form-control" id="phone" required>
          @error('phone')
            <span class="alert alert-danger" role="alert">
                {{ $message }}
            </span>
          @enderror
        </div>
        
        <div class="form-group">
          <label>Email :</label>
          <input type="text" name="email" value="{{$user->email}}" class="form-control" required>
          @error('email')
            <span class="alert alert-danger" role="alert">
                {{ $message }}
            </span>
          @enderror
        </div>

        <h6 style="text-align: center;">Adresse :</h6>

        <div class="form-group">
          <label>N° et nom de rue :</label>
          <input type="text" name="adresse" value="{{$user->adresse}}" class="form-control" required>
          @error('adresse')
            <span class="alert alert-danger" role="alert">
                {{ $message }}
            </span>
          @enderror
        </div>

        <div class="form-group">
          <label>Ville :</label>
          <select name="city" class="form-control">
            <option value="{{$user->city}}" disabled selected hidden>{{$user->ville_name}} {{$user->ville_cp}} </option>
            @foreach($villes as $ville)
            {
              <option value="{{$ville->insee}}">{{$ville->name}} ({{$ville->cp}})</option>
            }
            @endforeach
          </select>
          @error('city')
            <span class="alert alert-danger" role="alert">
                {{ $message }}
            </span>
          @enderror
        </div>

        <div class="text-center"> 
            <button type="submit" class="btn btn-primary">Modifier</button>
        </div>

      </form>
  </div>
</div>

@endsection
