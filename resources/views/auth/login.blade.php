@extends('layouts.base')

@section('title') Connexion @endsection

@section('content')

<form method="POST" action="{{ route('login') }}">
    @csrf
    <h5 style="text-align: center;">Connexion :</h5>

    <div class="form-group ">
        <label for="email">{{ __('Adresse email :') }}</label>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
    </div>

    <div class="form-group row">
        <label for="password">{{ __('Mots de passe :') }}</label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
    </div>

    <div class="text-center">
        <button type="submit" class="btn btn-primary">
            {{ __('Connexion') }}
        </button>
    </div>
</form>
@endsection
