<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etat extends Model
{
    protected $table = "etat";
    protected $fillable = ['id', 'name'];
    public $timestamps = false;

    public function annonce()
    {
        return $this->hasMany('App\Annonce', 'etat_id', 'id');
    }
}
