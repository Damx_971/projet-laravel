<?php

namespace App\Http\Controllers;

use view;
use App\Etat;
use App\Ville;
use App\Annonce;
use App\Category;
use App\Http\Requests\UpdateAnnoncesRequest;
use App\Http\Requests\UpdateProfilRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use SebastianBergmann\Environment\Console;

class ProfilController extends Controller
{
    public function index(){

        $villes = Ville::orderBy('name', 'asc')->get();

        $userId=Auth::id();

        $users = DB::table('users')->join('ville', 'users.city', '=', 'ville.insee')
                                    ->select('users.*', 'ville.name AS ville_name', 'ville.cp AS ville_cp')
                                     ->where('users.id', '=', $userId)
                                     ->get();

        return view('profil/index') ->with('villes', $villes)
                                    ->with('users', $users);    
    }

    public function update(UpdateProfilRequest $request)
    {        
        $params = $request->validated();

        $userId=Auth::id();

        $profil = User::findOrFail($userId);

        $profil->update($params);

        return back()->with('success', 'Votre profil à été modifier');
    }

    public function annonces(){

        $categories = Category::all();
        $etats = Etat::all();
        $villes= Ville::orderBy('insee', 'asc')->get();
        $user=Auth::id();

        $annonces=DB::table('annonces')->join('categories', 'annonces.categorie_id', '=', 'categories.id')
                                        ->join('etat', 'annonces.etat_id', '=', 'etat.id')
                                        ->join('ville', 'annonces.ville_insee', '=', 'ville.insee')
                                        ->select('annonces.*', 'categories.name AS categorie_name', 'categories.slug', 'etat.name AS etat_name', 'ville.name AS ville_name', 'ville.cp AS ville_cp')
                                        ->where('annonces.user_id', '=', $user)
                                        ->get();

        return view('profil/annonces')->with('categories', $categories)
                                    ->with('etats', $etats)
                                    ->with('villes', $villes)
                                    ->with('annonces', $annonces);  
    }


    public function remove_annonce($id)
    {
        $annonces = Annonce::findOrFail($id);
        Storage::delete('public/' . $annonces->photo);
        $annonces->delete();
        return back();
    }

    
    public function update_annonce(UpdateAnnoncesRequest $request, $id)
    {
        $params = $request->validated();

        $annonces = Annonce::findOrFail($id);

        $params['photo'] = isset($params['photo']) ? $params['photo'] : null;

        $isImage = $params['photo'];
        $params['photo'] = $annonces->photo;

        if($isImage !== null){
            Storage::delete('public/' . $annonces->photo);
            Storage::put('public/photo', $isImage);
            $params['photo'] = 'photo/' . $isImage->hashName();
        }

        $annonces->update($params);

        return back()->with('success', 'La modification à bien été éffectuer');
    }

}
