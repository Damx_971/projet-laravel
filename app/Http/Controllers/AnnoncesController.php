<?php

namespace App\Http\Controllers;

use view;
use App\Etat;
use App\Ville;
use App\Annonce;
use App\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreAnnoncesRequest;

class AnnoncesController extends Controller
{
    public function index(){
        $categories = Category::all();
        $etats = Etat::all();
        $villes= Ville::orderBy('name', 'asc')->get();

        return view('annonces/post')->with('categories', $categories)
                                    ->with('etats', $etats)
                                    ->with('villes', $villes);
    }

    public function store(StoreAnnoncesRequest $request){
        $params = $request->validated();

        Storage::put('public/photo', $params['photo']);
        $params['photo'] = 'photo/' . $params['photo']->hashName();
        
        $params['user_id'] = Auth::id();

        Annonce::create($params);
        return back()->with('success', 'Votre annonce été déposer');
    }

    public function categorie($slug){
        $categories = Category::all();
        $etats = Etat::all();
        $villes= Ville::orderBy('insee', 'asc')->get();

        $annonces=DB::table('annonces')->join('categories', 'annonces.categorie_id', '=', 'categories.id')
                                        ->join('etat', 'annonces.etat_id', '=', 'etat.id')
                                        ->join('ville', 'annonces.ville_insee', '=', 'ville.insee')
                                        ->select('annonces.*', 'categories.name AS categorie_name', 'categories.slug', 'etat.name AS etat_name', 'ville.name AS ville_name')
                                        ->where('categories.slug', '=', $slug)
                                        ->get();

        return view('search/index')->with('categories', $categories)
                                    ->with('etats', $etats)
                                    ->with('villes', $villes)
                                    ->with('annonces', $annonces);
    }

    public function details($slug, $id){
        $annonces=DB::table('annonces')->join('categories', 'annonces.categorie_id', '=', 'categories.id')
                                        ->join('etat', 'annonces.etat_id', '=', 'etat.id')
                                        ->join('ville', 'annonces.ville_insee', '=', 'ville.insee')
                                        ->join('users', 'annonces.user_id', '=', 'users.id')
                                        ->select('annonces.*', 'categories.name AS categorie_name', 'etat.name AS etat_name', 'ville.name AS ville_name', 'users.name AS user_name', 'users.surname AS user_surname', 'users.phone AS user_phone', 'users.email AS user_email')
                                        ->where('annonces.id', '=', $id)
                                        ->get();

        return view('annonces/details')->with('annonces', $annonces);
    }
}
