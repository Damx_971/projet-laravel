<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ville extends Model
{
    protected $table = "ville";
    protected $fillable = ['insee', 'name', 'cp', 'departement'];
    public $timestamps = false;

    public function annonce()
    {
        return $this->hasMany('App\Annonce', 'ville_insee', 'insee');
    }
}
