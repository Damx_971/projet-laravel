<?php

namespace App\Http\Controllers;

use App\Etat;
use App\Ville;
use App\Category;
use App\Http\Requests\SearchAnnoncesrequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function search(Request $request){

        $categories = Category::all();
        $etats = Etat::all();
        $villes= Ville::orderBy('name', 'asc')->get();

        $titre = $request->input('search');
        if ($titre==null) {
            $titre="";
        }
        
        $categorie = $request->input('categorie');
        if ($categorie==null) {
            $categorie="annonces.categorie_id";
        }

        $etat = $request->input('etat');
        if ($etat==null) {
            $etat="annonces.etat_id";
        }
        $prix_min = $request->input('prix_min');
        if ($prix_min==null) {
            $prix_min="annonces.prix";
        }
        $prix_max = $request->input('prix_max');
        if ($prix_max==null) {
            $prix_max="annonces.prix";
        }

        $ville = $request->input('ville');
        if ($ville==null) {
            $ville="annonces.ville_insee";
        }

        $annonces=DB::table('annonces')->join('categories', 'annonces.categorie_id', '=', 'categories.id')
                                        ->join('etat', 'annonces.etat_id', '=', 'etat.id')
                                        ->join('ville', 'annonces.ville_insee', '=', 'ville.insee')
                                        ->select('annonces.*', 'categories.name AS categorie_name', 'categories.slug', 'etat.name AS etat_name', 'ville.name AS ville_name')                               
                                        ->where('annonces.titre', 'like', '%'.$titre.'%')
                                        ->whereExists(function ($query) use($titre, $categorie, $etat, $prix_min, $prix_max, $ville){
                                            $query
                                            ->select('annonces.*', 'categories.name AS categorie_name', 'categories.slug', 'etat.name AS etat_name', 'ville.name AS ville_name')
                                            ->whereRaw('annonces.categorie_id = '.$categorie.'')
                                            ->whereExists(function ($query) use($titre, $categorie, $etat, $prix_min, $prix_max, $ville){
                                                $query
                                                ->select('annonces.*', 'categories.name AS categorie_name', 'categories.slug', 'etat.name AS etat_name', 'ville.name AS ville_name')
                                                ->whereRaw('annonces.etat_id = '.$etat.'')
                                                ->whereExists(function ($query) use($titre, $categorie, $etat, $prix_min, $prix_max, $ville){
                                                    $query
                                                    ->select('annonces.*', 'categories.name AS categorie_name', 'categories.slug', 'etat.name AS etat_name', 'ville.name AS ville_name')
                                                    ->whereBetween(DB::raw('annonces.prix'), [DB::raw($prix_min), DB::raw($prix_max)])
                                                    ->whereExists(function ($query) use($titre, $categorie, $etat, $prix_min, $prix_max, $ville){
                                                        $query
                                                        ->select('annonces.*', 'categories.name AS categorie_name', 'categories.slug', 'etat.name AS etat_name', 'ville.name AS ville_name')
                                                        ->whereRaw('annonces.ville_insee = '.$ville.'');
                                                    });
                                                });
                                            });
                                        })
                                        ->get();                                        

        return view('search/index')->with('categories', $categories)
                                    ->with('etats', $etats)
                                    ->with('villes', $villes)
                                    ->with('annonces', $annonces);
    }

    public function departement($departement){

        $categories = Category::all();
        $etats = Etat::all();
        $villes= Ville::orderBy('name', 'asc')->get();
        
        $annonces=DB::table('annonces')->join('categories', 'annonces.categorie_id', '=', 'categories.id')
                                        ->join('etat', 'annonces.etat_id', '=', 'etat.id')
                                        ->join('ville', 'annonces.ville_insee', '=', 'ville.insee')
                                        ->select('annonces.*', 'categories.name AS categorie_name', 'categories.slug', 'etat.name AS etat_name', 'ville.name AS ville_name')
                                        ->where('ville.slug', '=', $departement)
                                        ->get();

        return view('search/index')->with('categories', $categories)
                                    ->with('etats', $etats)
                                    ->with('villes', $villes)
                                    ->with('annonces', $annonces);
    }

}
