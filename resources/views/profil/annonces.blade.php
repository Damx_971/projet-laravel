@extends('layouts.base')

@section('title') Mes annonces @endsection

@section('content')
<h6 class="text-center">Mes annonces</h6>
<br>

@if(session()->has('success'))
  <div class="alert alert-success" role="alert">
      {{ session()->get('success') }}
  </div>
@endif

@if(count($errors) > 0)
    <div id="alert_annonce" class="alert alert-danger" role="alert">
        {{ 'La modification à échouer' }}
    </div>
@endif


<ul class="list-group">
  @foreach($annonces as $annonce)
    <li class="list-group-item">
    <div class="form-row" style="text-align: center;">
        <div class="form-group col-md-12">
            <h6>{{$annonce->titre}}</h6>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-5">
            <img class="rounded float-left w-75 mr-4" src="{{asset('storage/'.$annonce->photo)}}" alt="{{$annonce->titre}}"> 
        </div>
        <div class="form-group col-md-6">
            <h6>{{$annonce->categorie_name}}</h6>
            <h6>{{$annonce->etat_name}}</h6>
            <h6>{{$annonce->prix}} €</h6>
        </div>
        <div class="form-group col-md-1">
            <!-- delete -->
            <form method="POST" action="{{route('remove_annonces', $annonce->id)}}">
            @csrf
            @method('delete')
                <button type="submit" class="btn_delete">
                    <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-trash-fill float-right " fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="color: #007bff;"> 
                        <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                    </svg>
                </button>
            </form>
            <!-- edit -->
            <button class="btn_edit" data-toggle="modal" data-target="#modal{{$annonce->id}}">
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-pencil-fill float-right" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="color: #007bff;">
                    <path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                </svg>
            </button>
        </div>
    </div>

        <!-- Modal -->
        <div class="modal" id="modal{{$annonce->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">

            <!-- Formulaire -->
            <form method="POST" action="{{route('update_annonces', $annonce->id)}}" enctype="multipart/form-data">
            @csrf
            @method('put')
                <div class="form-group">
                    <label for="exampleFormControlInput1">Titre :</label>
                    <input type="text" name="titre" value="{{$annonce->titre}}" class="form-control" id="exampleFormControlInput1" required>
                    @error('titre')
                        <span class="alert alert-danger" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleFormControlSelect1">Catégorie :</label>
                    <select name="categorie_id" class="form-control" id="exampleFormControlSelect1">
                    <option value="{{$annonce->categorie_id}}" disabled selected hidden>{{$annonce->categorie_name}}</option>
                    @foreach($categories as $category)
                    {
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    }
                    @endforeach
                    </select>
                    @error('categorie_id')
                        <span class="alert alert-danger" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleFormControlSelect1">Etat :</label>
                    <select name="etat_id" class="form-control" id="exampleFormControlSelect1">
                    <option value="{{$annonce->etat_id}}" disabled selected hidden>{{$annonce->etat_name}}</option>
                    @foreach($etats as $etat)
                    {
                        <option value="{{$etat->id}}">{{$etat->name}}</option>
                    }
                    @endforeach
                    </select>
                    @error('etat_id')
                        <span class="alert alert-danger" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Description :</label>
                    <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="3" required>{{$annonce->description}}</textarea>
                    @error('description')
                        <span class="alert alert-danger" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Prix :</label>
                    <input type="number" name="prix" value="{{$annonce->prix}}" class="form-control" id="exampleFormControlInput1" required>
                    @error('prix')
                        <span class="alert alert-danger" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleFormControlFile1">Ajouter une nouvelle photo :</label>
                    <input type="file" name="photo" accept="image/*" class="form-control-file" id="exampleFormControlFile1">
                    @error('photo')
                        <span class="alert alert-danger" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleFormControlSelect1">Ville :</label>
                    <select name="ville_insee" class="form-control" id="exampleFormControlSelect1">
                    <option value="{{$annonce->ville_insee}}" disabled selected hidden>{{$annonce->ville_name}} {{$annonce->ville_cp}}</option>
                    @foreach($villes as $ville)
                    {
                        <option value="{{$ville->insee}}">{{$ville->name}} ({{$ville->cp}})</option>
                    }
                    @endforeach
                    </select>
                    @error('ville_insee')
                        <span class="alert alert-danger" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="text-center"> 
                    <button type="submit" class="btn btn-primary">Modifier</button>
                </div>
            </form>
        </div>
        </div>
    </li>
  @endforeach
</ul>


@endsection
