<nav class="navbar navbar-light bg-light">
<!-- logo -->
<a class="navbar-brand" href="{{route('home')}}"><img src="{{asset('icone.png')}}" height="70"></a>
@if(\Illuminate\Support\Facades\Auth::check())
<!-- nouvelle annonce -->
<a class="btn btn-primary" href="{{route('annonce')}}" role="button">Déposer une annonce 
    <svg width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-file-plus-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M12 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM8.5 6a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V10a.5.5 0 0 0 1 0V8.5H10a.5.5 0 0 0 0-1H8.5V6z"/>
    </svg>
</a>
@else
<!-- inscription -->
<a class="btn btn-primary" href="{{route('register')}}" role="button">inscription</a>
@endif
<!-- recherche -->
<form action="{{route('search')}}" method="get">
@csrf
  <div class="active-cyan-3 active-cyan-4 mb-4">
    <input class="form-control" type="text" aria-label="Search" placeholder="Rechercher" name="search">
  </div>
</form>
@if(\Illuminate\Support\Facades\Auth::check())
<!-- mes annonces -->
<a href="{{route('mesannonces')}}">
  <svg width="2.2em" height="2.2em" viewBox="0 0 16 16" class="bi bi-newspaper" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="color: #007bff;">
    <path fill-rule="evenodd" d="M0 2.5A1.5 1.5 0 0 1 1.5 1h11A1.5 1.5 0 0 1 14 2.5v10.528c0 .3-.05.654-.238.972h.738a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 1 1 0v9a1.5 1.5 0 0 1-1.5 1.5H1.497A1.497 1.497 0 0 1 0 13.5v-11zM12 14c.37 0 .654-.211.853-.441.092-.106.147-.279.147-.531V2.5a.5.5 0 0 0-.5-.5h-11a.5.5 0 0 0-.5.5v11c0 .278.223.5.497.5H12z"/>
    <path d="M2 3h10v2H2V3zm0 3h4v3H2V6zm0 4h4v1H2v-1zm0 2h4v1H2v-1zm5-6h2v1H7V6zm3 0h2v1h-2V6zM7 8h2v1H7V8zm3 0h2v1h-2V8zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1z"/>
  </svg>
</a>

<!-- profil -->
<a href="{{route('profil')}}">
  <svg height="2.2em" viewBox="-77 -19 629 629.33435" width="2.2em" class="bi bi-heart" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="color: #007bff;">
  <path d="m233.371094 294.5c81.328125 0 147.25-65.925781 147.25-147.25 0-81.320312-65.921875-147.25-147.25-147.25-81.320313 0-147.25 65.929688-147.25 147.25.0625 81.296875 65.953125 147.1875 147.25 147.25zm0-269.542969c67.542968 0 122.292968 54.75 122.292968 122.292969s-54.75 122.292969-122.292968 122.292969c-67.535156 0-122.292969-54.75-122.292969-122.292969.113281-67.492188 54.800781-122.183594 122.292969-122.292969zm0 0"/>
  <path d="m233.371094 331.3125c-64.011719 0-123.539063 24.832031-167.710938 70.007812-44.800781 45.796876-69.386718 108.3125-69.386718 176.203126.019531 6.882812 5.597656 12.457031 12.480468 12.476562h449.238282c6.886718-.019531 12.457031-5.59375 12.476562-12.476562 0-67.761719-24.582031-130.40625-69.378906-176.078126-44.175782-45.175781-103.699219-70.132812-167.71875-70.132812zm-211.886719 233.730469c2.746094-56.402344 24.582031-107.941407 61.894531-146.128907 39.433594-40.308593 92.71875-62.515624 149.871094-62.515624s110.4375 22.207031 149.867188 62.515624c37.4375 38.1875 59.152343 89.726563 61.898437 146.128907zm0 0"/>
  </svg>
</a>
@endif

@if(\Illuminate\Support\Facades\Auth::check())
<!-- déconnexion -->
<a class="btn btn-primary" href="{{url('/logout')}}" role="button">déconnexion</a>
@else
<!-- connexion -->
<a class="btn btn-primary" href="{{route('login')}}" role="button">connexion</a>
@endif

</nav>

<!-- menu des catégories -->
<div class="btn-group" role="group" aria-label="Basic example">
  <a href="{{route('categorieAnnonce', 'informatique')}}"> <button type="button" class="btn btn-secondary" >Informatique</button></a>
  <a href="{{route('categorieAnnonce', 'image-&-son')}}"> <button type="button" class="btn btn-secondary">Image & son</button></a>
  <a href="{{route('categorieAnnonce', 'telephonie')}}"> <button type="button" class="btn btn-secondary">Téléphonie</button></a>
  <a href="{{route('categorieAnnonce', 'jeux-video')}}"> <button type="button" class="btn btn-secondary">Jeux-vidéo</button></a>
  <a href="{{route('categorieAnnonce', 'objet-connecte')}}"> <button type="button" class="btn btn-secondary">Objet connecté</button></a>
  <a href="{{route('categorieAnnonce', 'livre')}}"> <button type="button" class="btn btn-secondary">Livre</button></a>
  <a href="{{route('categorieAnnonce', 'divers')}}"> <button type="button" class="btn btn-secondary">Divers</button></a>
</div>