<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {return view('welcome');});

Route::get('/', 'HomeController@index')->name('home');

Route::get('/deposer', 'AnnoncesController@index')->name('annonce')->middleware('auth');
Route::post('/deposer', 'AnnoncesController@store')->name('storeAnnonce')->middleware('auth');
Route::get('/annonces/{slug}', 'AnnoncesController@categorie')->name('categorieAnnonce');
Route::get('/annonces/{slug}/{id}', 'AnnoncesController@details')->name('detailsAnnonce');

Route::get('/rechercher', 'SearchController@search')->name('search');
Route::get('/rechercher/{departement}', 'SearchController@departement')->name('search_departement');

Route::get('/profil', 'ProfilController@index')->name('profil')->middleware('auth');
Route::put('/profil', 'ProfilController@update')->name('update_profil');

Route::get('/profil/annonces', 'ProfilController@annonces')->name('mesannonces')->middleware('auth');
Route::put('/profil/annonces/{id}', 'ProfilController@update_annonce')->name('update_annonces');
Route::delete('/profil/annonces/{id}', 'ProfilController@remove_annonce')->name('remove_annonces');


Auth::routes();

Route::get('/logout', function(){
    Auth::logout();
    return redirect()->route('home');
});


