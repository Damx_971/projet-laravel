<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategorieSeeder::class);
        $this->call(EtatSeeder::class);
        $this->call(VilleTableSeeder::class);
        $this->call(AnnoncesTableSeeder::class);
    }
}
