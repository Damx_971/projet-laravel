@extends('layouts.base')

@section('title') Annonce @endsection

@section('content')
  @foreach($annonces as $annonce)
  <div class="card">
    <div class="card-body">
    <img class="card-img-top mb-3" src="{{asset('storage/'.$annonce->photo)}}">
      <h5 class="card-title text-center">{{$annonce->titre}}</h5>
      <h6 class="card-title ">Description :</h6>
      <p> {{$annonce->description}}</p>
      <h6 class="card-title ">Catégorie : {{$annonce->categorie_name}}</h6>
      <h6 class="card-title ">Etat : {{$annonce->etat_name}}</h6>
      <h6 class="card-title ">Ville : {{$annonce->ville_name}}</h6>
      <h5 class="card-title text-center ">{{$annonce->prix}} €</h5>
    </div>
  </div>
  <br>
  <div class="text-center">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Contacter le vendeur</button>
  </div>

  <!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{$annonce->user_name}} {{$annonce->user_surname}}</h5>
      </div>
      <div class="modal-body">

      <p>
        <svg width="1.6em" height="1.6em" viewBox="0 0 16 16" class="bi bi-telephone-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="color: #007bff;">
          <path fill-rule="evenodd" d="M2.267.98a1.636 1.636 0 0 1 2.448.152l1.681 2.162c.309.396.418.913.296 1.4l-.513 2.053a.636.636 0 0 0 .167.604L8.65 9.654a.636.636 0 0 0 .604.167l2.052-.513a1.636 1.636 0 0 1 1.401.296l2.162 1.681c.777.604.849 1.753.153 2.448l-.97.97c-.693.693-1.73.998-2.697.658a17.47 17.47 0 0 1-6.571-4.144A17.47 17.47 0 0 1 .639 4.646c-.34-.967-.035-2.004.658-2.698l.97-.969z"/>
        </svg>
        {{$annonce->user_phone}}
      </p>

      <p>
        <svg width="1.6em" height="1.6em" viewBox="0 0 16 16" class="bi bi-envelope-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="color: #007bff;">
          <path fill-rule="evenodd" d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555zM0 4.697v7.104l5.803-3.558L0 4.697zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757zm3.436-.586L16 11.801V4.697l-5.803 3.546z"/>
        </svg>
        {{$annonce->user_email}}
      </p>

      </div>
      <div class="modal-footer">
        <a href="tel:{{$annonce->user_phone}}" type="button" class="btn btn-secondary">Contacter par téléphone</a>
        <a href="mailto:{{$annonce->user_email}}?subject=NetShop : {{$annonce->titre}}" type="button" class="btn btn-primary">Envoyer un mail</a>
      </div>
    </div>
  </div>
</div>
  @endforeach

@endsection
