<?php

use Illuminate\Database\Seeder;

class VilleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('ville')->delete();
        
        \DB::table('ville')->insert(array (
            0 => 
            array (
                'code_departement' => 75,
                'cp' => 7530,
                'id' => 1,
                'insee' => 7011,
                'name' => 'VALLEES D ANTRAIGUES ASPERJOC',
                'slug' => 'paris',
            ),
            1 => 
            array (
                'code_departement' => 77,
                'cp' => 7700,
                'id' => 2,
                'insee' => 7034,
                'name' => 'BIDON',
                'slug' => 'seine_et_marne',
            ),
            2 => 
            array (
                'code_departement' => 75,
                'cp' => 7590,
                'id' => 3,
                'insee' => 7038,
                'name' => 'BORNE',
                'slug' => 'paris',
            ),
            3 => 
            array (
                'code_departement' => 75,
                'cp' => 7590,
                'id' => 4,
                'insee' => 7047,
                'name' => 'CELLIER DU LUC',
                'slug' => 'paris',
            ),
            4 => 
            array (
                'code_departement' => 75,
                'cp' => 7510,
                'id' => 5,
                'insee' => 7075,
                'name' => 'CROS DE GEORAND',
                'slug' => 'paris',
            ),
            5 => 
            array (
                'code_departement' => 75,
                'cp' => 7500,
                'id' => 6,
                'insee' => 7102,
                'name' => 'GUILHERAND GRANGES',
                'slug' => 'paris',
            ),
            6 => 
            array (
                'code_departement' => 75,
                'cp' => 7520,
                'id' => 7,
                'insee' => 7128,
                'name' => 'LALOUVESC',
                'slug' => 'paris',
            ),
            7 => 
            array (
                'code_departement' => 75,
                'cp' => 7560,
                'id' => 8,
                'insee' => 7200,
                'name' => 'LE ROUX',
                'slug' => 'paris',
            ),
            8 => 
            array (
                'code_departement' => 78,
                'cp' => 7800,
                'id' => 9,
                'insee' => 7221,
                'name' => 'ST CIERGE LA SERRE',
                'slug' => 'yvelines',
            ),
            9 => 
            array (
                'code_departement' => 75,
                'cp' => 7510,
                'id' => 10,
                'insee' => 7224,
                'name' => 'ST CIRGUES EN MONTAGNE',
                'slug' => 'paris',
            ),
            10 => 
            array (
                'code_departement' => 78,
                'cp' => 7800,
                'id' => 11,
                'insee' => 7240,
                'name' => 'ST GEORGES LES BAINS',
                'slug' => 'yvelines',
            ),
            11 => 
            array (
                'code_departement' => 77,
                'cp' => 7700,
                'id' => 12,
                'insee' => 7291,
                'name' => 'ST REMEZE',
                'slug' => 'seine_et_marne',
            ),
            12 => 
            array (
                'code_departement' => 94,
                'cp' => 9460,
                'id' => 13,
                'insee' => 9020,
                'name' => 'ARTIGUES',
                'slug' => 'val_de_marne',
            ),
            13 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 14,
                'insee' => 9022,
                'name' => 'ARVIGNA',
                'slug' => 'essonne',
            ),
            14 => 
            array (
                'code_departement' => 92,
                'cp' => 9220,
                'id' => 15,
                'insee' => 9030,
                'name' => 'AUZAT',
                'slug' => 'hauts_de_seine',
            ),
            15 => 
            array (
                'code_departement' => 93,
                'cp' => 9350,
                'id' => 16,
                'insee' => 9075,
                'name' => 'CAMPAGNE SUR ARIZE',
                'slug' => 'seine_saint_denis',
            ),
            16 => 
            array (
                'code_departement' => 92,
                'cp' => 9250,
                'id' => 17,
                'insee' => 9088,
                'name' => 'CAYCHAX',
                'slug' => 'hauts_de_seine',
            ),
            17 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 18,
                'insee' => 9089,
                'name' => 'CAZALS DES BAYLES',
                'slug' => 'val_doise',
            ),
            18 => 
            array (
                'code_departement' => 92,
                'cp' => 9230,
                'id' => 19,
                'insee' => 9094,
                'name' => 'CERIZOLS',
                'slug' => 'hauts_de_seine',
            ),
            19 => 
            array (
                'code_departement' => 92,
                'cp' => 9230,
                'id' => 20,
                'insee' => 9120,
                'name' => 'FABAS',
                'slug' => 'hauts_de_seine',
            ),
            20 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 21,
                'insee' => 9125,
                'name' => 'FOUGAX ET BARRINEUF',
                'slug' => 'seine_saint_denis',
            ),
            21 => 
            array (
                'code_departement' => 91,
                'cp' => 9110,
                'id' => 22,
                'insee' => 9140,
                'name' => 'IGNAUX',
                'slug' => 'essonne',
            ),
            22 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 23,
                'insee' => 9145,
                'name' => 'LES ISSARDS',
                'slug' => 'essonne',
            ),
            23 => 
            array (
                'code_departement' => 92,
                'cp' => 9200,
                'id' => 24,
                'insee' => 9149,
                'name' => 'LACOURT',
                'slug' => 'hauts_de_seine',
            ),
            24 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 25,
                'insee' => 9150,
                'name' => 'LAGARDE',
                'slug' => 'val_doise',
            ),
            25 => 
            array (
                'code_departement' => 93,
                'cp' => 9310,
                'id' => 26,
                'insee' => 9155,
                'name' => 'LARCAT',
                'slug' => 'seine_saint_denis',
            ),
            26 => 
            array (
                'code_departement' => 93,
                'cp' => 9310,
                'id' => 27,
                'insee' => 9156,
                'name' => 'LARNAT',
                'slug' => 'seine_saint_denis',
            ),
            27 => 
            array (
                'code_departement' => 92,
                'cp' => 9230,
                'id' => 28,
                'insee' => 9158,
                'name' => 'LASSERRE',
                'slug' => 'hauts_de_seine',
            ),
            28 => 
            array (
                'code_departement' => 93,
                'cp' => 9350,
                'id' => 29,
                'insee' => 9172,
                'name' => 'LOUBAUT',
                'slug' => 'seine_saint_denis',
            ),
            29 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 30,
                'insee' => 9173,
                'name' => 'LOUBENS',
                'slug' => 'essonne',
            ),
            30 => 
            array (
                'code_departement' => 92,
                'cp' => 9250,
                'id' => 31,
                'insee' => 9176,
                'name' => 'LUZENAC',
                'slug' => 'hauts_de_seine',
            ),
            31 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 32,
                'insee' => 9177,
                'name' => 'MADIERE',
                'slug' => 'essonne',
            ),
            32 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 33,
                'insee' => 9178,
                'name' => 'MALEGOUDE',
                'slug' => 'val_doise',
            ),
            33 => 
            array (
                'code_departement' => 93,
                'cp' => 9350,
                'id' => 34,
                'insee' => 9186,
                'name' => 'MERAS',
                'slug' => 'seine_saint_denis',
            ),
            34 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 35,
                'insee' => 9192,
                'name' => 'MIGLOS',
                'slug' => 'val_de_marne',
            ),
            35 => 
            array (
                'code_departement' => 91,
                'cp' => 9130,
                'id' => 36,
                'insee' => 9195,
                'name' => 'MONESPLE',
                'slug' => 'essonne',
            ),
            36 => 
            array (
                'code_departement' => 93,
                'cp' => 9350,
                'id' => 37,
                'insee' => 9205,
                'name' => 'MONTFA',
                'slug' => 'seine_saint_denis',
            ),
            37 => 
            array (
                'code_departement' => 93,
                'cp' => 9330,
                'id' => 38,
                'insee' => 9207,
                'name' => 'MONTGAILLARD',
                'slug' => 'seine_saint_denis',
            ),
            38 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 39,
                'insee' => 9211,
                'name' => 'MONTSEGUR',
                'slug' => 'seine_saint_denis',
            ),
            39 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 40,
                'insee' => 9213,
                'name' => 'MOULIN NEUF',
                'slug' => 'val_doise',
            ),
            40 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 41,
                'insee' => 9225,
                'name' => 'PAMIERS',
                'slug' => 'essonne',
            ),
            41 => 
            array (
                'code_departement' => 93,
                'cp' => 9310,
                'id' => 42,
                'insee' => 9226,
                'name' => 'PECH',
                'slug' => 'seine_saint_denis',
            ),
            42 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 43,
                'insee' => 9227,
                'name' => 'PEREILLE',
                'slug' => 'seine_saint_denis',
            ),
            43 => 
            array (
                'code_departement' => 94,
                'cp' => 9460,
                'id' => 44,
                'insee' => 9237,
                'name' => 'LE PUCH',
                'slug' => 'val_de_marne',
            ),
            44 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 45,
                'insee' => 9241,
                'name' => 'RABAT LES TROIS SEIGNEURS',
                'slug' => 'val_de_marne',
            ),
            45 => 
            array (
                'code_departement' => 92,
                'cp' => 9200,
                'id' => 46,
                'insee' => 9247,
                'name' => 'RIVERENERT',
                'slug' => 'hauts_de_seine',
            ),
            46 => 
            array (
                'code_departement' => 93,
                'cp' => 9350,
                'id' => 47,
                'insee' => 9253,
                'name' => 'SABARAT',
                'slug' => 'seine_saint_denis',
            ),
            47 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 48,
                'insee' => 9256,
                'name' => 'ST BAUZEIL',
                'slug' => 'essonne',
            ),
            48 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 49,
                'insee' => 9265,
                'name' => 'ST JEAN DU FALGA',
                'slug' => 'essonne',
            ),
            49 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 50,
                'insee' => 9281,
                'name' => 'SAUTEL',
                'slug' => 'seine_saint_denis',
            ),
            50 => 
            array (
                'code_departement' => 91,
                'cp' => 9110,
                'id' => 51,
                'insee' => 9283,
                'name' => 'SAVIGNAC LES ORMEAUX',
                'slug' => 'essonne',
            ),
            51 => 
            array (
                'code_departement' => 91,
                'cp' => 9110,
                'id' => 52,
                'insee' => 9298,
                'name' => 'SORGEAT',
                'slug' => 'essonne',
            ),
            52 => 
            array (
                'code_departement' => 92,
                'cp' => 9240,
                'id' => 53,
                'insee' => 9304,
                'name' => 'SUZAN',
                'slug' => 'hauts_de_seine',
            ),
            53 => 
            array (
                'code_departement' => 91,
                'cp' => 9160,
                'id' => 54,
                'insee' => 9307,
                'name' => 'TAURIGNAN CASTET',
                'slug' => 'essonne',
            ),
            54 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 55,
                'insee' => 9314,
                'name' => 'TOURTROL',
                'slug' => 'val_doise',
            ),
            55 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 56,
                'insee' => 9323,
                'name' => 'VALS',
                'slug' => 'val_doise',
            ),
            56 => 
            array (
                'code_departement' => 91,
                'cp' => 9130,
                'id' => 57,
                'insee' => 9338,
                'name' => 'VILLENEUVE DU LATOU',
                'slug' => 'essonne',
            ),
            57 => 
            array (
                'code_departement' => 91,
                'cp' => 91670,
                'id' => 58,
                'insee' => 91016,
                'name' => 'ANGERVILLE',
                'slug' => 'essonne',
            ),
            58 => 
            array (
                'code_departement' => 91,
                'cp' => 91610,
                'id' => 59,
                'insee' => 91045,
                'name' => 'BALLANCOURT SUR ESSONNE',
                'slug' => 'essonne',
            ),
            59 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 60,
                'insee' => 91075,
                'name' => 'BOIS HERPIN',
                'slug' => 'essonne',
            ),
            60 => 
            array (
                'code_departement' => 91,
                'cp' => 91070,
                'id' => 61,
                'insee' => 91086,
                'name' => 'BONDOUFLE',
                'slug' => 'essonne',
            ),
            61 => 
            array (
                'code_departement' => 91,
                'cp' => 91650,
                'id' => 62,
                'insee' => 91105,
                'name' => 'BREUILLET',
                'slug' => 'essonne',
            ),
            62 => 
            array (
                'code_departement' => 91,
                'cp' => 91650,
                'id' => 63,
                'insee' => 91106,
                'name' => 'BREUX JOUY',
                'slug' => 'essonne',
            ),
            63 => 
            array (
                'code_departement' => 91,
                'cp' => 91800,
                'id' => 64,
                'insee' => 91114,
                'name' => 'BRUNOY',
                'slug' => 'essonne',
            ),
            64 => 
            array (
                'code_departement' => 91,
                'cp' => 91720,
                'id' => 65,
                'insee' => 91121,
                'name' => 'BUNO BONNEVAUX',
                'slug' => 'essonne',
            ),
            65 => 
            array (
                'code_departement' => 91,
                'cp' => 91440,
                'id' => 66,
                'insee' => 91122,
                'name' => 'BURES SUR YVETTE',
                'slug' => 'essonne',
            ),
            66 => 
            array (
                'code_departement' => 91,
                'cp' => 91560,
                'id' => 67,
                'insee' => 91191,
                'name' => 'CROSNE',
                'slug' => 'essonne',
            ),
            67 => 
            array (
                'code_departement' => 91,
                'cp' => 91080,
                'id' => 68,
                'insee' => 91228,
                'name' => 'EVRY COURCOURONNES',
                'slug' => 'essonne',
            ),
            68 => 
            array (
                'code_departement' => 91,
                'cp' => 91690,
                'id' => 69,
                'insee' => 91240,
                'name' => 'FONTAINE LA RIVIERE',
                'slug' => 'essonne',
            ),
            69 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 70,
                'insee' => 91248,
                'name' => 'LA FORET STE CROIX',
                'slug' => 'essonne',
            ),
            70 => 
            array (
                'code_departement' => 91,
                'cp' => 91190,
                'id' => 71,
                'insee' => 91272,
                'name' => 'GIF SUR YVETTE',
                'slug' => 'essonne',
            ),
            71 => 
            array (
                'code_departement' => 91,
                'cp' => 91720,
                'id' => 72,
                'insee' => 91273,
                'name' => 'GIRONVILLE SUR ESSONNE',
                'slug' => 'essonne',
            ),
            72 => 
            array (
                'code_departement' => 91,
                'cp' => 91400,
                'id' => 73,
                'insee' => 91274,
                'name' => 'GOMETZ LA VILLE',
                'slug' => 'essonne',
            ),
            73 => 
            array (
                'code_departement' => 91,
                'cp' => 91350,
                'id' => 74,
                'insee' => 91286,
                'name' => 'GRIGNY',
                'slug' => 'essonne',
            ),
            74 => 
            array (
                'code_departement' => 91,
                'cp' => 91760,
                'id' => 75,
                'insee' => 91315,
                'name' => 'ITTEVILLE',
                'slug' => 'essonne',
            ),
            75 => 
            array (
                'code_departement' => 91,
                'cp' => 91510,
                'id' => 76,
                'insee' => 91330,
                'name' => 'LARDY',
                'slug' => 'essonne',
            ),
            76 => 
            array (
                'code_departement' => 91,
                'cp' => 91310,
                'id' => 77,
                'insee' => 91333,
                'name' => 'LEUVILLE SUR ORGE',
                'slug' => 'essonne',
            ),
            77 => 
            array (
                'code_departement' => 91,
                'cp' => 91090,
                'id' => 78,
                'insee' => 91340,
                'name' => 'LISSES',
                'slug' => 'essonne',
            ),
            78 => 
            array (
                'code_departement' => 91,
                'cp' => 91630,
                'id' => 79,
                'insee' => 91376,
                'name' => 'MAROLLES EN HUREPOIX',
                'slug' => 'essonne',
            ),
            79 => 
            array (
                'code_departement' => 91,
                'cp' => 91730,
                'id' => 80,
                'insee' => 91378,
                'name' => 'MAUCHAMPS',
                'slug' => 'essonne',
            ),
            80 => 
            array (
                'code_departement' => 91,
                'cp' => 91660,
                'id' => 81,
                'insee' => 91390,
                'name' => 'LE MEREVILLOIS',
                'slug' => 'essonne',
            ),
            81 => 
            array (
                'code_departement' => 91,
                'cp' => 91420,
                'id' => 82,
                'insee' => 91432,
                'name' => 'MORANGIS',
                'slug' => 'essonne',
            ),
            82 => 
            array (
                'code_departement' => 91,
                'cp' => 91490,
                'id' => 83,
                'insee' => 91463,
                'name' => 'ONCY SUR ECOLE',
                'slug' => 'essonne',
            ),
            83 => 
            array (
                'code_departement' => 91,
                'cp' => 91720,
                'id' => 84,
                'insee' => 91507,
                'name' => 'PRUNAY SUR ESSONNE',
                'slug' => 'essonne',
            ),
            84 => 
            array (
                'code_departement' => 91,
                'cp' => 91480,
                'id' => 85,
                'insee' => 91514,
                'name' => 'QUINCY SOUS SENART',
                'slug' => 'essonne',
            ),
            85 => 
            array (
                'code_departement' => 91,
                'cp' => 91410,
                'id' => 86,
                'insee' => 91519,
                'name' => 'RICHARVILLE',
                'slug' => 'essonne',
            ),
            86 => 
            array (
                'code_departement' => 91,
                'cp' => 91130,
                'id' => 87,
                'insee' => 91521,
                'name' => 'RIS ORANGIS',
                'slug' => 'essonne',
            ),
            87 => 
            array (
                'code_departement' => 91,
                'cp' => 91730,
                'id' => 88,
                'insee' => 91619,
                'name' => 'TORFOU',
                'slug' => 'essonne',
            ),
            88 => 
            array (
                'code_departement' => 91,
                'cp' => 91530,
                'id' => 89,
                'insee' => 91630,
                'name' => 'LE VAL ST GERMAIN',
                'slug' => 'essonne',
            ),
            89 => 
            array (
                'code_departement' => 91,
                'cp' => 91480,
                'id' => 90,
                'insee' => 91631,
                'name' => 'VARENNES JARCY',
                'slug' => 'essonne',
            ),
            90 => 
            array (
                'code_departement' => 91,
                'cp' => 91820,
                'id' => 91,
                'insee' => 91639,
                'name' => 'VAYRES SUR ESSONNE',
                'slug' => 'essonne',
            ),
            91 => 
            array (
                'code_departement' => 91,
                'cp' => 91620,
                'id' => 92,
                'insee' => 91665,
                'name' => 'LA VILLE DU BOIS',
                'slug' => 'essonne',
            ),
            92 => 
            array (
                'code_departement' => 91,
                'cp' => 91580,
                'id' => 93,
                'insee' => 91671,
                'name' => 'VILLENEUVE SUR AUVERS',
                'slug' => 'essonne',
            ),
            93 => 
            array (
                'code_departement' => 91,
                'cp' => 91700,
                'id' => 94,
                'insee' => 91685,
                'name' => 'VILLIERS SUR ORGE',
                'slug' => 'essonne',
            ),
            94 => 
            array (
                'code_departement' => 92,
                'cp' => 92270,
                'id' => 95,
                'insee' => 92009,
                'name' => 'BOIS COLOMBES',
                'slug' => 'hauts_de_seine',
            ),
            95 => 
            array (
                'code_departement' => 92,
                'cp' => 92100,
                'id' => 96,
                'insee' => 92012,
                'name' => 'BOULOGNE BILLANCOURT',
                'slug' => 'hauts_de_seine',
            ),
            96 => 
            array (
                'code_departement' => 92,
                'cp' => 92370,
                'id' => 97,
                'insee' => 92022,
                'name' => 'CHAVILLE',
                'slug' => 'hauts_de_seine',
            ),
            97 => 
            array (
                'code_departement' => 92,
                'cp' => 92400,
                'id' => 98,
                'insee' => 92026,
                'name' => 'COURBEVOIE',
                'slug' => 'hauts_de_seine',
            ),
            98 => 
            array (
                'code_departement' => 92,
                'cp' => 92250,
                'id' => 99,
                'insee' => 92035,
                'name' => 'LA GARENNE COLOMBES',
                'slug' => 'hauts_de_seine',
            ),
            99 => 
            array (
                'code_departement' => 92,
                'cp' => 92300,
                'id' => 100,
                'insee' => 92044,
                'name' => 'LEVALLOIS PERRET',
                'slug' => 'hauts_de_seine',
            ),
            100 => 
            array (
                'code_departement' => 92,
                'cp' => 92240,
                'id' => 101,
                'insee' => 92046,
                'name' => 'MALAKOFF',
                'slug' => 'hauts_de_seine',
            ),
            101 => 
            array (
                'code_departement' => 92,
                'cp' => 92500,
                'id' => 102,
                'insee' => 92063,
                'name' => 'RUEIL MALMAISON',
                'slug' => 'hauts_de_seine',
            ),
            102 => 
            array (
                'code_departement' => 92,
                'cp' => 92330,
                'id' => 103,
                'insee' => 92071,
                'name' => 'SCEAUX',
                'slug' => 'hauts_de_seine',
            ),
            103 => 
            array (
                'code_departement' => 92,
                'cp' => 92310,
                'id' => 104,
                'insee' => 92072,
                'name' => 'SEVRES',
                'slug' => 'hauts_de_seine',
            ),
            104 => 
            array (
                'code_departement' => 93,
                'cp' => 93350,
                'id' => 105,
                'insee' => 93013,
                'name' => 'LE BOURGET',
                'slug' => 'seine_saint_denis',
            ),
            105 => 
            array (
                'code_departement' => 93,
                'cp' => 93470,
                'id' => 106,
                'insee' => 93015,
                'name' => 'COUBRON',
                'slug' => 'seine_saint_denis',
            ),
            106 => 
            array (
                'code_departement' => 93,
                'cp' => 93370,
                'id' => 107,
                'insee' => 93047,
                'name' => 'MONTFERMEIL',
                'slug' => 'seine_saint_denis',
            ),
            107 => 
            array (
                'code_departement' => 93,
                'cp' => 93330,
                'id' => 108,
                'insee' => 93050,
                'name' => 'NEUILLY SUR MARNE',
                'slug' => 'seine_saint_denis',
            ),
            108 => 
            array (
                'code_departement' => 93,
                'cp' => 93500,
                'id' => 109,
                'insee' => 93055,
                'name' => 'PANTIN',
                'slug' => 'seine_saint_denis',
            ),
            109 => 
            array (
                'code_departement' => 93,
                'cp' => 93320,
                'id' => 110,
                'insee' => 93057,
                'name' => 'LES PAVILLONS SOUS BOIS',
                'slug' => 'seine_saint_denis',
            ),
            110 => 
            array (
                'code_departement' => 93,
                'cp' => 93410,
                'id' => 111,
                'insee' => 93074,
                'name' => 'VAUJOURS',
                'slug' => 'seine_saint_denis',
            ),
            111 => 
            array (
                'code_departement' => 94,
                'cp' => 94480,
                'id' => 112,
                'insee' => 94001,
                'name' => 'ABLON SUR SEINE',
                'slug' => 'val_de_marne',
            ),
            112 => 
            array (
                'code_departement' => 94,
                'cp' => 94500,
                'id' => 113,
                'insee' => 94017,
                'name' => 'CHAMPIGNY SUR MARNE',
                'slug' => 'val_de_marne',
            ),
            113 => 
            array (
                'code_departement' => 94,
                'cp' => 94430,
                'id' => 114,
                'insee' => 94019,
                'name' => 'CHENNEVIERES SUR MARNE',
                'slug' => 'val_de_marne',
            ),
            114 => 
            array (
                'code_departement' => 94,
                'cp' => 94120,
                'id' => 115,
                'insee' => 94033,
                'name' => 'FONTENAY SOUS BOIS',
                'slug' => 'val_de_marne',
            ),
            115 => 
            array (
                'code_departement' => 94,
                'cp' => 94240,
                'id' => 116,
                'insee' => 94038,
                'name' => 'L HAY LES ROSES',
                'slug' => 'val_de_marne',
            ),
            116 => 
            array (
                'code_departement' => 94,
                'cp' => 94340,
                'id' => 117,
                'insee' => 94042,
                'name' => 'JOINVILLE LE PONT',
                'slug' => 'val_de_marne',
            ),
            117 => 
            array (
                'code_departement' => 94,
                'cp' => 94700,
                'id' => 118,
                'insee' => 94046,
                'name' => 'MAISONS ALFORT',
                'slug' => 'val_de_marne',
            ),
            118 => 
            array (
                'code_departement' => 94,
                'cp' => 94160,
                'id' => 119,
                'insee' => 94067,
                'name' => 'ST MANDE',
                'slug' => 'val_de_marne',
            ),
            119 => 
            array (
                'code_departement' => 94,
                'cp' => 94210,
                'id' => 120,
                'insee' => 94068,
                'name' => 'ST MAUR DES FOSSES',
                'slug' => 'val_de_marne',
            ),
            120 => 
            array (
                'code_departement' => 94,
                'cp' => 94370,
                'id' => 121,
                'insee' => 94071,
                'name' => 'SUCY EN BRIE',
                'slug' => 'val_de_marne',
            ),
            121 => 
            array (
                'code_departement' => 94,
                'cp' => 94190,
                'id' => 122,
                'insee' => 94078,
                'name' => 'VILLENEUVE ST GEORGES',
                'slug' => 'val_de_marne',
            ),
            122 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 123,
                'insee' => 95002,
                'name' => 'ABLEIGES',
                'slug' => 'val_doise',
            ),
            123 => 
            array (
                'code_departement' => 95,
                'cp' => 95510,
                'id' => 124,
                'insee' => 95008,
                'name' => 'AINCOURT',
                'slug' => 'val_doise',
            ),
            124 => 
            array (
                'code_departement' => 95,
                'cp' => 95510,
                'id' => 125,
                'insee' => 95012,
                'name' => 'AMENUCOURT',
                'slug' => 'val_doise',
            ),
            125 => 
            array (
                'code_departement' => 95,
                'cp' => 95810,
                'id' => 126,
                'insee' => 95023,
                'name' => 'ARRONVILLE',
                'slug' => 'val_doise',
            ),
            126 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 127,
                'insee' => 95040,
                'name' => 'AVERNES',
                'slug' => 'val_doise',
            ),
            127 => 
            array (
                'code_departement' => 95,
                'cp' => 95270,
                'id' => 128,
                'insee' => 95055,
                'name' => 'BELLEFONTAINE',
                'slug' => 'val_doise',
            ),
            128 => 
            array (
                'code_departement' => 95,
                'cp' => 95500,
                'id' => 129,
                'insee' => 95088,
                'name' => 'BONNEUIL EN FRANCE',
                'slug' => 'val_doise',
            ),
            129 => 
            array (
                'code_departement' => 95,
                'cp' => 95570,
                'id' => 130,
                'insee' => 95091,
                'name' => 'BOUFFEMONT',
                'slug' => 'val_doise',
            ),
            130 => 
            array (
                'code_departement' => 95,
                'cp' => 95000,
                'id' => 131,
                'insee' => 95127,
                'name' => 'CERGY',
                'slug' => 'val_doise',
            ),
            131 => 
            array (
                'code_departement' => 95,
                'cp' => 95710,
                'id' => 132,
                'insee' => 95150,
                'name' => 'CHAUSSY',
                'slug' => 'val_doise',
            ),
            132 => 
            array (
                'code_departement' => 95,
                'cp' => 95170,
                'id' => 133,
                'insee' => 95197,
                'name' => 'DEUIL LA BARRE',
                'slug' => 'val_doise',
            ),
            133 => 
            array (
                'code_departement' => 95,
                'cp' => 95880,
                'id' => 134,
                'insee' => 95210,
                'name' => 'ENGHIEN LES BAINS',
                'slug' => 'val_doise',
            ),
            134 => 
            array (
                'code_departement' => 95,
                'cp' => 95810,
                'id' => 135,
                'insee' => 95213,
                'name' => 'EPIAIS RHUS',
                'slug' => 'val_doise',
            ),
            135 => 
            array (
                'code_departement' => 95,
                'cp' => 95120,
                'id' => 136,
                'insee' => 95219,
                'name' => 'ERMONT',
                'slug' => 'val_doise',
            ),
            136 => 
            array (
                'code_departement' => 95,
                'cp' => 95190,
                'id' => 137,
                'insee' => 95241,
                'name' => 'FONTENAY EN PARISIS',
                'slug' => 'val_doise',
            ),
            137 => 
            array (
                'code_departement' => 95,
                'cp' => 95740,
                'id' => 138,
                'insee' => 95256,
                'name' => 'FREPILLON',
                'slug' => 'val_doise',
            ),
            138 => 
            array (
                'code_departement' => 95,
                'cp' => 95530,
                'id' => 139,
                'insee' => 95257,
                'name' => 'LA FRETTE SUR SEINE',
                'slug' => 'val_doise',
            ),
            139 => 
            array (
                'code_departement' => 95,
                'cp' => 95690,
                'id' => 140,
                'insee' => 95258,
                'name' => 'FROUVILLE',
                'slug' => 'val_doise',
            ),
            140 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 141,
                'insee' => 95282,
                'name' => 'GOUZANGREZ',
                'slug' => 'val_doise',
            ),
            141 => 
            array (
                'code_departement' => 95,
                'cp' => 95640,
                'id' => 142,
                'insee' => 95303,
                'name' => 'LE HEAULME',
                'slug' => 'val_doise',
            ),
            142 => 
            array (
                'code_departement' => 95,
                'cp' => 95690,
                'id' => 143,
                'insee' => 95304,
                'name' => 'HEDOUVILLE',
                'slug' => 'val_doise',
            ),
            143 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 144,
                'insee' => 95309,
                'name' => 'HODENT',
                'slug' => 'val_doise',
            ),
            144 => 
            array (
                'code_departement' => 95,
                'cp' => 95280,
                'id' => 145,
                'insee' => 95323,
                'name' => 'JOUY LE MOUTIER',
                'slug' => 'val_doise',
            ),
            145 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 146,
                'insee' => 95355,
                'name' => 'MAGNY EN VEXIN',
                'slug' => 'val_doise',
            ),
            146 => 
            array (
                'code_departement' => 95,
                'cp' => 95810,
                'id' => 147,
                'insee' => 95387,
                'name' => 'MENOUVILLE',
                'slug' => 'val_doise',
            ),
            147 => 
            array (
                'code_departement' => 95,
                'cp' => 95640,
                'id' => 148,
                'insee' => 95447,
                'name' => 'NEUILLY EN VEXIN',
                'slug' => 'val_doise',
            ),
            148 => 
            array (
                'code_departement' => 95,
                'cp' => 95620,
                'id' => 149,
                'insee' => 95480,
                'name' => 'PARMAIN',
                'slug' => 'val_doise',
            ),
            149 => 
            array (
                'code_departement' => 95,
                'cp' => 95300,
                'id' => 150,
                'insee' => 95500,
                'name' => 'PONTOISE',
                'slug' => 'val_doise',
            ),
            150 => 
            array (
                'code_departement' => 95,
                'cp' => 95700,
                'id' => 151,
                'insee' => 95527,
                'name' => 'ROISSY EN FRANCE',
                'slug' => 'val_doise',
            ),
            151 => 
            array (
                'code_departement' => 95,
                'cp' => 95340,
                'id' => 152,
                'insee' => 95529,
                'name' => 'RONQUEROLLES',
                'slug' => 'val_doise',
            ),
            152 => 
            array (
                'code_departement' => 95,
                'cp' => 95510,
                'id' => 153,
                'insee' => 95543,
                'name' => 'ST CYR EN ARTHIES',
                'slug' => 'val_doise',
            ),
            153 => 
            array (
                'code_departement' => 95,
                'cp' => 95640,
                'id' => 154,
                'insee' => 95584,
                'name' => 'SANTEUIL',
                'slug' => 'val_doise',
            ),
            154 => 
            array (
                'code_departement' => 95,
                'cp' => 95230,
                'id' => 155,
                'insee' => 95598,
                'name' => 'SOISY SOUS MONTMORENCY',
                'slug' => 'val_doise',
            ),
            155 => 
            array (
                'code_departement' => 95,
                'cp' => 95810,
                'id' => 156,
                'insee' => 95611,
                'name' => 'THEUVILLE',
                'slug' => 'val_doise',
            ),
            156 => 
            array (
                'code_departement' => 95,
                'cp' => 95760,
                'id' => 157,
                'insee' => 95628,
                'name' => 'VALMONDOIS',
                'slug' => 'val_doise',
            ),
            157 => 
            array (
                'code_departement' => 95,
                'cp' => 95500,
                'id' => 158,
                'insee' => 95633,
                'name' => 'VAUDHERLAND',
                'slug' => 'val_doise',
            ),
            158 => 
            array (
                'code_departement' => 95,
                'cp' => 95270,
                'id' => 159,
                'insee' => 95652,
                'name' => 'VIARMES',
                'slug' => 'val_doise',
            ),
            159 => 
            array (
                'code_departement' => 95,
                'cp' => 95510,
                'id' => 160,
                'insee' => 95656,
                'name' => 'VIENNE EN ARTHIES',
                'slug' => 'val_doise',
            ),
            160 => 
            array (
                'code_departement' => 75,
                'cp' => 75002,
                'id' => 161,
                'insee' => 75102,
                'name' => 'PARIS 02',
                'slug' => 'paris',
            ),
            161 => 
            array (
                'code_departement' => 75,
                'cp' => 75003,
                'id' => 162,
                'insee' => 75103,
                'name' => 'PARIS 03',
                'slug' => 'paris',
            ),
            162 => 
            array (
                'code_departement' => 75,
                'cp' => 75008,
                'id' => 163,
                'insee' => 75108,
                'name' => 'PARIS 08',
                'slug' => 'paris',
            ),
            163 => 
            array (
                'code_departement' => 75,
                'cp' => 75014,
                'id' => 164,
                'insee' => 75114,
                'name' => 'PARIS 14',
                'slug' => 'paris',
            ),
            164 => 
            array (
                'code_departement' => 75,
                'cp' => 75016,
                'id' => 165,
                'insee' => 75116,
                'name' => 'PARIS 16',
                'slug' => 'paris',
            ),
            165 => 
            array (
                'code_departement' => 75,
                'cp' => 75017,
                'id' => 166,
                'insee' => 75117,
                'name' => 'PARIS 17',
                'slug' => 'paris',
            ),
            166 => 
            array (
                'code_departement' => 75,
                'cp' => 75019,
                'id' => 167,
                'insee' => 75119,
                'name' => 'PARIS 19',
                'slug' => 'paris',
            ),
            167 => 
            array (
                'code_departement' => 77,
                'cp' => 77410,
                'id' => 168,
                'insee' => 77005,
                'name' => 'ANNET SUR MARNE',
                'slug' => 'seine_et_marne',
            ),
            168 => 
            array (
                'code_departement' => 77,
                'cp' => 77630,
                'id' => 169,
                'insee' => 77006,
                'name' => 'ARBONNE LA FORET',
                'slug' => 'seine_et_marne',
            ),
            169 => 
            array (
                'code_departement' => 77,
                'cp' => 77440,
                'id' => 170,
                'insee' => 77008,
                'name' => 'ARMENTIERES EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            170 => 
            array (
                'code_departement' => 77,
                'cp' => 77720,
                'id' => 171,
                'insee' => 77010,
                'name' => 'AUBEPIERRE OZOUER LE REPOS',
                'slug' => 'seine_et_marne',
            ),
            171 => 
            array (
                'code_departement' => 77,
                'cp' => 77118,
                'id' => 172,
                'insee' => 77019,
                'name' => 'BALLOY',
                'slug' => 'seine_et_marne',
            ),
            172 => 
            array (
                'code_departement' => 77,
                'cp' => 77970,
                'id' => 173,
                'insee' => 77020,
                'name' => 'BANNOST VILLEGAGNON',
                'slug' => 'seine_et_marne',
            ),
            173 => 
            array (
                'code_departement' => 77,
                'cp' => 77630,
                'id' => 174,
                'insee' => 77022,
                'name' => 'BARBIZON',
                'slug' => 'seine_et_marne',
            ),
            174 => 
            array (
                'code_departement' => 77,
                'cp' => 77118,
                'id' => 175,
                'insee' => 77025,
                'name' => 'BAZOCHES LES BRAY',
                'slug' => 'seine_et_marne',
            ),
            175 => 
            array (
                'code_departement' => 77,
                'cp' => 77890,
                'id' => 176,
                'insee' => 77027,
                'name' => 'BEAUMONT DU GATINAIS',
                'slug' => 'seine_et_marne',
            ),
            176 => 
            array (
                'code_departement' => 77,
                'cp' => 77540,
                'id' => 177,
                'insee' => 77031,
                'name' => 'BERNAY VILBERT',
                'slug' => 'seine_et_marne',
            ),
            177 => 
            array (
                'code_departement' => 77,
                'cp' => 77940,
                'id' => 178,
                'insee' => 77035,
                'name' => 'BLENNES',
                'slug' => 'seine_et_marne',
            ),
            178 => 
            array (
                'code_departement' => 77,
                'cp' => 77350,
                'id' => 179,
                'insee' => 77038,
                'name' => 'BOISSETTES',
                'slug' => 'seine_et_marne',
            ),
            179 => 
            array (
                'code_departement' => 77,
                'cp' => 77720,
                'id' => 180,
                'insee' => 77044,
                'name' => 'BOMBON',
                'slug' => 'seine_et_marne',
            ),
            180 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 181,
                'insee' => 77046,
                'name' => 'BOULANCOURT',
                'slug' => 'seine_et_marne',
            ),
            181 => 
            array (
                'code_departement' => 77,
                'cp' => 77470,
                'id' => 182,
                'insee' => 77049,
                'name' => 'BOUTIGNY',
                'slug' => 'seine_et_marne',
            ),
            182 => 
            array (
                'code_departement' => 77,
                'cp' => 77750,
                'id' => 183,
                'insee' => 77057,
                'name' => 'BUSSIERES',
                'slug' => 'seine_et_marne',
            ),
            183 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 184,
                'insee' => 77060,
                'name' => 'BUTHIERS',
                'slug' => 'seine_et_marne',
            ),
            184 => 
            array (
                'code_departement' => 77,
                'cp' => 77400,
                'id' => 185,
                'insee' => 77062,
                'name' => 'CARNETIN',
                'slug' => 'seine_et_marne',
            ),
            185 => 
            array (
                'code_departement' => 77,
                'cp' => 77930,
                'id' => 186,
                'insee' => 77065,
                'name' => 'CELY',
                'slug' => 'seine_et_marne',
            ),
            186 => 
            array (
                'code_departement' => 77,
                'cp' => 77171,
                'id' => 187,
                'insee' => 77072,
                'name' => 'CHALAUTRE LA GRANDE',
                'slug' => 'seine_et_marne',
            ),
            187 => 
            array (
                'code_departement' => 77,
                'cp' => 77160,
                'id' => 188,
                'insee' => 77073,
                'name' => 'CHALAUTRE LA PETITE',
                'slug' => 'seine_et_marne',
            ),
            188 => 
            array (
                'code_departement' => 77,
                'cp' => 77144,
                'id' => 189,
                'insee' => 77075,
                'name' => 'CHALIFERT',
                'slug' => 'seine_et_marne',
            ),
            189 => 
            array (
                'code_departement' => 77,
                'cp' => 77910,
                'id' => 190,
                'insee' => 77077,
                'name' => 'CHAMBRY',
                'slug' => 'seine_et_marne',
            ),
            190 => 
            array (
                'code_departement' => 77,
                'cp' => 77260,
                'id' => 191,
                'insee' => 77078,
                'name' => 'CHAMIGNY',
                'slug' => 'seine_et_marne',
            ),
            191 => 
            array (
                'code_departement' => 77,
                'cp' => 77430,
                'id' => 192,
                'insee' => 77079,
                'name' => 'CHAMPAGNE SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            192 => 
            array (
                'code_departement' => 77,
                'cp' => 77660,
                'id' => 193,
                'insee' => 77084,
                'name' => 'CHANGIS SUR MARNE',
                'slug' => 'seine_et_marne',
            ),
            193 => 
            array (
                'code_departement' => 77,
                'cp' => 77160,
                'id' => 194,
                'insee' => 77090,
                'name' => 'LA CHAPELLE ST SULPICE',
                'slug' => 'seine_et_marne',
            ),
            194 => 
            array (
                'code_departement' => 77,
                'cp' => 77169,
                'id' => 195,
                'insee' => 77106,
                'name' => 'CHAUFFRY',
                'slug' => 'seine_et_marne',
            ),
            195 => 
            array (
                'code_departement' => 77,
                'cp' => 77390,
                'id' => 196,
                'insee' => 77107,
                'name' => 'CHAUMES EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            196 => 
            array (
                'code_departement' => 77,
                'cp' => 77160,
                'id' => 197,
                'insee' => 77109,
                'name' => 'CHENOISE CUCHARMOY',
                'slug' => 'seine_et_marne',
            ),
            197 => 
            array (
                'code_departement' => 77,
                'cp' => 77410,
                'id' => 198,
                'insee' => 77118,
                'name' => 'CLAYE SOUILLY',
                'slug' => 'seine_et_marne',
            ),
            198 => 
            array (
                'code_departement' => 77,
                'cp' => 77370,
                'id' => 200,
                'insee' => 77119,
                'name' => 'CLOS FONTAINE',
                'slug' => 'seine_et_marne',
            ),
            199 => 
            array (
                'code_departement' => 77,
                'cp' => 77450,
                'id' => 201,
                'insee' => 77125,
                'name' => 'CONDE STE LIBIAIRE',
                'slug' => 'seine_et_marne',
            ),
            200 => 
            array (
                'code_departement' => 77,
                'cp' => 77580,
                'id' => 202,
                'insee' => 77130,
                'name' => 'COULOMMES',
                'slug' => 'seine_et_marne',
            ),
            201 => 
            array (
                'code_departement' => 77,
                'cp' => 77560,
                'id' => 203,
                'insee' => 77134,
                'name' => 'COURCHAMP',
                'slug' => 'seine_et_marne',
            ),
            202 => 
            array (
                'code_departement' => 77,
                'cp' => 77390,
                'id' => 204,
                'insee' => 77138,
                'name' => 'COURTOMER',
                'slug' => 'seine_et_marne',
            ),
            203 => 
            array (
                'code_departement' => 77,
                'cp' => 77580,
                'id' => 205,
                'insee' => 77142,
                'name' => 'CRECY LA CHAPELLE',
                'slug' => 'seine_et_marne',
            ),
            204 => 
            array (
                'code_departement' => 77,
                'cp' => 77840,
                'id' => 207,
                'insee' => 77148,
                'name' => 'CROUY SUR OURCQ',
                'slug' => 'seine_et_marne',
            ),
            205 => 
            array (
                'code_departement' => 77,
                'cp' => 77440,
                'id' => 208,
                'insee' => 77157,
                'name' => 'DHUISY',
                'slug' => 'seine_et_marne',
            ),
            206 => 
            array (
                'code_departement' => 77,
                'cp' => 77830,
                'id' => 209,
                'insee' => 77164,
                'name' => 'ECHOUBOULAINS',
                'slug' => 'seine_et_marne',
            ),
            207 => 
            array (
                'code_departement' => 77,
                'cp' => 77820,
                'id' => 210,
                'insee' => 77165,
                'name' => 'LES ECRENNES',
                'slug' => 'seine_et_marne',
            ),
            208 => 
            array (
                'code_departement' => 77,
                'cp' => 77157,
                'id' => 211,
                'insee' => 77174,
                'name' => 'EVERLY',
                'slug' => 'seine_et_marne',
            ),
            209 => 
            array (
                'code_departement' => 77,
                'cp' => 77220,
                'id' => 212,
                'insee' => 77177,
                'name' => 'FAVIERES',
                'slug' => 'seine_et_marne',
            ),
            210 => 
            array (
                'code_departement' => 77,
                'cp' => 77150,
                'id' => 213,
                'insee' => 77180,
                'name' => 'FEROLLES ATTILLY',
                'slug' => 'seine_et_marne',
            ),
            211 => 
            array (
                'code_departement' => 77,
                'cp' => 77164,
                'id' => 214,
                'insee' => 77181,
                'name' => 'FERRIERES EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            212 => 
            array (
                'code_departement' => 77,
                'cp' => 77260,
                'id' => 215,
                'insee' => 77183,
                'name' => 'LA FERTE SOUS JOUARRE',
                'slug' => 'seine_et_marne',
            ),
            213 => 
            array (
                'code_departement' => 77,
                'cp' => 77300,
                'id' => 216,
                'insee' => 77186,
                'name' => 'FONTAINEBLEAU',
                'slug' => 'seine_et_marne',
            ),
            214 => 
            array (
                'code_departement' => 77,
                'cp' => 77480,
                'id' => 217,
                'insee' => 77187,
                'name' => 'FONTAINE FOURCHES',
                'slug' => 'seine_et_marne',
            ),
            215 => 
            array (
                'code_departement' => 77,
                'cp' => 77165,
                'id' => 218,
                'insee' => 77193,
                'name' => 'FORFRY',
                'slug' => 'seine_et_marne',
            ),
            216 => 
            array (
                'code_departement' => 77,
                'cp' => 77410,
                'id' => 219,
                'insee' => 77196,
                'name' => 'FRESNES SUR MARNE',
                'slug' => 'seine_et_marne',
            ),
            217 => 
            array (
                'code_departement' => 77,
                'cp' => 77370,
                'id' => 220,
                'insee' => 77201,
                'name' => 'GASTINS',
                'slug' => 'seine_et_marne',
            ),
            218 => 
            array (
                'code_departement' => 77,
                'cp' => 77120,
                'id' => 221,
                'insee' => 77206,
                'name' => 'GIREMOUTIERS',
                'slug' => 'seine_et_marne',
            ),
            219 => 
            array (
                'code_departement' => 77,
                'cp' => 77720,
                'id' => 222,
                'insee' => 77211,
                'name' => 'GRANDPUITS BAILLY CARROIS',
                'slug' => 'seine_et_marne',
            ),
            220 => 
            array (
                'code_departement' => 77,
                'cp' => 77410,
                'id' => 223,
                'insee' => 77214,
                'name' => 'GRESSY',
                'slug' => 'seine_et_marne',
            ),
            221 => 
            array (
                'code_departement' => 77,
                'cp' => 77220,
                'id' => 224,
                'insee' => 77215,
                'name' => 'GRETZ ARMAINVILLIERS',
                'slug' => 'seine_et_marne',
            ),
            222 => 
            array (
                'code_departement' => 77,
                'cp' => 77166,
                'id' => 225,
                'insee' => 77217,
                'name' => 'GRISY SUISNES',
                'slug' => 'seine_et_marne',
            ),
            223 => 
            array (
                'code_departement' => 77,
                'cp' => 77520,
                'id' => 226,
                'insee' => 77223,
                'name' => 'GURCY LE CHATEL',
                'slug' => 'seine_et_marne',
            ),
            224 => 
            array (
                'code_departement' => 77,
                'cp' => 77890,
                'id' => 227,
                'insee' => 77230,
                'name' => 'ICHY',
                'slug' => 'seine_et_marne',
            ),
            225 => 
            array (
                'code_departement' => 77,
                'cp' => 77480,
                'id' => 228,
                'insee' => 77236,
                'name' => 'JAULNES',
                'slug' => 'seine_et_marne',
            ),
            226 => 
            array (
                'code_departement' => 77,
                'cp' => 77640,
                'id' => 229,
                'insee' => 77238,
                'name' => 'JOUARRE',
                'slug' => 'seine_et_marne',
            ),
            227 => 
            array (
                'code_departement' => 77,
                'cp' => 77230,
                'id' => 230,
                'insee' => 77241,
                'name' => 'JUILLY',
                'slug' => 'seine_et_marne',
            ),
            228 => 
            array (
                'code_departement' => 77,
                'cp' => 77650,
                'id' => 231,
                'insee' => 77242,
                'name' => 'JUTIGNY',
                'slug' => 'seine_et_marne',
            ),
            229 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 232,
                'insee' => 77244,
                'name' => 'LARCHANT',
                'slug' => 'seine_et_marne',
            ),
            230 => 
            array (
                'code_departement' => 77,
                'cp' => 77450,
                'id' => 233,
                'insee' => 77248,
                'name' => 'LESCHES',
                'slug' => 'seine_et_marne',
            ),
            231 => 
            array (
                'code_departement' => 77,
                'cp' => 77550,
                'id' => 234,
                'insee' => 77253,
                'name' => 'LISSY',
                'slug' => 'seine_et_marne',
            ),
            232 => 
            array (
                'code_departement' => 77,
                'cp' => 77440,
                'id' => 235,
                'insee' => 77257,
                'name' => 'LIZY SUR OURCQ',
                'slug' => 'seine_et_marne',
            ),
            233 => 
            array (
                'code_departement' => 77,
                'cp' => 77185,
                'id' => 236,
                'insee' => 77258,
                'name' => 'LOGNES',
                'slug' => 'seine_et_marne',
            ),
            234 => 
            array (
                'code_departement' => 77,
                'cp' => 77710,
                'id' => 237,
                'insee' => 77261,
                'name' => 'LORREZ LE BOCAGE PREAUX',
                'slug' => 'seine_et_marne',
            ),
            235 => 
            array (
                'code_departement' => 77,
                'cp' => 77560,
                'id' => 238,
                'insee' => 77262,
                'name' => 'LOUAN VILLEGRUIS FONTAINE',
                'slug' => 'seine_et_marne',
            ),
            236 => 
            array (
                'code_departement' => 77,
                'cp' => 77540,
                'id' => 239,
                'insee' => 77264,
                'name' => 'LUMIGNY NESLES ORMEAUX',
                'slug' => 'seine_et_marne',
            ),
            237 => 
            array (
                'code_departement' => 77,
                'cp' => 77138,
                'id' => 241,
                'insee' => 77265,
                'name' => 'LUZANCY',
                'slug' => 'seine_et_marne',
            ),
            238 => 
            array (
                'code_departement' => 77,
                'cp' => 77570,
                'id' => 242,
                'insee' => 77271,
                'name' => 'MAISONCELLES EN GATINAIS',
                'slug' => 'seine_et_marne',
            ),
            239 => 
            array (
                'code_departement' => 77,
                'cp' => 77230,
                'id' => 243,
                'insee' => 77273,
                'name' => 'MARCHEMORET',
                'slug' => 'seine_et_marne',
            ),
            240 => 
            array (
                'code_departement' => 77,
                'cp' => 77560,
                'id' => 244,
                'insee' => 77275,
                'name' => 'LES MARETS',
                'slug' => 'seine_et_marne',
            ),
            241 => 
            array (
                'code_departement' => 77,
                'cp' => 77440,
                'id' => 245,
                'insee' => 77280,
                'name' => 'MARY SUR MARNE',
                'slug' => 'seine_et_marne',
            ),
            242 => 
            array (
                'code_departement' => 77,
                'cp' => 77520,
                'id' => 246,
                'insee' => 77286,
                'name' => 'MEIGNEUX',
                'slug' => 'seine_et_marne',
            ),
            243 => 
            array (
                'code_departement' => 77,
                'cp' => 77730,
                'id' => 247,
                'insee' => 77290,
                'name' => 'MERY SUR MARNE',
                'slug' => 'seine_et_marne',
            ),
            244 => 
            array (
                'code_departement' => 77,
                'cp' => 77290,
                'id' => 248,
                'insee' => 77294,
                'name' => 'MITRY MORY',
                'slug' => 'seine_et_marne',
            ),
            245 => 
            array (
                'code_departement' => 77,
                'cp' => 77550,
                'id' => 249,
                'insee' => 77296,
                'name' => 'MOISSY CRAMAYEL',
                'slug' => 'seine_et_marne',
            ),
            246 => 
            array (
                'code_departement' => 77,
                'cp' => 77570,
                'id' => 250,
                'insee' => 77297,
                'name' => 'MONDREVILLE',
                'slug' => 'seine_et_marne',
            ),
            247 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 251,
                'insee' => 77303,
                'name' => 'MONTDAUPHIN',
                'slug' => 'seine_et_marne',
            ),
            248 => 
            array (
                'code_departement' => 77,
                'cp' => 77950,
                'id' => 252,
                'insee' => 77306,
                'name' => 'MONTEREAU SUR LE JARD',
                'slug' => 'seine_et_marne',
            ),
            249 => 
            array (
                'code_departement' => 77,
                'cp' => 77690,
                'id' => 253,
                'insee' => 77312,
                'name' => 'MONTIGNY SUR LOING',
                'slug' => 'seine_et_marne',
            ),
            250 => 
            array (
                'code_departement' => 77,
                'cp' => 77720,
                'id' => 254,
                'insee' => 77317,
                'name' => 'MORMANT',
                'slug' => 'seine_et_marne',
            ),
            251 => 
            array (
                'code_departement' => 77,
                'cp' => 77160,
                'id' => 255,
                'insee' => 77319,
                'name' => 'MORTERY',
                'slug' => 'seine_et_marne',
            ),
            252 => 
            array (
                'code_departement' => 77,
                'cp' => 77120,
                'id' => 256,
                'insee' => 77320,
                'name' => 'MOUROUX',
                'slug' => 'seine_et_marne',
            ),
            253 => 
            array (
                'code_departement' => 77,
                'cp' => 77370,
                'id' => 257,
                'insee' => 77327,
                'name' => 'NANGIS',
                'slug' => 'seine_et_marne',
            ),
            254 => 
            array (
                'code_departement' => 77,
                'cp' => 77730,
                'id' => 258,
                'insee' => 77331,
                'name' => 'NANTEUIL SUR MARNE',
                'slug' => 'seine_et_marne',
            ),
            255 => 
            array (
                'code_departement' => 77,
                'cp' => 77230,
                'id' => 259,
                'insee' => 77332,
                'name' => 'NANTOUILLET',
                'slug' => 'seine_et_marne',
            ),
            256 => 
            array (
                'code_departement' => 77,
                'cp' => 77140,
                'id' => 260,
                'insee' => 77340,
                'name' => 'NONVILLE',
                'slug' => 'seine_et_marne',
            ),
            257 => 
            array (
                'code_departement' => 77,
                'cp' => 77114,
                'id' => 261,
                'insee' => 77341,
                'name' => 'NOYEN SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            258 => 
            array (
                'code_departement' => 77,
                'cp' => 77178,
                'id' => 262,
                'insee' => 77344,
                'name' => 'OISSERY',
                'slug' => 'seine_et_marne',
            ),
            259 => 
            array (
                'code_departement' => 77,
                'cp' => 77167,
                'id' => 263,
                'insee' => 77348,
                'name' => 'ORMESSON',
                'slug' => 'seine_et_marne',
            ),
            260 => 
            array (
                'code_departement' => 77,
                'cp' => 77520,
                'id' => 264,
                'insee' => 77355,
                'name' => 'PAROY',
                'slug' => 'seine_et_marne',
            ),
            261 => 
            array (
                'code_departement' => 77,
                'cp' => 77970,
                'id' => 265,
                'insee' => 77357,
                'name' => 'PECY',
                'slug' => 'seine_et_marne',
            ),
            262 => 
            array (
                'code_departement' => 77,
                'cp' => 77165,
                'id' => 266,
                'insee' => 77366,
                'name' => 'LE PLESSIS L EVEQUE',
                'slug' => 'seine_et_marne',
            ),
            263 => 
            array (
                'code_departement' => 77,
                'cp' => 77470,
                'id' => 267,
                'insee' => 77369,
                'name' => 'POINCY',
                'slug' => 'seine_et_marne',
            ),
            264 => 
            array (
                'code_departement' => 77,
                'cp' => 77400,
                'id' => 268,
                'insee' => 77372,
                'name' => 'POMPONNE',
                'slug' => 'seine_et_marne',
            ),
            265 => 
            array (
                'code_departement' => 77,
                'cp' => 77310,
                'id' => 269,
                'insee' => 77378,
                'name' => 'PRINGY',
                'slug' => 'seine_et_marne',
            ),
            266 => 
            array (
                'code_departement' => 77,
                'cp' => 77860,
                'id' => 270,
                'insee' => 77382,
                'name' => 'QUINCY VOISINS',
                'slug' => 'seine_et_marne',
            ),
            267 => 
            array (
                'code_departement' => 77,
                'cp' => 77510,
                'id' => 271,
                'insee' => 77385,
                'name' => 'REBAIS',
                'slug' => 'seine_et_marne',
            ),
            268 => 
            array (
                'code_departement' => 77,
                'cp' => 77540,
                'id' => 272,
                'insee' => 77393,
                'name' => 'ROZAY EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            269 => 
            array (
                'code_departement' => 77,
                'cp' => 77950,
                'id' => 273,
                'insee' => 77394,
                'name' => 'RUBELLES',
                'slug' => 'seine_et_marne',
            ),
            270 => 
            array (
                'code_departement' => 77,
                'cp' => 77515,
                'id' => 274,
                'insee' => 77400,
                'name' => 'ST AUGUSTIN',
                'slug' => 'seine_et_marne',
            ),
            271 => 
            array (
                'code_departement' => 77,
                'cp' => 77160,
                'id' => 275,
                'insee' => 77403,
                'name' => 'ST BRICE',
                'slug' => 'seine_et_marne',
            ),
            272 => 
            array (
                'code_departement' => 77,
                'cp' => 77470,
                'id' => 276,
                'insee' => 77408,
                'name' => 'ST FIACRE',
                'slug' => 'seine_et_marne',
            ),
            273 => 
            array (
                'code_departement' => 77,
                'cp' => 77660,
                'id' => 277,
                'insee' => 77415,
                'name' => 'ST JEAN LES DEUX JUMEAUX',
                'slug' => 'seine_et_marne',
            ),
            274 => 
            array (
                'code_departement' => 77,
                'cp' => 77370,
                'id' => 278,
                'insee' => 77416,
                'name' => 'ST JUST EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            275 => 
            array (
                'code_departement' => 77,
                'cp' => 77169,
                'id' => 279,
                'insee' => 77436,
                'name' => 'ST SIMEON',
                'slug' => 'seine_et_marne',
            ),
            276 => 
            array (
                'code_departement' => 77,
                'cp' => 77580,
                'id' => 280,
                'insee' => 77443,
                'name' => 'SANCY',
                'slug' => 'seine_et_marne',
            ),
            277 => 
            array (
                'code_departement' => 77,
                'cp' => 77176,
                'id' => 281,
                'insee' => 77445,
                'name' => 'SAVIGNY LE TEMPLE',
                'slug' => 'seine_et_marne',
            ),
            278 => 
            array (
                'code_departement' => 77,
                'cp' => 77700,
                'id' => 282,
                'insee' => 77449,
                'name' => 'SERRIS',
                'slug' => 'seine_et_marne',
            ),
            279 => 
            array (
                'code_departement' => 77,
                'cp' => 77640,
                'id' => 283,
                'insee' => 77451,
                'name' => 'SIGNY SIGNETS',
                'slug' => 'seine_et_marne',
            ),
            280 => 
            array (
                'code_departement' => 77,
                'cp' => 77460,
                'id' => 284,
                'insee' => 77458,
                'name' => 'SOUPPES SUR LOING',
                'slug' => 'seine_et_marne',
            ),
            281 => 
            array (
                'code_departement' => 77,
                'cp' => 77131,
                'id' => 285,
                'insee' => 77469,
                'name' => 'TOUQUIN',
                'slug' => 'seine_et_marne',
            ),
            282 => 
            array (
                'code_departement' => 77,
                'cp' => 77370,
                'id' => 286,
                'insee' => 77481,
                'name' => 'VANVILLE',
                'slug' => 'seine_et_marne',
            ),
            283 => 
            array (
                'code_departement' => 77,
                'cp' => 77910,
                'id' => 287,
                'insee' => 77483,
                'name' => 'VARREDDES',
                'slug' => 'seine_et_marne',
            ),
            284 => 
            array (
                'code_departement' => 77,
                'cp' => 77440,
                'id' => 288,
                'insee' => 77490,
                'name' => 'VENDREST',
                'slug' => 'seine_et_marne',
            ),
            285 => 
            array (
                'code_departement' => 77,
                'cp' => 77670,
                'id' => 289,
                'insee' => 77494,
                'name' => 'VERNOU LA CELLE SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            286 => 
            array (
                'code_departement' => 77,
                'cp' => 77450,
                'id' => 290,
                'insee' => 77498,
                'name' => 'VIGNELY',
                'slug' => 'seine_et_marne',
            ),
            287 => 
            array (
                'code_departement' => 77,
                'cp' => 77710,
                'id' => 291,
                'insee' => 77500,
                'name' => 'VILLEBEON',
                'slug' => 'seine_et_marne',
            ),
            288 => 
            array (
                'code_departement' => 77,
                'cp' => 77710,
                'id' => 292,
                'insee' => 77504,
                'name' => 'VILLEMARECHAL',
                'slug' => 'seine_et_marne',
            ),
            289 => 
            array (
                'code_departement' => 77,
                'cp' => 77480,
                'id' => 293,
                'insee' => 77507,
                'name' => 'VILLENAUXE LA PETITE',
                'slug' => 'seine_et_marne',
            ),
            290 => 
            array (
                'code_departement' => 77,
                'cp' => 77174,
                'id' => 294,
                'insee' => 77510,
                'name' => 'VILLENEUVE ST DENIS',
                'slug' => 'seine_et_marne',
            ),
            291 => 
            array (
                'code_departement' => 77,
                'cp' => 77230,
                'id' => 295,
                'insee' => 77511,
                'name' => 'VILLENEUVE SOUS DAMMARTIN',
                'slug' => 'seine_et_marne',
            ),
            292 => 
            array (
                'code_departement' => 77,
                'cp' => 77130,
                'id' => 296,
                'insee' => 77516,
                'name' => 'VILLE ST JACQUES',
                'slug' => 'seine_et_marne',
            ),
            293 => 
            array (
                'code_departement' => 77,
                'cp' => 77410,
                'id' => 297,
                'insee' => 77517,
                'name' => 'VILLEVAUDE',
                'slug' => 'seine_et_marne',
            ),
            294 => 
            array (
                'code_departement' => 77,
                'cp' => 77114,
                'id' => 299,
                'insee' => 77522,
                'name' => 'VILLIERS SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            295 => 
            array (
                'code_departement' => 77,
                'cp' => 77480,
                'id' => 300,
                'insee' => 77523,
                'name' => 'VILLUIS',
                'slug' => 'seine_et_marne',
            ),
            296 => 
            array (
                'code_departement' => 77,
                'cp' => 77520,
                'id' => 301,
                'insee' => 77524,
                'name' => 'VIMPELLES',
                'slug' => 'seine_et_marne',
            ),
            297 => 
            array (
                'code_departement' => 78,
                'cp' => 78770,
                'id' => 302,
                'insee' => 78013,
                'name' => 'ANDELU',
                'slug' => 'yvelines',
            ),
            298 => 
            array (
                'code_departement' => 78,
                'cp' => 78930,
                'id' => 303,
                'insee' => 78031,
                'name' => 'AUFFREVILLE BRASSEUIL',
                'slug' => 'yvelines',
            ),
            299 => 
            array (
                'code_departement' => 78,
                'cp' => 78550,
                'id' => 304,
                'insee' => 78048,
                'name' => 'BAZAINVILLE',
                'slug' => 'yvelines',
            ),
            300 => 
            array (
                'code_departement' => 78,
                'cp' => 78930,
                'id' => 305,
                'insee' => 78070,
                'name' => 'BOINVILLE EN MANTOIS',
                'slug' => 'yvelines',
            ),
            301 => 
            array (
                'code_departement' => 78,
                'cp' => 78125,
                'id' => 306,
                'insee' => 78077,
                'name' => 'LA BOISSIERE ECOLE',
                'slug' => 'yvelines',
            ),
            302 => 
            array (
                'code_departement' => 78,
                'cp' => 78490,
                'id' => 307,
                'insee' => 78084,
                'name' => 'BOISSY SANS AVOIR',
                'slug' => 'yvelines',
            ),
            303 => 
            array (
                'code_departement' => 78,
                'cp' => 78830,
                'id' => 308,
                'insee' => 78087,
                'name' => 'BONNELLES',
                'slug' => 'yvelines',
            ),
            304 => 
            array (
                'code_departement' => 78,
                'cp' => 78410,
                'id' => 309,
                'insee' => 78090,
                'name' => 'BOUAFLE',
                'slug' => 'yvelines',
            ),
            305 => 
            array (
                'code_departement' => 78,
                'cp' => 78955,
                'id' => 310,
                'insee' => 78123,
                'name' => 'CARRIERES SOUS POISSY',
                'slug' => 'yvelines',
            ),
            306 => 
            array (
                'code_departement' => 78,
                'cp' => 78720,
                'id' => 311,
                'insee' => 78128,
                'name' => 'CERNAY LA VILLE',
                'slug' => 'yvelines',
            ),
            307 => 
            array (
                'code_departement' => 78,
                'cp' => 78240,
                'id' => 312,
                'insee' => 78133,
                'name' => 'CHAMBOURCY',
                'slug' => 'yvelines',
            ),
            308 => 
            array (
                'code_departement' => 78,
                'cp' => 78150,
                'id' => 313,
                'insee' => 78158,
                'name' => 'LE CHESNAY ROCQUENCOURT',
                'slug' => 'yvelines',
            ),
            309 => 
            array (
                'code_departement' => 78,
                'cp' => 78460,
                'id' => 315,
                'insee' => 78162,
                'name' => 'CHOISEL',
                'slug' => 'yvelines',
            ),
            310 => 
            array (
                'code_departement' => 78,
                'cp' => 78310,
                'id' => 316,
                'insee' => 78168,
                'name' => 'COIGNIERES',
                'slug' => 'yvelines',
            ),
            311 => 
            array (
                'code_departement' => 78,
                'cp' => 78111,
                'id' => 317,
                'insee' => 78192,
                'name' => 'DAMMARTIN EN SERVE',
                'slug' => 'yvelines',
            ),
            312 => 
            array (
                'code_departement' => 78,
                'cp' => 78720,
                'id' => 318,
                'insee' => 78193,
                'name' => 'DAMPIERRE EN YVELINES',
                'slug' => 'yvelines',
            ),
            313 => 
            array (
                'code_departement' => 78,
                'cp' => 78740,
                'id' => 319,
                'insee' => 78227,
                'name' => 'EVECQUEMONT',
                'slug' => 'yvelines',
            ),
            314 => 
            array (
                'code_departement' => 78,
                'cp' => 78790,
                'id' => 320,
                'insee' => 78237,
                'name' => 'FLINS NEUVE EGLISE',
                'slug' => 'yvelines',
            ),
            315 => 
            array (
                'code_departement' => 78,
                'cp' => 78410,
                'id' => 321,
                'insee' => 78238,
                'name' => 'FLINS SUR SEINE',
                'slug' => 'yvelines',
            ),
            316 => 
            array (
                'code_departement' => 78,
                'cp' => 78490,
                'id' => 322,
                'insee' => 78264,
                'name' => 'GAMBAISEUIL',
                'slug' => 'yvelines',
            ),
            317 => 
            array (
                'code_departement' => 78,
                'cp' => 78125,
                'id' => 323,
                'insee' => 78269,
                'name' => 'GAZERAN',
                'slug' => 'yvelines',
            ),
            318 => 
            array (
                'code_departement' => 78,
                'cp' => 78770,
                'id' => 324,
                'insee' => 78278,
                'name' => 'GOUPILLIERES',
                'slug' => 'yvelines',
            ),
            319 => 
            array (
                'code_departement' => 78,
                'cp' => 78113,
                'id' => 325,
                'insee' => 78283,
                'name' => 'GRANDCHAMP',
                'slug' => 'yvelines',
            ),
            320 => 
            array (
                'code_departement' => 78,
                'cp' => 78930,
                'id' => 326,
                'insee' => 78291,
                'name' => 'GUERVILLE',
                'slug' => 'yvelines',
            ),
            321 => 
            array (
                'code_departement' => 78,
                'cp' => 78440,
                'id' => 327,
                'insee' => 78317,
                'name' => 'JAMBVILLE',
                'slug' => 'yvelines',
            ),
            322 => 
            array (
                'code_departement' => 78,
                'cp' => 78760,
                'id' => 328,
                'insee' => 78321,
                'name' => 'JOUARS PONTCHARTRAIN',
                'slug' => 'yvelines',
            ),
            323 => 
            array (
                'code_departement' => 78,
                'cp' => 78820,
                'id' => 329,
                'insee' => 78327,
                'name' => 'JUZIERS',
                'slug' => 'yvelines',
            ),
            324 => 
            array (
                'code_departement' => 78,
                'cp' => 78114,
                'id' => 330,
                'insee' => 78356,
                'name' => 'MAGNY LES HAMEAUX',
                'slug' => 'yvelines',
            ),
            325 => 
            array (
                'code_departement' => 78,
                'cp' => 78490,
                'id' => 331,
                'insee' => 78366,
                'name' => 'MAREIL LE GUYON',
                'slug' => 'yvelines',
            ),
            326 => 
            array (
                'code_departement' => 78,
                'cp' => 78580,
                'id' => 332,
                'insee' => 78380,
                'name' => 'MAULE',
                'slug' => 'yvelines',
            ),
            327 => 
            array (
                'code_departement' => 78,
                'cp' => 78550,
                'id' => 333,
                'insee' => 78381,
                'name' => 'MAULETTE',
                'slug' => 'yvelines',
            ),
            328 => 
            array (
                'code_departement' => 78,
                'cp' => 78600,
                'id' => 334,
                'insee' => 78396,
                'name' => 'LE MESNIL LE ROI',
                'slug' => 'yvelines',
            ),
            329 => 
            array (
                'code_departement' => 78,
                'cp' => 78490,
                'id' => 335,
                'insee' => 78398,
                'name' => 'LES MESNULS',
                'slug' => 'yvelines',
            ),
            330 => 
            array (
                'code_departement' => 78,
                'cp' => 78470,
                'id' => 336,
                'insee' => 78406,
                'name' => 'MILON LA CHAPELLE',
                'slug' => 'yvelines',
            ),
            331 => 
            array (
                'code_departement' => 78,
                'cp' => 78250,
                'id' => 337,
                'insee' => 78460,
                'name' => 'OINVILLE SUR MONTCIENT',
                'slug' => 'yvelines',
            ),
            332 => 
            array (
                'code_departement' => 78,
                'cp' => 78910,
                'id' => 338,
                'insee' => 78465,
                'name' => 'ORGERUS',
                'slug' => 'yvelines',
            ),
            333 => 
            array (
                'code_departement' => 78,
                'cp' => 78910,
                'id' => 339,
                'insee' => 78474,
                'name' => 'ORVILLIERS',
                'slug' => 'yvelines',
            ),
            334 => 
            array (
                'code_departement' => 78,
                'cp' => 78300,
                'id' => 340,
                'insee' => 78498,
                'name' => 'POISSY',
                'slug' => 'yvelines',
            ),
            335 => 
            array (
                'code_departement' => 78,
                'cp' => 78590,
                'id' => 341,
                'insee' => 78518,
                'name' => 'RENNEMOULIN',
                'slug' => 'yvelines',
            ),
            336 => 
            array (
                'code_departement' => 78,
                'cp' => 78550,
                'id' => 342,
                'insee' => 78520,
                'name' => 'RICHEBOURG',
                'slug' => 'yvelines',
            ),
            337 => 
            array (
                'code_departement' => 78,
                'cp' => 78210,
                'id' => 343,
                'insee' => 78545,
                'name' => 'ST CYR L ECOLE',
                'slug' => 'yvelines',
            ),
            338 => 
            array (
                'code_departement' => 78,
                'cp' => 78112,
                'id' => 344,
                'insee' => 78551,
                'name' => 'ST GERMAIN EN LAYE',
                'slug' => 'yvelines',
            ),
            339 => 
            array (
                'code_departement' => 78,
                'cp' => 78980,
                'id' => 345,
                'insee' => 78559,
                'name' => 'ST ILLIERS LE BOIS',
                'slug' => 'yvelines',
            ),
            340 => 
            array (
                'code_departement' => 78,
                'cp' => 78610,
                'id' => 346,
                'insee' => 78562,
                'name' => 'ST LEGER EN YVELINES',
                'slug' => 'yvelines',
            ),
            341 => 
            array (
                'code_departement' => 78,
                'cp' => 78520,
                'id' => 347,
                'insee' => 78567,
                'name' => 'ST MARTIN LA GARENNE',
                'slug' => 'yvelines',
            ),
            342 => 
            array (
                'code_departement' => 78,
                'cp' => 78470,
                'id' => 348,
                'insee' => 78575,
                'name' => 'ST REMY LES CHEVREUSE',
                'slug' => 'yvelines',
            ),
            343 => 
            array (
                'code_departement' => 78,
                'cp' => 78120,
                'id' => 349,
                'insee' => 78601,
                'name' => 'SONCHAMP',
                'slug' => 'yvelines',
            ),
            344 => 
            array (
                'code_departement' => 78,
                'cp' => 78740,
                'id' => 350,
                'insee' => 78638,
                'name' => 'VAUX SUR SEINE',
                'slug' => 'yvelines',
            ),
            345 => 
            array (
                'code_departement' => 78,
                'cp' => 78540,
                'id' => 351,
                'insee' => 78643,
                'name' => 'VERNOUILLET',
                'slug' => 'yvelines',
            ),
            346 => 
            array (
                'code_departement' => 78,
                'cp' => 78125,
                'id' => 352,
                'insee' => 78655,
                'name' => 'VIEILLE EGLISE EN YVELINES',
                'slug' => 'yvelines',
            ),
            347 => 
            array (
                'code_departement' => 78,
                'cp' => 78450,
                'id' => 353,
                'insee' => 78674,
                'name' => 'VILLEPREUX',
                'slug' => 'yvelines',
            ),
            348 => 
            array (
                'code_departement' => 78,
                'cp' => 78930,
                'id' => 354,
                'insee' => 78677,
                'name' => 'VILLETTE',
                'slug' => 'yvelines',
            ),
            349 => 
            array (
                'code_departement' => 78,
                'cp' => 78770,
                'id' => 355,
                'insee' => 78681,
                'name' => 'VILLIERS LE MAHIEU',
                'slug' => 'yvelines',
            ),
            350 => 
            array (
                'code_departement' => 75,
                'cp' => 7570,
                'id' => 356,
                'insee' => 7079,
                'name' => 'DESAIGNES',
                'slug' => 'paris',
            ),
            351 => 
            array (
                'code_departement' => 78,
                'cp' => 7800,
                'id' => 357,
                'insee' => 7094,
                'name' => 'GILHAC ET BRUZAC',
                'slug' => 'yvelines',
            ),
            352 => 
            array (
                'code_departement' => 77,
                'cp' => 7700,
                'id' => 358,
                'insee' => 7099,
                'name' => 'GRAS',
                'slug' => 'seine_et_marne',
            ),
            353 => 
            array (
                'code_departement' => 75,
                'cp' => 7520,
                'id' => 359,
                'insee' => 7124,
                'name' => 'LAFARRE',
                'slug' => 'paris',
            ),
            354 => 
            array (
                'code_departement' => 75,
                'cp' => 7530,
                'id' => 360,
                'insee' => 7139,
                'name' => 'LAVIOLLE',
                'slug' => 'paris',
            ),
            355 => 
            array (
                'code_departement' => 75,
                'cp' => 7510,
                'id' => 361,
                'insee' => 7154,
                'name' => 'MAZAN L ABBAYE',
                'slug' => 'paris',
            ),
            356 => 
            array (
                'code_departement' => 75,
                'cp' => 7560,
                'id' => 362,
                'insee' => 7161,
                'name' => 'MONTPEZAT SOUS BAUZON',
                'slug' => 'paris',
            ),
            357 => 
            array (
                'code_departement' => 75,
                'cp' => 7590,
                'id' => 363,
                'insee' => 7175,
                'name' => 'LE PLAGNAL',
                'slug' => 'paris',
            ),
            358 => 
            array (
                'code_departement' => 77,
                'cp' => 7700,
                'id' => 364,
                'insee' => 7259,
                'name' => 'ST JUST D ARDECHE',
                'slug' => 'seine_et_marne',
            ),
            359 => 
            array (
                'code_departement' => 77,
                'cp' => 7700,
                'id' => 365,
                'insee' => 7268,
                'name' => 'ST MARTIN D ARDECHE',
                'slug' => 'seine_et_marne',
            ),
            360 => 
            array (
                'code_departement' => 75,
                'cp' => 7520,
                'id' => 366,
                'insee' => 7285,
                'name' => 'ST PIERRE SUR DOUX',
                'slug' => 'paris',
            ),
            361 => 
            array (
                'code_departement' => 75,
                'cp' => 7510,
                'id' => 367,
                'insee' => 7326,
                'name' => 'USCLADES ET RIEUTORD',
                'slug' => 'paris',
            ),
            362 => 
            array (
                'code_departement' => 78,
                'cp' => 7800,
                'id' => 368,
                'insee' => 7349,
                'name' => 'LA VOULTE SUR RHONE',
                'slug' => 'yvelines',
            ),
            363 => 
            array (
                'code_departement' => 93,
                'cp' => 9310,
                'id' => 369,
                'insee' => 9004,
                'name' => 'ALBIES',
                'slug' => 'seine_saint_denis',
            ),
            364 => 
            array (
                'code_departement' => 92,
                'cp' => 9240,
                'id' => 370,
                'insee' => 9007,
                'name' => 'ALLIERES',
                'slug' => 'hauts_de_seine',
            ),
            365 => 
            array (
                'code_departement' => 92,
                'cp' => 9240,
                'id' => 371,
                'insee' => 9009,
                'name' => 'ALZEN',
                'slug' => 'hauts_de_seine',
            ),
            366 => 
            array (
                'code_departement' => 92,
                'cp' => 9250,
                'id' => 372,
                'insee' => 9012,
                'name' => 'APPY',
                'slug' => 'hauts_de_seine',
            ),
            367 => 
            array (
                'code_departement' => 91,
                'cp' => 9110,
                'id' => 373,
                'insee' => 9023,
                'name' => 'ASCOU',
                'slug' => 'essonne',
            ),
            368 => 
            array (
                'code_departement' => 92,
                'cp' => 9230,
                'id' => 374,
                'insee' => 9033,
                'name' => 'BAGERT',
                'slug' => 'hauts_de_seine',
            ),
            369 => 
            array (
                'code_departement' => 92,
                'cp' => 9230,
                'id' => 375,
                'insee' => 9037,
                'name' => 'BARJAC',
                'slug' => 'hauts_de_seine',
            ),
            370 => 
            array (
                'code_departement' => 93,
                'cp' => 9350,
                'id' => 376,
                'insee' => 9038,
                'name' => 'LA BASTIDE DE BESPLAS',
                'slug' => 'seine_saint_denis',
            ),
            371 => 
            array (
                'code_departement' => 92,
                'cp' => 9240,
                'id' => 377,
                'insee' => 9042,
                'name' => 'LA BASTIDE DE SEROU',
                'slug' => 'hauts_de_seine',
            ),
            372 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 378,
                'insee' => 9047,
                'name' => 'BELESTA',
                'slug' => 'seine_saint_denis',
            ),
            373 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 379,
                'insee' => 9050,
                'name' => 'BENAGUES',
                'slug' => 'essonne',
            ),
            374 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 380,
                'insee' => 9051,
                'name' => 'BENAIX',
                'slug' => 'seine_saint_denis',
            ),
            375 => 
            array (
                'code_departement' => 92,
                'cp' => 9250,
                'id' => 381,
                'insee' => 9053,
                'name' => 'BESTIAC',
                'slug' => 'hauts_de_seine',
            ),
            376 => 
            array (
                'code_departement' => 91,
                'cp' => 9160,
                'id' => 382,
                'insee' => 9054,
                'name' => 'BETCHAT',
                'slug' => 'essonne',
            ),
            377 => 
            array (
                'code_departement' => 93,
                'cp' => 9320,
                'id' => 383,
                'insee' => 9057,
                'name' => 'BIERT',
                'slug' => 'seine_saint_denis',
            ),
            378 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 384,
                'insee' => 9058,
                'name' => 'BOMPAS',
                'slug' => 'val_de_marne',
            ),
            379 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 385,
                'insee' => 9080,
                'name' => 'CARLA DE ROQUEFORT',
                'slug' => 'seine_saint_denis',
            ),
            380 => 
            array (
                'code_departement' => 91,
                'cp' => 9160,
                'id' => 386,
                'insee' => 9091,
                'name' => 'CAZAVET',
                'slug' => 'essonne',
            ),
            381 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 387,
                'insee' => 9102,
                'name' => 'COUTENS',
                'slug' => 'val_doise',
            ),
            382 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 388,
                'insee' => 9103,
                'name' => 'CRAMPAGNA',
                'slug' => 'essonne',
            ),
            383 => 
            array (
                'code_departement' => 93,
                'cp' => 9350,
                'id' => 389,
                'insee' => 9105,
                'name' => 'DAUMAZAN SUR ARIZE',
                'slug' => 'seine_saint_denis',
            ),
            384 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 390,
                'insee' => 9106,
                'name' => 'DREUILHE',
                'slug' => 'seine_saint_denis',
            ),
            385 => 
            array (
                'code_departement' => 91,
                'cp' => 9130,
                'id' => 391,
                'insee' => 9109,
                'name' => 'DURFORT',
                'slug' => 'essonne',
            ),
            386 => 
            array (
                'code_departement' => 92,
                'cp' => 9200,
                'id' => 392,
                'insee' => 9114,
                'name' => 'ERP',
                'slug' => 'hauts_de_seine',
            ),
            387 => 
            array (
                'code_departement' => 93,
                'cp' => 9350,
                'id' => 393,
                'insee' => 9123,
                'name' => 'FORNEX',
                'slug' => 'seine_saint_denis',
            ),
            388 => 
            array (
                'code_departement' => 92,
                'cp' => 9250,
                'id' => 394,
                'insee' => 9131,
                'name' => 'GARANOU',
                'slug' => 'hauts_de_seine',
            ),
            389 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 395,
                'insee' => 9136,
                'name' => 'GOURBIT',
                'slug' => 'val_de_marne',
            ),
            390 => 
            array (
                'code_departement' => 92,
                'cp' => 9220,
                'id' => 396,
                'insee' => 9162,
                'name' => 'LERCOUL',
                'slug' => 'hauts_de_seine',
            ),
            391 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 397,
                'insee' => 9166,
                'name' => 'LEYCHERT',
                'slug' => 'seine_saint_denis',
            ),
            392 => 
            array (
                'code_departement' => 92,
                'cp' => 9210,
                'id' => 398,
                'insee' => 9167,
                'name' => 'LEZAT SUR LEZE',
                'slug' => 'hauts_de_seine',
            ),
            393 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 399,
                'insee' => 9168,
                'name' => 'LIEURAC',
                'slug' => 'seine_saint_denis',
            ),
            394 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 400,
                'insee' => 9179,
                'name' => 'MALLEON',
                'slug' => 'essonne',
            ),
            395 => 
            array (
                'code_departement' => 92,
                'cp' => 9290,
                'id' => 401,
                'insee' => 9181,
                'name' => 'LE MAS D AZIL',
                'slug' => 'hauts_de_seine',
            ),
            396 => 
            array (
                'code_departement' => 93,
                'cp' => 9320,
                'id' => 402,
                'insee' => 9182,
                'name' => 'MASSAT',
                'slug' => 'seine_saint_denis',
            ),
            397 => 
            array (
                'code_departement' => 92,
                'cp' => 9230,
                'id' => 403,
                'insee' => 9190,
                'name' => 'MERIGON',
                'slug' => 'hauts_de_seine',
            ),
            398 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 404,
                'insee' => 9194,
                'name' => 'MIREPOIX',
                'slug' => 'val_doise',
            ),
            399 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 405,
                'insee' => 9202,
                'name' => 'MONTEGUT PLANTAUREL',
                'slug' => 'essonne',
            ),
            400 => 
            array (
                'code_departement' => 92,
                'cp' => 9200,
                'id' => 406,
                'insee' => 9204,
                'name' => 'MONTESQUIEU AVANTES',
                'slug' => 'hauts_de_seine',
            ),
            401 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 407,
                'insee' => 9206,
                'name' => 'MONTFERRIER',
                'slug' => 'seine_saint_denis',
            ),
            402 => 
            array (
                'code_departement' => 92,
                'cp' => 9240,
                'id' => 408,
                'insee' => 9216,
                'name' => 'NESCUS',
                'slug' => 'hauts_de_seine',
            ),
            403 => 
            array (
                'code_departement' => 91,
                'cp' => 9110,
                'id' => 409,
                'insee' => 9218,
                'name' => 'ORGEIX',
                'slug' => 'essonne',
            ),
            404 => 
            array (
                'code_departement' => 91,
                'cp' => 9110,
                'id' => 410,
                'insee' => 9220,
                'name' => 'ORLU',
                'slug' => 'essonne',
            ),
            405 => 
            array (
                'code_departement' => 91,
                'cp' => 9140,
                'id' => 411,
                'insee' => 9223,
                'name' => 'OUST',
                'slug' => 'essonne',
            ),
            406 => 
            array (
                'code_departement' => 91,
                'cp' => 9110,
                'id' => 412,
                'insee' => 9228,
                'name' => 'PERLES ET CASTELET',
                'slug' => 'essonne',
            ),
            407 => 
            array (
                'code_departement' => 91,
                'cp' => 9160,
                'id' => 413,
                'insee' => 9235,
                'name' => 'PRAT BONREPAUX',
                'slug' => 'essonne',
            ),
            408 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 414,
                'insee' => 9240,
                'name' => 'QUIE',
                'slug' => 'val_de_marne',
            ),
            409 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 415,
                'insee' => 9249,
                'name' => 'ROQUEFIXADE',
                'slug' => 'seine_saint_denis',
            ),
            410 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 416,
                'insee' => 9274,
                'name' => 'ST QUENTIN LA TOUR',
                'slug' => 'val_doise',
            ),
            411 => 
            array (
                'code_departement' => 92,
                'cp' => 9210,
                'id' => 417,
                'insee' => 9277,
                'name' => 'ST YBARS',
                'slug' => 'hauts_de_seine',
            ),
            412 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 418,
                'insee' => 9280,
                'name' => 'SAURAT',
                'slug' => 'val_de_marne',
            ),
            413 => 
            array (
                'code_departement' => 93,
                'cp' => 9310,
                'id' => 419,
                'insee' => 9296,
                'name' => 'AULOS SINSAT',
                'slug' => 'seine_saint_denis',
            ),
            414 => 
            array (
                'code_departement' => 93,
                'cp' => 9320,
                'id' => 420,
                'insee' => 9301,
                'name' => 'SOULAN',
                'slug' => 'seine_saint_denis',
            ),
            415 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 421,
                'insee' => 9306,
                'name' => 'TARASCON SUR ARIEGE',
                'slug' => 'val_de_marne',
            ),
            416 => 
            array (
                'code_departement' => 91,
                'cp' => 9140,
                'id' => 422,
                'insee' => 9322,
                'name' => 'USTOU',
                'slug' => 'essonne',
            ),
            417 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 423,
                'insee' => 9327,
                'name' => 'VENTENAC',
                'slug' => 'essonne',
            ),
            418 => 
            array (
                'code_departement' => 93,
                'cp' => 9310,
                'id' => 424,
                'insee' => 9328,
                'name' => 'VERDUN',
                'slug' => 'seine_saint_denis',
            ),
            419 => 
            array (
                'code_departement' => 93,
                'cp' => 9340,
                'id' => 425,
                'insee' => 9332,
                'name' => 'VERNIOLLE',
                'slug' => 'seine_saint_denis',
            ),
            420 => 
            array (
                'code_departement' => 92,
                'cp' => 9220,
                'id' => 426,
                'insee' => 9334,
                'name' => 'VAL DE SOS',
                'slug' => 'hauts_de_seine',
            ),
            421 => 
            array (
                'code_departement' => 91,
                'cp' => 9130,
                'id' => 428,
                'insee' => 9342,
                'name' => 'STE SUZANNE',
                'slug' => 'essonne',
            ),
            422 => 
            array (
                'code_departement' => 91,
                'cp' => 91690,
                'id' => 429,
                'insee' => 91022,
                'name' => 'ARRANCOURT',
                'slug' => 'essonne',
            ),
            423 => 
            array (
                'code_departement' => 91,
                'cp' => 91870,
                'id' => 430,
                'insee' => 91081,
                'name' => 'BOISSY LE SEC',
                'slug' => 'essonne',
            ),
            424 => 
            array (
                'code_departement' => 91,
                'cp' => 91790,
                'id' => 431,
                'insee' => 91085,
                'name' => 'BOISSY SOUS ST YON',
                'slug' => 'essonne',
            ),
            425 => 
            array (
                'code_departement' => 91,
                'cp' => 91850,
                'id' => 432,
                'insee' => 91095,
                'name' => 'BOURAY SUR JUINE',
                'slug' => 'essonne',
            ),
            426 => 
            array (
                'code_departement' => 91,
                'cp' => 91730,
                'id' => 433,
                'insee' => 91132,
                'name' => 'CHAMARANDE',
                'slug' => 'essonne',
            ),
            427 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 434,
                'insee' => 91137,
                'name' => 'CHAMPMOTTEUX',
                'slug' => 'essonne',
            ),
            428 => 
            array (
                'code_departement' => 91,
                'cp' => 91410,
                'id' => 435,
                'insee' => 91145,
                'name' => 'CHATIGNONVILLE',
                'slug' => 'essonne',
            ),
            429 => 
            array (
                'code_departement' => 91,
                'cp' => 91630,
                'id' => 436,
                'insee' => 91156,
                'name' => 'CHEPTAINVILLE',
                'slug' => 'essonne',
            ),
            430 => 
            array (
                'code_departement' => 91,
                'cp' => 91750,
                'id' => 437,
                'insee' => 91159,
                'name' => 'CHEVANNES',
                'slug' => 'essonne',
            ),
            431 => 
            array (
                'code_departement' => 91,
                'cp' => 91100,
                'id' => 438,
                'insee' => 91174,
                'name' => 'CORBEIL ESSONNES',
                'slug' => 'essonne',
            ),
            432 => 
            array (
                'code_departement' => 91,
                'cp' => 91410,
                'id' => 439,
                'insee' => 91175,
                'name' => 'CORBREUSE',
                'slug' => 'essonne',
            ),
            433 => 
            array (
                'code_departement' => 91,
                'cp' => 91860,
                'id' => 440,
                'insee' => 91215,
                'name' => 'EPINAY SOUS SENART',
                'slug' => 'essonne',
            ),
            434 => 
            array (
                'code_departement' => 91,
                'cp' => 91000,
                'id' => 441,
                'insee' => 91228,
                'name' => 'EVRY COURCOURONNES',
                'slug' => 'essonne',
            ),
            435 => 
            array (
                'code_departement' => 91,
                'cp' => 91260,
                'id' => 442,
                'insee' => 91326,
                'name' => 'JUVISY SUR ORGE',
                'slug' => 'essonne',
            ),
            436 => 
            array (
                'code_departement' => 91,
                'cp' => 91470,
                'id' => 443,
                'insee' => 91338,
                'name' => 'LIMOURS',
                'slug' => 'essonne',
            ),
            437 => 
            array (
                'code_departement' => 91,
                'cp' => 91310,
                'id' => 444,
                'insee' => 91347,
                'name' => 'LONGPONT SUR ORGE',
                'slug' => 'essonne',
            ),
            438 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 445,
                'insee' => 91374,
                'name' => 'MAROLLES EN BEAUCE',
                'slug' => 'essonne',
            ),
            439 => 
            array (
                'code_departement' => 91,
                'cp' => 91470,
                'id' => 446,
                'insee' => 91411,
                'name' => 'LES MOLIERES',
                'slug' => 'essonne',
            ),
            440 => 
            array (
                'code_departement' => 91,
                'cp' => 91930,
                'id' => 447,
                'insee' => 91414,
                'name' => 'MONNERVILLE',
                'slug' => 'essonne',
            ),
            441 => 
            array (
                'code_departement' => 91,
                'cp' => 91290,
                'id' => 448,
                'insee' => 91457,
                'name' => 'LA NORVILLE',
                'slug' => 'essonne',
            ),
            442 => 
            array (
                'code_departement' => 91,
                'cp' => 91620,
                'id' => 449,
                'insee' => 91458,
                'name' => 'NOZAY',
                'slug' => 'essonne',
            ),
            443 => 
            array (
                'code_departement' => 91,
                'cp' => 91540,
                'id' => 450,
                'insee' => 91468,
                'name' => 'ORMOY',
                'slug' => 'essonne',
            ),
            444 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 451,
                'insee' => 91469,
                'name' => 'ORMOY LA RIVIERE',
                'slug' => 'essonne',
            ),
            445 => 
            array (
                'code_departement' => 91,
                'cp' => 91120,
                'id' => 452,
                'insee' => 91477,
                'name' => 'PALAISEAU',
                'slug' => 'essonne',
            ),
            446 => 
            array (
                'code_departement' => 91,
                'cp' => 91220,
                'id' => 453,
                'insee' => 91494,
                'name' => 'LE PLESSIS PATE',
                'slug' => 'essonne',
            ),
            447 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 454,
                'insee' => 91526,
                'name' => 'ROINVILLIERS',
                'slug' => 'essonne',
            ),
            448 => 
            array (
                'code_departement' => 91,
                'cp' => 91700,
                'id' => 455,
                'insee' => 91549,
                'name' => 'STE GENEVIEVE DES BOIS',
                'slug' => 'essonne',
            ),
            449 => 
            array (
                'code_departement' => 91,
                'cp' => 91780,
                'id' => 456,
                'insee' => 91556,
                'name' => 'ST HILAIRE',
                'slug' => 'essonne',
            ),
            450 => 
            array (
                'code_departement' => 91,
                'cp' => 91940,
                'id' => 457,
                'insee' => 91560,
                'name' => 'ST JEAN DE BEAUREGARD',
                'slug' => 'essonne',
            ),
            451 => 
            array (
                'code_departement' => 91,
                'cp' => 91240,
                'id' => 458,
                'insee' => 91570,
                'name' => 'ST MICHEL SUR ORGE',
                'slug' => 'essonne',
            ),
            452 => 
            array (
                'code_departement' => 91,
                'cp' => 91840,
                'id' => 459,
                'insee' => 91599,
                'name' => 'SOISY SUR ECOLE',
                'slug' => 'essonne',
            ),
            453 => 
            array (
                'code_departement' => 91,
                'cp' => 91270,
                'id' => 460,
                'insee' => 91657,
                'name' => 'VIGNEUX SUR SEINE',
                'slug' => 'essonne',
            ),
            454 => 
            array (
                'code_departement' => 91,
                'cp' => 91140,
                'id' => 461,
                'insee' => 91666,
                'name' => 'VILLEJUST',
                'slug' => 'essonne',
            ),
            455 => 
            array (
                'code_departement' => 91,
                'cp' => 91320,
                'id' => 462,
                'insee' => 91689,
                'name' => 'WISSOUS',
                'slug' => 'essonne',
            ),
            456 => 
            array (
                'code_departement' => 92,
                'cp' => 92160,
                'id' => 463,
                'insee' => 92002,
                'name' => 'ANTONY',
                'slug' => 'hauts_de_seine',
            ),
            457 => 
            array (
                'code_departement' => 92,
                'cp' => 92600,
                'id' => 464,
                'insee' => 92004,
                'name' => 'ASNIERES SUR SEINE',
                'slug' => 'hauts_de_seine',
            ),
            458 => 
            array (
                'code_departement' => 92,
                'cp' => 92340,
                'id' => 465,
                'insee' => 92014,
                'name' => 'BOURG LA REINE',
                'slug' => 'hauts_de_seine',
            ),
            459 => 
            array (
                'code_departement' => 92,
                'cp' => 92700,
                'id' => 466,
                'insee' => 92025,
                'name' => 'COLOMBES',
                'slug' => 'hauts_de_seine',
            ),
            460 => 
            array (
                'code_departement' => 92,
                'cp' => 92260,
                'id' => 467,
                'insee' => 92032,
                'name' => 'FONTENAY AUX ROSES',
                'slug' => 'hauts_de_seine',
            ),
            461 => 
            array (
                'code_departement' => 92,
                'cp' => 92380,
                'id' => 468,
                'insee' => 92033,
                'name' => 'GARCHES',
                'slug' => 'hauts_de_seine',
            ),
            462 => 
            array (
                'code_departement' => 92,
                'cp' => 92800,
                'id' => 469,
                'insee' => 92062,
                'name' => 'PUTEAUX',
                'slug' => 'hauts_de_seine',
            ),
            463 => 
            array (
                'code_departement' => 92,
                'cp' => 92210,
                'id' => 470,
                'insee' => 92064,
                'name' => 'ST CLOUD',
                'slug' => 'hauts_de_seine',
            ),
            464 => 
            array (
                'code_departement' => 92,
                'cp' => 92150,
                'id' => 471,
                'insee' => 92073,
                'name' => 'SURESNES',
                'slug' => 'hauts_de_seine',
            ),
            465 => 
            array (
                'code_departement' => 93,
                'cp' => 93150,
                'id' => 472,
                'insee' => 93007,
                'name' => 'LE BLANC MESNIL',
                'slug' => 'seine_saint_denis',
            ),
            466 => 
            array (
                'code_departement' => 93,
                'cp' => 93390,
                'id' => 473,
                'insee' => 93014,
                'name' => 'CLICHY SOUS BOIS',
                'slug' => 'seine_saint_denis',
            ),
            467 => 
            array (
                'code_departement' => 93,
                'cp' => 93120,
                'id' => 474,
                'insee' => 93027,
                'name' => 'LA COURNEUVE',
                'slug' => 'seine_saint_denis',
            ),
            468 => 
            array (
                'code_departement' => 93,
                'cp' => 93220,
                'id' => 475,
                'insee' => 93032,
                'name' => 'GAGNY',
                'slug' => 'seine_saint_denis',
            ),
            469 => 
            array (
                'code_departement' => 93,
                'cp' => 93460,
                'id' => 476,
                'insee' => 93033,
                'name' => 'GOURNAY SUR MARNE',
                'slug' => 'seine_saint_denis',
            ),
            470 => 
            array (
                'code_departement' => 93,
                'cp' => 93130,
                'id' => 477,
                'insee' => 93053,
                'name' => 'NOISY LE SEC',
                'slug' => 'seine_saint_denis',
            ),
            471 => 
            array (
                'code_departement' => 93,
                'cp' => 93310,
                'id' => 478,
                'insee' => 93061,
                'name' => 'LE PRE ST GERVAIS',
                'slug' => 'seine_saint_denis',
            ),
            472 => 
            array (
                'code_departement' => 93,
                'cp' => 93210,
                'id' => 479,
                'insee' => 93066,
                'name' => 'ST DENIS',
                'slug' => 'seine_saint_denis',
            ),
            473 => 
            array (
                'code_departement' => 94,
                'cp' => 94550,
                'id' => 480,
                'insee' => 94021,
                'name' => 'CHEVILLY LARUE',
                'slug' => 'val_de_marne',
            ),
            474 => 
            array (
                'code_departement' => 94,
                'cp' => 94200,
                'id' => 481,
                'insee' => 94041,
                'name' => 'IVRY SUR SEINE',
                'slug' => 'val_de_marne',
            ),
            475 => 
            array (
                'code_departement' => 94,
                'cp' => 94520,
                'id' => 482,
                'insee' => 94047,
                'name' => 'MANDRES LES ROSES',
                'slug' => 'val_de_marne',
            ),
            476 => 
            array (
                'code_departement' => 94,
                'cp' => 94130,
                'id' => 483,
                'insee' => 94052,
                'name' => 'NOGENT SUR MARNE',
                'slug' => 'val_de_marne',
            ),
            477 => 
            array (
                'code_departement' => 94,
                'cp' => 94440,
                'id' => 484,
                'insee' => 94070,
                'name' => 'SANTENY',
                'slug' => 'val_de_marne',
            ),
            478 => 
            array (
                'code_departement' => 94,
                'cp' => 94320,
                'id' => 486,
                'insee' => 94073,
                'name' => 'THIAIS',
                'slug' => 'val_de_marne',
            ),
            479 => 
            array (
                'code_departement' => 94,
                'cp' => 94800,
                'id' => 487,
                'insee' => 94076,
                'name' => 'VILLEJUIF',
                'slug' => 'val_de_marne',
            ),
            480 => 
            array (
                'code_departement' => 95,
                'cp' => 95580,
                'id' => 488,
                'insee' => 95014,
                'name' => 'ANDILLY',
                'slug' => 'val_doise',
            ),
            481 => 
            array (
                'code_departement' => 95,
                'cp' => 95430,
                'id' => 489,
                'insee' => 95039,
                'name' => 'AUVERS SUR OISE',
                'slug' => 'val_doise',
            ),
            482 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 491,
                'insee' => 95046,
                'name' => 'BANTHELU',
                'slug' => 'val_doise',
            ),
            483 => 
            array (
                'code_departement' => 95,
                'cp' => 95270,
                'id' => 492,
                'insee' => 95056,
                'name' => 'BELLOY EN FRANCE',
                'slug' => 'val_doise',
            ),
            484 => 
            array (
                'code_departement' => 95,
                'cp' => 95810,
                'id' => 493,
                'insee' => 95059,
                'name' => 'BERVILLE',
                'slug' => 'val_doise',
            ),
            485 => 
            array (
                'code_departement' => 95,
                'cp' => 95870,
                'id' => 494,
                'insee' => 95063,
                'name' => 'BEZONS',
                'slug' => 'val_doise',
            ),
            486 => 
            array (
                'code_departement' => 95,
                'cp' => 95000,
                'id' => 495,
                'insee' => 95074,
                'name' => 'BOISEMONT',
                'slug' => 'val_doise',
            ),
            487 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 496,
                'insee' => 95141,
                'name' => 'CHARMONT',
                'slug' => 'val_doise',
            ),
            488 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 497,
                'insee' => 95170,
                'name' => 'CONDECOURT',
                'slug' => 'val_doise',
            ),
            489 => 
            array (
                'code_departement' => 95,
                'cp' => 95240,
                'id' => 498,
                'insee' => 95176,
                'name' => 'CORMEILLES EN PARISIS',
                'slug' => 'val_doise',
            ),
            490 => 
            array (
                'code_departement' => 95,
                'cp' => 95330,
                'id' => 499,
                'insee' => 95199,
                'name' => 'DOMONT',
                'slug' => 'val_doise',
            ),
            491 => 
            array (
                'code_departement' => 95,
                'cp' => 95440,
                'id' => 500,
                'insee' => 95205,
                'name' => 'ECOUEN',
                'slug' => 'val_doise',
            ),
            492 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 501,
                'insee' => 95253,
                'name' => 'FREMAINVILLE',
                'slug' => 'val_doise',
            ),
            493 => 
            array (
                'code_departement' => 95,
                'cp' => 95830,
                'id' => 502,
                'insee' => 95254,
                'name' => 'FREMECOURT',
                'slug' => 'val_doise',
            ),
            494 => 
            array (
                'code_departement' => 95,
                'cp' => 95140,
                'id' => 503,
                'insee' => 95268,
                'name' => 'GARGES LES GONESSE',
                'slug' => 'val_doise',
            ),
            495 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 504,
                'insee' => 95270,
                'name' => 'GENAINVILLE',
                'slug' => 'val_doise',
            ),
            496 => 
            array (
                'code_departement' => 95,
                'cp' => 95220,
                'id' => 505,
                'insee' => 95306,
                'name' => 'HERBLAY SUR SEINE',
                'slug' => 'val_doise',
            ),
            497 => 
            array (
                'code_departement' => 95,
                'cp' => 95290,
                'id' => 507,
                'insee' => 95313,
                'name' => 'L ISLE ADAM',
                'slug' => 'val_doise',
            ),
            498 => 
            array (
                'code_departement' => 95,
                'cp' => 95690,
                'id' => 508,
                'insee' => 95328,
                'name' => 'LABBEVILLE',
                'slug' => 'val_doise',
            ),
            499 => 
            array (
                'code_departement' => 95,
                'cp' => 95580,
                'id' => 509,
                'insee' => 95369,
                'name' => 'MARGENCY',
                'slug' => 'val_doise',
            ),
        ));
        \DB::table('ville')->insert(array (
            0 => 
            array (
                'code_departement' => 95,
                'cp' => 95640,
                'id' => 510,
                'insee' => 95370,
                'name' => 'MARINES',
                'slug' => 'val_doise',
            ),
            1 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 511,
                'insee' => 95379,
                'name' => 'MAUDETOUR EN VEXIN',
                'slug' => 'val_doise',
            ),
            2 => 
            array (
                'code_departement' => 95,
                'cp' => 95650,
                'id' => 512,
                'insee' => 95422,
                'name' => 'MONTGEROULT',
                'slug' => 'val_doise',
            ),
            3 => 
            array (
                'code_departement' => 95,
                'cp' => 95370,
                'id' => 513,
                'insee' => 95424,
                'name' => 'MONTIGNY LES CORMEILLES',
                'slug' => 'val_doise',
            ),
            4 => 
            array (
                'code_departement' => 95,
                'cp' => 95680,
                'id' => 514,
                'insee' => 95426,
                'name' => 'MONTLIGNON',
                'slug' => 'val_doise',
            ),
            5 => 
            array (
                'code_departement' => 95,
                'cp' => 95360,
                'id' => 515,
                'insee' => 95427,
                'name' => 'MONTMAGNY',
                'slug' => 'val_doise',
            ),
            6 => 
            array (
                'code_departement' => 95,
                'cp' => 95000,
                'id' => 516,
                'insee' => 95450,
                'name' => 'NEUVILLE SUR OISE',
                'slug' => 'val_doise',
            ),
            7 => 
            array (
                'code_departement' => 95,
                'cp' => 95350,
                'id' => 517,
                'insee' => 95539,
                'name' => 'ST BRICE SOUS FORET',
                'slug' => 'val_doise',
            ),
            8 => 
            array (
                'code_departement' => 95,
                'cp' => 95320,
                'id' => 518,
                'insee' => 95563,
                'name' => 'ST LEU LA FORET',
                'slug' => 'val_doise',
            ),
            9 => 
            array (
                'code_departement' => 95,
                'cp' => 95110,
                'id' => 519,
                'insee' => 95582,
                'name' => 'SANNOIS',
                'slug' => 'val_doise',
            ),
            10 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 520,
                'insee' => 95592,
                'name' => 'SERAINCOURT',
                'slug' => 'val_doise',
            ),
            11 => 
            array (
                'code_departement' => 95,
                'cp' => 95500,
                'id' => 521,
                'insee' => 95612,
                'name' => 'LE THILLAY',
                'slug' => 'val_doise',
            ),
            12 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 522,
                'insee' => 95625,
                'name' => 'US',
                'slug' => 'val_doise',
            ),
            13 => 
            array (
                'code_departement' => 95,
                'cp' => 95470,
                'id' => 523,
                'insee' => 95641,
                'name' => 'VEMARS',
                'slug' => 'val_doise',
            ),
            14 => 
            array (
                'code_departement' => 95,
                'cp' => 95840,
                'id' => 524,
                'insee' => 95678,
                'name' => 'VILLIERS ADAM',
                'slug' => 'val_doise',
            ),
            15 => 
            array (
                'code_departement' => 95,
                'cp' => 95720,
                'id' => 525,
                'insee' => 95682,
                'name' => 'VILLIERS LE SEC',
                'slug' => 'val_doise',
            ),
            16 => 
            array (
                'code_departement' => 75,
                'cp' => 75001,
                'id' => 526,
                'insee' => 75101,
                'name' => 'PARIS 01',
                'slug' => 'paris',
            ),
            17 => 
            array (
                'code_departement' => 75,
                'cp' => 75005,
                'id' => 527,
                'insee' => 75105,
                'name' => 'PARIS 05',
                'slug' => 'paris',
            ),
            18 => 
            array (
                'code_departement' => 75,
                'cp' => 75009,
                'id' => 528,
                'insee' => 75109,
                'name' => 'PARIS 09',
                'slug' => 'paris',
            ),
            19 => 
            array (
                'code_departement' => 75,
                'cp' => 75010,
                'id' => 529,
                'insee' => 75110,
                'name' => 'PARIS 10',
                'slug' => 'paris',
            ),
            20 => 
            array (
                'code_departement' => 77,
                'cp' => 77120,
                'id' => 530,
                'insee' => 77002,
                'name' => 'AMILLIS',
                'slug' => 'seine_et_marne',
            ),
            21 => 
            array (
                'code_departement' => 77,
                'cp' => 77120,
                'id' => 531,
                'insee' => 77013,
                'name' => 'AULNOY',
                'slug' => 'seine_et_marne',
            ),
            22 => 
            array (
                'code_departement' => 77,
                'cp' => 77750,
                'id' => 532,
                'insee' => 77024,
                'name' => 'BASSEVELLE',
                'slug' => 'seine_et_marne',
            ),
            23 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 534,
                'insee' => 77032,
                'name' => 'BETON BAZOCHES',
                'slug' => 'seine_et_marne',
            ),
            24 => 
            array (
                'code_departement' => 77,
                'cp' => 77970,
                'id' => 535,
                'insee' => 77033,
                'name' => 'BEZALLES',
                'slug' => 'seine_et_marne',
            ),
            25 => 
            array (
                'code_departement' => 77,
                'cp' => 77590,
                'id' => 536,
                'insee' => 77037,
                'name' => 'BOIS LE ROI',
                'slug' => 'seine_et_marne',
            ),
            26 => 
            array (
                'code_departement' => 77,
                'cp' => 77350,
                'id' => 537,
                'insee' => 77039,
                'name' => 'BOISSISE LA BERTRAND',
                'slug' => 'seine_et_marne',
            ),
            27 => 
            array (
                'code_departement' => 77,
                'cp' => 77310,
                'id' => 538,
                'insee' => 77040,
                'name' => 'BOISSISE LE ROI',
                'slug' => 'seine_et_marne',
            ),
            28 => 
            array (
                'code_departement' => 77,
                'cp' => 77620,
                'id' => 539,
                'insee' => 77050,
                'name' => 'BRANSLES',
                'slug' => 'seine_et_marne',
            ),
            29 => 
            array (
                'code_departement' => 77,
                'cp' => 77720,
                'id' => 540,
                'insee' => 77052,
                'name' => 'BREAU',
                'slug' => 'seine_et_marne',
            ),
            30 => 
            array (
                'code_departement' => 77,
                'cp' => 77170,
                'id' => 541,
                'insee' => 77053,
                'name' => 'BRIE COMTE ROBERT',
                'slug' => 'seine_et_marne',
            ),
            31 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 542,
                'insee' => 77056,
                'name' => 'BURCY',
                'slug' => 'seine_et_marne',
            ),
            32 => 
            array (
                'code_departement' => 77,
                'cp' => 77600,
                'id' => 543,
                'insee' => 77058,
                'name' => 'BUSSY ST GEORGES',
                'slug' => 'seine_et_marne',
            ),
            33 => 
            array (
                'code_departement' => 77,
                'cp' => 77600,
                'id' => 544,
                'insee' => 77059,
                'name' => 'BUSSY ST MARTIN',
                'slug' => 'seine_et_marne',
            ),
            34 => 
            array (
                'code_departement' => 77,
                'cp' => 77460,
                'id' => 546,
                'insee' => 77071,
                'name' => 'CHAINTREAUX',
                'slug' => 'seine_et_marne',
            ),
            35 => 
            array (
                'code_departement' => 77,
                'cp' => 77600,
                'id' => 547,
                'insee' => 77085,
                'name' => 'CHANTELOUP EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            36 => 
            array (
                'code_departement' => 77,
                'cp' => 77590,
                'id' => 548,
                'insee' => 77096,
                'name' => 'CHARTRETTES',
                'slug' => 'seine_et_marne',
            ),
            37 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 549,
                'insee' => 77097,
                'name' => 'CHARTRONGES',
                'slug' => 'seine_et_marne',
            ),
            38 => 
            array (
                'code_departement' => 77,
                'cp' => 77500,
                'id' => 550,
                'insee' => 77108,
                'name' => 'CHELLES',
                'slug' => 'seine_et_marne',
            ),
            39 => 
            array (
                'code_departement' => 77,
                'cp' => 77700,
                'id' => 551,
                'insee' => 77111,
                'name' => 'CHESSY',
                'slug' => 'seine_et_marne',
            ),
            40 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 552,
                'insee' => 77112,
                'name' => 'CHEVRAINVILLIERS',
                'slug' => 'seine_et_marne',
            ),
            41 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 553,
                'insee' => 77113,
                'name' => 'CHEVRU',
                'slug' => 'seine_et_marne',
            ),
            42 => 
            array (
                'code_departement' => 77,
                'cp' => 77440,
                'id' => 554,
                'insee' => 77120,
                'name' => 'COCHEREL',
                'slug' => 'seine_et_marne',
            ),
            43 => 
            array (
                'code_departement' => 77,
                'cp' => 77090,
                'id' => 555,
                'insee' => 77121,
                'name' => 'COLLEGIEN',
                'slug' => 'seine_et_marne',
            ),
            44 => 
            array (
                'code_departement' => 77,
                'cp' => 77380,
                'id' => 556,
                'insee' => 77122,
                'name' => 'COMBS LA VILLE',
                'slug' => 'seine_et_marne',
            ),
            45 => 
            array (
                'code_departement' => 77,
                'cp' => 77840,
                'id' => 557,
                'insee' => 77129,
                'name' => 'COULOMBS EN VALOIS',
                'slug' => 'seine_et_marne',
            ),
            46 => 
            array (
                'code_departement' => 77,
                'cp' => 77120,
                'id' => 558,
                'insee' => 77131,
                'name' => 'COULOMMIERS',
                'slug' => 'seine_et_marne',
            ),
            47 => 
            array (
                'code_departement' => 77,
                'cp' => 77700,
                'id' => 559,
                'insee' => 77132,
                'name' => 'COUPVRAY',
                'slug' => 'seine_et_marne',
            ),
            48 => 
            array (
                'code_departement' => 77,
                'cp' => 77560,
                'id' => 560,
                'insee' => 77137,
                'name' => 'COURTACON',
                'slug' => 'seine_et_marne',
            ),
            49 => 
            array (
                'code_departement' => 77,
                'cp' => 77181,
                'id' => 561,
                'insee' => 77139,
                'name' => 'COURTRY',
                'slug' => 'seine_et_marne',
            ),
            50 => 
            array (
                'code_departement' => 77,
                'cp' => 77610,
                'id' => 562,
                'insee' => 77144,
                'name' => 'CREVECOEUR EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            51 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 563,
                'insee' => 77151,
                'name' => 'DAGNY',
                'slug' => 'seine_et_marne',
            ),
            52 => 
            array (
                'code_departement' => 77,
                'cp' => 77940,
                'id' => 564,
                'insee' => 77158,
                'name' => 'DIANT',
                'slug' => 'seine_et_marne',
            ),
            53 => 
            array (
                'code_departement' => 77,
                'cp' => 77130,
                'id' => 565,
                'insee' => 77161,
                'name' => 'DORMELLES',
                'slug' => 'seine_et_marne',
            ),
            54 => 
            array (
                'code_departement' => 77,
                'cp' => 77510,
                'id' => 566,
                'insee' => 77162,
                'name' => 'DOUE',
                'slug' => 'seine_et_marne',
            ),
            55 => 
            array (
                'code_departement' => 77,
                'cp' => 77184,
                'id' => 567,
                'insee' => 77169,
                'name' => 'EMERAINVILLE',
                'slug' => 'seine_et_marne',
            ),
            56 => 
            array (
                'code_departement' => 77,
                'cp' => 77167,
                'id' => 568,
                'insee' => 77178,
                'name' => 'FAY LES NEMOURS',
                'slug' => 'seine_et_marne',
            ),
            57 => 
            array (
                'code_departement' => 77,
                'cp' => 77610,
                'id' => 569,
                'insee' => 77192,
                'name' => 'FONTENAY TRESIGNY',
                'slug' => 'seine_et_marne',
            ),
            58 => 
            array (
                'code_departement' => 77,
                'cp' => 77130,
                'id' => 570,
                'insee' => 77194,
                'name' => 'FORGES',
                'slug' => 'seine_et_marne',
            ),
            59 => 
            array (
                'code_departement' => 77,
                'cp' => 77890,
                'id' => 571,
                'insee' => 77200,
                'name' => 'GARENTREVILLE',
                'slug' => 'seine_et_marne',
            ),
            60 => 
            array (
                'code_departement' => 77,
                'cp' => 77165,
                'id' => 572,
                'insee' => 77205,
                'name' => 'GESVRES LE CHAPITRE',
                'slug' => 'seine_et_marne',
            ),
            61 => 
            array (
                'code_departement' => 77,
                'cp' => 77890,
                'id' => 573,
                'insee' => 77207,
                'name' => 'GIRONVILLE',
                'slug' => 'seine_et_marne',
            ),
            62 => 
            array (
                'code_departement' => 77,
                'cp' => 77400,
                'id' => 574,
                'insee' => 77209,
                'name' => 'GOUVERNES',
                'slug' => 'seine_et_marne',
            ),
            63 => 
            array (
                'code_departement' => 77,
                'cp' => 77880,
                'id' => 575,
                'insee' => 77216,
                'name' => 'GREZ SUR LOING',
                'slug' => 'seine_et_marne',
            ),
            64 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 576,
                'insee' => 77220,
                'name' => 'GUERCHEVILLE',
                'slug' => 'seine_et_marne',
            ),
            65 => 
            array (
                'code_departement' => 77,
                'cp' => 77600,
                'id' => 577,
                'insee' => 77221,
                'name' => 'GUERMANTES',
                'slug' => 'seine_et_marne',
            ),
            66 => 
            array (
                'code_departement' => 77,
                'cp' => 77390,
                'id' => 578,
                'insee' => 77222,
                'name' => 'GUIGNES',
                'slug' => 'seine_et_marne',
            ),
            67 => 
            array (
                'code_departement' => 77,
                'cp' => 77515,
                'id' => 579,
                'insee' => 77224,
                'name' => 'HAUTEFEUILLE',
                'slug' => 'seine_et_marne',
            ),
            68 => 
            array (
                'code_departement' => 77,
                'cp' => 77580,
                'id' => 580,
                'insee' => 77225,
                'name' => 'LA HAUTE MAISON',
                'slug' => 'seine_et_marne',
            ),
            69 => 
            array (
                'code_departement' => 77,
                'cp' => 77440,
                'id' => 581,
                'insee' => 77231,
                'name' => 'ISLES LES MELDEUSES',
                'slug' => 'seine_et_marne',
            ),
            70 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 582,
                'insee' => 77247,
                'name' => 'LESCHEROLLES',
                'slug' => 'seine_et_marne',
            ),
            71 => 
            array (
                'code_departement' => 77,
                'cp' => 77150,
                'id' => 583,
                'insee' => 77249,
                'name' => 'LESIGNY',
                'slug' => 'seine_et_marne',
            ),
            72 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 584,
                'insee' => 77250,
                'name' => 'LEUDON EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            73 => 
            array (
                'code_departement' => 77,
                'cp' => 77000,
                'id' => 585,
                'insee' => 77255,
                'name' => 'LIVRY SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            74 => 
            array (
                'code_departement' => 77,
                'cp' => 77650,
                'id' => 586,
                'insee' => 77256,
                'name' => 'LIZINES',
                'slug' => 'seine_et_marne',
            ),
            75 => 
            array (
                'code_departement' => 77,
                'cp' => 77650,
                'id' => 587,
                'insee' => 77260,
                'name' => 'LONGUEVILLE',
                'slug' => 'seine_et_marne',
            ),
            76 => 
            array (
                'code_departement' => 77,
                'cp' => 77520,
                'id' => 588,
                'insee' => 77263,
                'name' => 'LUISETAINES',
                'slug' => 'seine_et_marne',
            ),
            77 => 
            array (
                'code_departement' => 77,
                'cp' => 77133,
                'id' => 590,
                'insee' => 77266,
                'name' => 'MACHAULT',
                'slug' => 'seine_et_marne',
            ),
            78 => 
            array (
                'code_departement' => 77,
                'cp' => 77580,
                'id' => 591,
                'insee' => 77270,
                'name' => 'MAISONCELLES EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            79 => 
            array (
                'code_departement' => 77,
                'cp' => 77120,
                'id' => 592,
                'insee' => 77281,
                'name' => 'MAUPERTHUIS',
                'slug' => 'seine_et_marne',
            ),
            80 => 
            array (
                'code_departement' => 77,
                'cp' => 77990,
                'id' => 593,
                'insee' => 77291,
                'name' => 'LE MESNIL AMELOT',
                'slug' => 'seine_et_marne',
            ),
            81 => 
            array (
                'code_departement' => 77,
                'cp' => 77144,
                'id' => 594,
                'insee' => 77307,
                'name' => 'MONTEVRAIN',
                'slug' => 'seine_et_marne',
            ),
            82 => 
            array (
                'code_departement' => 77,
                'cp' => 77250,
                'id' => 595,
                'insee' => 77316,
                'name' => 'MORET LOING ET ORVANNE',
                'slug' => 'seine_et_marne',
            ),
            83 => 
            array (
                'code_departement' => 77,
                'cp' => 77163,
                'id' => 597,
                'insee' => 77318,
                'name' => 'MORTCERF',
                'slug' => 'seine_et_marne',
            ),
            84 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 598,
                'insee' => 77328,
                'name' => 'NANTEAU SUR ESSONNE',
                'slug' => 'seine_et_marne',
            ),
            85 => 
            array (
                'code_departement' => 77,
                'cp' => 77140,
                'id' => 599,
                'insee' => 77333,
                'name' => 'NEMOURS',
                'slug' => 'seine_et_marne',
            ),
            86 => 
            array (
                'code_departement' => 77,
                'cp' => 77186,
                'id' => 600,
                'insee' => 77337,
                'name' => 'NOISIEL',
                'slug' => 'seine_et_marne',
            ),
            87 => 
            array (
                'code_departement' => 77,
                'cp' => 77830,
                'id' => 601,
                'insee' => 77354,
                'name' => 'PAMFOU',
                'slug' => 'seine_et_marne',
            ),
            88 => 
            array (
                'code_departement' => 77,
                'cp' => 77480,
                'id' => 602,
                'insee' => 77356,
                'name' => 'PASSY SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            89 => 
            array (
                'code_departement' => 77,
                'cp' => 77440,
                'id' => 603,
                'insee' => 77367,
                'name' => 'LE PLESSIS PLACY',
                'slug' => 'seine_et_marne',
            ),
            90 => 
            array (
                'code_departement' => 77,
                'cp' => 77167,
                'id' => 604,
                'insee' => 77370,
                'name' => 'POLIGNY',
                'slug' => 'seine_et_marne',
            ),
            91 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 605,
                'insee' => 77386,
                'name' => 'RECLOSES',
                'slug' => 'seine_et_marne',
            ),
            92 => 
            array (
                'code_departement' => 77,
                'cp' => 77710,
                'id' => 606,
                'insee' => 77387,
                'name' => 'REMAUVILLE',
                'slug' => 'seine_et_marne',
            ),
            93 => 
            array (
                'code_departement' => 77,
                'cp' => 77260,
                'id' => 607,
                'insee' => 77388,
                'name' => 'REUIL EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            94 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 608,
                'insee' => 77402,
                'name' => 'ST BARTHELEMY',
                'slug' => 'seine_et_marne',
            ),
            95 => 
            array (
                'code_departement' => 77,
                'cp' => 77310,
                'id' => 609,
                'insee' => 77407,
                'name' => 'ST FARGEAU PONTHIERRY',
                'slug' => 'seine_et_marne',
            ),
            96 => 
            array (
                'code_departement' => 77,
                'cp' => 77169,
                'id' => 610,
                'insee' => 77411,
                'name' => 'ST GERMAIN SOUS DOUE',
                'slug' => 'seine_et_marne',
            ),
            97 => 
            array (
                'code_departement' => 77,
                'cp' => 77650,
                'id' => 611,
                'insee' => 77418,
                'name' => 'ST LOUP DE NAUD',
                'slug' => 'seine_et_marne',
            ),
            98 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 612,
                'insee' => 77421,
                'name' => 'ST MARS VIEUX MAISONS',
                'slug' => 'seine_et_marne',
            ),
            99 => 
            array (
                'code_departement' => 77,
                'cp' => 77410,
                'id' => 613,
                'insee' => 77427,
                'name' => 'ST MESMES',
                'slug' => 'seine_et_marne',
            ),
            100 => 
            array (
                'code_departement' => 77,
                'cp' => 77750,
                'id' => 614,
                'insee' => 77429,
                'name' => 'ST OUEN SUR MORIN',
                'slug' => 'seine_et_marne',
            ),
            101 => 
            array (
                'code_departement' => 77,
                'cp' => 77240,
                'id' => 615,
                'insee' => 77447,
                'name' => 'SEINE PORT',
                'slug' => 'seine_et_marne',
            ),
            102 => 
            array (
                'code_departement' => 77,
                'cp' => 77520,
                'id' => 616,
                'insee' => 77454,
                'name' => 'SOGNOLLES EN MONTOIS',
                'slug' => 'seine_et_marne',
            ),
            103 => 
            array (
                'code_departement' => 77,
                'cp' => 77520,
                'id' => 617,
                'insee' => 77461,
                'name' => 'THENISY',
                'slug' => 'seine_et_marne',
            ),
            104 => 
            array (
                'code_departement' => 77,
                'cp' => 77230,
                'id' => 618,
                'insee' => 77462,
                'name' => 'THIEUX',
                'slug' => 'seine_et_marne',
            ),
            105 => 
            array (
                'code_departement' => 77,
                'cp' => 77123,
                'id' => 619,
                'insee' => 77471,
                'name' => 'TOUSSON',
                'slug' => 'seine_et_marne',
            ),
            106 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 620,
                'insee' => 77477,
                'name' => 'URY',
                'slug' => 'seine_et_marne',
            ),
            107 => 
            array (
                'code_departement' => 77,
                'cp' => 77830,
                'id' => 621,
                'insee' => 77480,
                'name' => 'VALENCE EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            108 => 
            array (
                'code_departement' => 77,
                'cp' => 77240,
                'id' => 622,
                'insee' => 77495,
                'name' => 'VERT ST DENIS',
                'slug' => 'seine_et_marne',
            ),
            109 => 
            array (
                'code_departement' => 77,
                'cp' => 77250,
                'id' => 623,
                'insee' => 77506,
                'name' => 'VILLEMER',
                'slug' => 'seine_et_marne',
            ),
            110 => 
            array (
                'code_departement' => 77,
                'cp' => 77174,
                'id' => 624,
                'insee' => 77508,
                'name' => 'VILLENEUVE LE COMTE',
                'slug' => 'seine_et_marne',
            ),
            111 => 
            array (
                'code_departement' => 77,
                'cp' => 77190,
                'id' => 626,
                'insee' => 77518,
                'name' => 'VILLIERS EN BIERE',
                'slug' => 'seine_et_marne',
            ),
            112 => 
            array (
                'code_departement' => 77,
                'cp' => 77139,
                'id' => 627,
                'insee' => 77526,
                'name' => 'VINCY MANOEUVRE',
                'slug' => 'seine_et_marne',
            ),
            113 => 
            array (
                'code_departement' => 77,
                'cp' => 77540,
                'id' => 628,
                'insee' => 77527,
                'name' => 'VOINSLES',
                'slug' => 'seine_et_marne',
            ),
            114 => 
            array (
                'code_departement' => 77,
                'cp' => 77580,
                'id' => 629,
                'insee' => 77529,
                'name' => 'VOULANGIS',
                'slug' => 'seine_et_marne',
            ),
            115 => 
            array (
                'code_departement' => 77,
                'cp' => 77870,
                'id' => 630,
                'insee' => 77533,
                'name' => 'VULAINES SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            116 => 
            array (
                'code_departement' => 77,
                'cp' => 77390,
                'id' => 631,
                'insee' => 77534,
                'name' => 'YEBLES',
                'slug' => 'seine_et_marne',
            ),
            117 => 
            array (
                'code_departement' => 78,
                'cp' => 78260,
                'id' => 632,
                'insee' => 78005,
                'name' => 'ACHERES',
                'slug' => 'yvelines',
            ),
            118 => 
            array (
                'code_departement' => 78,
                'cp' => 78660,
                'id' => 633,
                'insee' => 78009,
                'name' => 'ALLAINVILLE',
                'slug' => 'yvelines',
            ),
            119 => 
            array (
                'code_departement' => 78,
                'cp' => 78490,
                'id' => 634,
                'insee' => 78050,
                'name' => 'BAZOCHES SUR GUYONNE',
                'slug' => 'yvelines',
            ),
            120 => 
            array (
                'code_departement' => 78,
                'cp' => 78910,
                'id' => 635,
                'insee' => 78053,
                'name' => 'BEHOUST',
                'slug' => 'yvelines',
            ),
            121 => 
            array (
                'code_departement' => 78,
                'cp' => 78650,
                'id' => 636,
                'insee' => 78062,
                'name' => 'BEYNES',
                'slug' => 'yvelines',
            ),
            122 => 
            array (
                'code_departement' => 78,
                'cp' => 78390,
                'id' => 637,
                'insee' => 78073,
                'name' => 'BOIS D ARCY',
                'slug' => 'yvelines',
            ),
            123 => 
            array (
                'code_departement' => 78,
                'cp' => 78200,
                'id' => 638,
                'insee' => 78082,
                'name' => 'BOISSY MAUVOISIN',
                'slug' => 'yvelines',
            ),
            124 => 
            array (
                'code_departement' => 78,
                'cp' => 78380,
                'id' => 639,
                'insee' => 78092,
                'name' => 'BOUGIVAL',
                'slug' => 'yvelines',
            ),
            125 => 
            array (
                'code_departement' => 78,
                'cp' => 78930,
                'id' => 640,
                'insee' => 78104,
                'name' => 'BREUIL BOIS ROBERT',
                'slug' => 'yvelines',
            ),
            126 => 
            array (
                'code_departement' => 78,
                'cp' => 78720,
                'id' => 642,
                'insee' => 78125,
                'name' => 'LA CELLE LES BORDES',
                'slug' => 'yvelines',
            ),
            127 => 
            array (
                'code_departement' => 78,
                'cp' => 78570,
                'id' => 643,
                'insee' => 78138,
                'name' => 'CHANTELOUP LES VIGNES',
                'slug' => 'yvelines',
            ),
            128 => 
            array (
                'code_departement' => 78,
                'cp' => 78270,
                'id' => 644,
                'insee' => 78147,
                'name' => 'CHAUFOUR LES BONNIERES',
                'slug' => 'yvelines',
            ),
            129 => 
            array (
                'code_departement' => 78,
                'cp' => 78340,
                'id' => 645,
                'insee' => 78165,
                'name' => 'LES CLAYES SOUS BOIS',
                'slug' => 'yvelines',
            ),
            130 => 
            array (
                'code_departement' => 78,
                'cp' => 78790,
                'id' => 646,
                'insee' => 78185,
                'name' => 'COURGENT',
                'slug' => 'yvelines',
            ),
            131 => 
            array (
                'code_departement' => 78,
                'cp' => 78121,
                'id' => 647,
                'insee' => 78189,
                'name' => 'CRESPIERES',
                'slug' => 'yvelines',
            ),
            132 => 
            array (
                'code_departement' => 78,
                'cp' => 78520,
                'id' => 648,
                'insee' => 78239,
                'name' => 'FOLLAINVILLE DENNEMONT',
                'slug' => 'yvelines',
            ),
            133 => 
            array (
                'code_departement' => 78,
                'cp' => 78330,
                'id' => 649,
                'insee' => 78242,
                'name' => 'FONTENAY LE FLEURY',
                'slug' => 'yvelines',
            ),
            134 => 
            array (
                'code_departement' => 78,
                'cp' => 78200,
                'id' => 650,
                'insee' => 78245,
                'name' => 'FONTENAY MAUVOISIN',
                'slug' => 'yvelines',
            ),
            135 => 
            array (
                'code_departement' => 78,
                'cp' => 78490,
                'id' => 651,
                'insee' => 78262,
                'name' => 'GALLUIS',
                'slug' => 'yvelines',
            ),
            136 => 
            array (
                'code_departement' => 78,
                'cp' => 78440,
                'id' => 652,
                'insee' => 78267,
                'name' => 'GARGENVILLE',
                'slug' => 'yvelines',
            ),
            137 => 
            array (
                'code_departement' => 78,
                'cp' => 78550,
                'id' => 653,
                'insee' => 78285,
                'name' => 'GRESSEY',
                'slug' => 'yvelines',
            ),
            138 => 
            array (
                'code_departement' => 78,
                'cp' => 78280,
                'id' => 654,
                'insee' => 78297,
                'name' => 'GUYANCOURT',
                'slug' => 'yvelines',
            ),
            139 => 
            array (
                'code_departement' => 78,
                'cp' => 78270,
                'id' => 656,
                'insee' => 78320,
                'name' => 'NOTRE DAME DE LA MER',
                'slug' => 'yvelines',
            ),
            140 => 
            array (
                'code_departement' => 78,
                'cp' => 78200,
                'id' => 657,
                'insee' => 78324,
                'name' => 'JOUY MAUVOISIN',
                'slug' => 'yvelines',
            ),
            141 => 
            array (
                'code_departement' => 78,
                'cp' => 78580,
                'id' => 658,
                'insee' => 78325,
                'name' => 'JUMEAUVILLE',
                'slug' => 'yvelines',
            ),
            142 => 
            array (
                'code_departement' => 78,
                'cp' => 78980,
                'id' => 659,
                'insee' => 78346,
                'name' => 'LONGNES',
                'slug' => 'yvelines',
            ),
            143 => 
            array (
                'code_departement' => 78,
                'cp' => 78711,
                'id' => 660,
                'insee' => 78362,
                'name' => 'MANTES LA VILLE',
                'slug' => 'yvelines',
            ),
            144 => 
            array (
                'code_departement' => 78,
                'cp' => 78160,
                'id' => 661,
                'insee' => 78372,
                'name' => 'MARLY LE ROI',
                'slug' => 'yvelines',
            ),
            145 => 
            array (
                'code_departement' => 78,
                'cp' => 78670,
                'id' => 662,
                'insee' => 78384,
                'name' => 'MEDAN',
                'slug' => 'yvelines',
            ),
            146 => 
            array (
                'code_departement' => 78,
                'cp' => 78490,
                'id' => 663,
                'insee' => 78389,
                'name' => 'MERE',
                'slug' => 'yvelines',
            ),
            147 => 
            array (
                'code_departement' => 78,
                'cp' => 78320,
                'id' => 664,
                'insee' => 78397,
                'name' => 'LE MESNIL ST DENIS',
                'slug' => 'yvelines',
            ),
            148 => 
            array (
                'code_departement' => 78,
                'cp' => 78250,
                'id' => 665,
                'insee' => 78403,
                'name' => 'MEZY SUR SEINE',
                'slug' => 'yvelines',
            ),
            149 => 
            array (
                'code_departement' => 78,
                'cp' => 78790,
                'id' => 666,
                'insee' => 78417,
                'name' => 'MONTCHAUVET',
                'slug' => 'yvelines',
            ),
            150 => 
            array (
                'code_departement' => 78,
                'cp' => 78490,
                'id' => 667,
                'insee' => 78420,
                'name' => 'MONTFORT L AMAURY',
                'slug' => 'yvelines',
            ),
            151 => 
            array (
                'code_departement' => 78,
                'cp' => 78630,
                'id' => 668,
                'insee' => 78431,
                'name' => 'MORAINVILLIERS',
                'slug' => 'yvelines',
            ),
            152 => 
            array (
                'code_departement' => 78,
                'cp' => 78270,
                'id' => 669,
                'insee' => 78437,
                'name' => 'MOUSSEAUX SUR SEINE',
                'slug' => 'yvelines',
            ),
            153 => 
            array (
                'code_departement' => 78,
                'cp' => 78640,
                'id' => 670,
                'insee' => 78442,
                'name' => 'NEAUPHLE LE CHATEAU',
                'slug' => 'yvelines',
            ),
            154 => 
            array (
                'code_departement' => 78,
                'cp' => 78640,
                'id' => 671,
                'insee' => 78443,
                'name' => 'NEAUPHLE LE VIEUX',
                'slug' => 'yvelines',
            ),
            155 => 
            array (
                'code_departement' => 78,
                'cp' => 78125,
                'id' => 672,
                'insee' => 78464,
                'name' => 'ORCEMONT',
                'slug' => 'yvelines',
            ),
            156 => 
            array (
                'code_departement' => 78,
                'cp' => 78910,
                'id' => 673,
                'insee' => 78475,
                'name' => 'OSMOY',
                'slug' => 'yvelines',
            ),
            157 => 
            array (
                'code_departement' => 78,
                'cp' => 78730,
                'id' => 674,
                'insee' => 78499,
                'name' => 'PONTHEVRARD',
                'slug' => 'yvelines',
            ),
            158 => 
            array (
                'code_departement' => 78,
                'cp' => 78440,
                'id' => 675,
                'insee' => 78501,
                'name' => 'PORCHEVILLE',
                'slug' => 'yvelines',
            ),
            159 => 
            array (
                'code_departement' => 78,
                'cp' => 78560,
                'id' => 676,
                'insee' => 78502,
                'name' => 'LE PORT MARLY',
                'slug' => 'yvelines',
            ),
            160 => 
            array (
                'code_departement' => 78,
                'cp' => 78100,
                'id' => 677,
                'insee' => 78551,
                'name' => 'ST GERMAIN EN LAYE',
                'slug' => 'yvelines',
            ),
            161 => 
            array (
                'code_departement' => 78,
                'cp' => 78860,
                'id' => 678,
                'insee' => 78571,
                'name' => 'ST NOM LA BRETECHE',
                'slug' => 'yvelines',
            ),
            162 => 
            array (
                'code_departement' => 78,
                'cp' => 78500,
                'id' => 679,
                'insee' => 78586,
                'name' => 'SARTROUVILLE',
                'slug' => 'yvelines',
            ),
            163 => 
            array (
                'code_departement' => 78,
                'cp' => 78650,
                'id' => 680,
                'insee' => 78588,
                'name' => 'SAULX MARCHAIS',
                'slug' => 'yvelines',
            ),
            164 => 
            array (
                'code_departement' => 78,
                'cp' => 78790,
                'id' => 681,
                'insee' => 78591,
                'name' => 'SEPTEUIL',
                'slug' => 'yvelines',
            ),
            165 => 
            array (
                'code_departement' => 78,
                'cp' => 78980,
                'id' => 682,
                'insee' => 78608,
                'name' => 'LE TERTRE ST DENIS',
                'slug' => 'yvelines',
            ),
            166 => 
            array (
                'code_departement' => 78,
                'cp' => 78790,
                'id' => 683,
                'insee' => 78618,
                'name' => 'TILLY',
                'slug' => 'yvelines',
            ),
            167 => 
            array (
                'code_departement' => 78,
                'cp' => 78480,
                'id' => 684,
                'insee' => 78642,
                'name' => 'VERNEUIL SUR SEINE',
                'slug' => 'yvelines',
            ),
            168 => 
            array (
                'code_departement' => 75,
                'cp' => 7530,
                'id' => 685,
                'insee' => 7003,
                'name' => 'AIZAC',
                'slug' => 'paris',
            ),
            169 => 
            array (
                'code_departement' => 75,
                'cp' => 7580,
                'id' => 686,
                'insee' => 7032,
                'name' => 'BERZEME',
                'slug' => 'paris',
            ),
            170 => 
            array (
                'code_departement' => 75,
                'cp' => 7530,
                'id' => 687,
                'insee' => 7093,
                'name' => 'GENESTELLE',
                'slug' => 'paris',
            ),
            171 => 
            array (
                'code_departement' => 75,
                'cp' => 7570,
                'id' => 688,
                'insee' => 7114,
                'name' => 'LABATIE D ANDAURE',
                'slug' => 'paris',
            ),
            172 => 
            array (
                'code_departement' => 75,
                'cp' => 7590,
                'id' => 689,
                'insee' => 7232,
                'name' => 'ST ETIENNE DE LUGDARES',
                'slug' => 'paris',
            ),
            173 => 
            array (
                'code_departement' => 75,
                'cp' => 7510,
                'id' => 690,
                'insee' => 7235,
                'name' => 'STE EULALIE',
                'slug' => 'paris',
            ),
            174 => 
            array (
                'code_departement' => 75,
                'cp' => 7580,
                'id' => 691,
                'insee' => 7247,
                'name' => 'ST JEAN LE CENTENIER',
                'slug' => 'paris',
            ),
            175 => 
            array (
                'code_departement' => 78,
                'cp' => 7800,
                'id' => 692,
                'insee' => 7261,
                'name' => 'ST LAURENT DU PAPE',
                'slug' => 'yvelines',
            ),
            176 => 
            array (
                'code_departement' => 92,
                'cp' => 9240,
                'id' => 693,
                'insee' => 9001,
                'name' => 'AIGUES JUNTES',
                'slug' => 'hauts_de_seine',
            ),
            177 => 
            array (
                'code_departement' => 93,
                'cp' => 9310,
                'id' => 694,
                'insee' => 9024,
                'name' => 'ASTON',
                'slug' => 'seine_saint_denis',
            ),
            178 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 695,
                'insee' => 9052,
                'name' => 'BESSET',
                'slug' => 'val_doise',
            ),
            179 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 696,
                'insee' => 9060,
                'name' => 'BONNAC',
                'slug' => 'essonne',
            ),
            180 => 
            array (
                'code_departement' => 93,
                'cp' => 9310,
                'id' => 697,
                'insee' => 9070,
                'name' => 'LES CABANNES',
                'slug' => 'seine_saint_denis',
            ),
            181 => 
            array (
                'code_departement' => 92,
                'cp' => 9240,
                'id' => 698,
                'insee' => 9071,
                'name' => 'CADARCET',
                'slug' => 'hauts_de_seine',
            ),
            182 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 699,
                'insee' => 9072,
                'name' => 'CALZAN',
                'slug' => 'essonne',
            ),
            183 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 700,
                'insee' => 9074,
                'name' => 'CAMON',
                'slug' => 'val_doise',
            ),
            184 => 
            array (
                'code_departement' => 94,
                'cp' => 9420,
                'id' => 701,
                'insee' => 9082,
                'name' => 'CASTELNAU DURBAN',
                'slug' => 'val_de_marne',
            ),
            185 => 
            array (
                'code_departement' => 91,
                'cp' => 9160,
                'id' => 702,
                'insee' => 9086,
                'name' => 'CAUMONT',
                'slug' => 'essonne',
            ),
            186 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 703,
                'insee' => 9090,
                'name' => 'CAZAUX',
                'slug' => 'essonne',
            ),
            187 => 
            array (
                'code_departement' => 94,
                'cp' => 9420,
                'id' => 704,
                'insee' => 9097,
                'name' => 'CLERMONT',
                'slug' => 'val_de_marne',
            ),
            188 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 705,
                'insee' => 9104,
                'name' => 'DALOU',
                'slug' => 'essonne',
            ),
            189 => 
            array (
                'code_departement' => 92,
                'cp' => 9240,
                'id' => 706,
                'insee' => 9108,
                'name' => 'DURBAN SUR ARIZE',
                'slug' => 'hauts_de_seine',
            ),
            190 => 
            array (
                'code_departement' => 92,
                'cp' => 9200,
                'id' => 707,
                'insee' => 9110,
                'name' => 'ENCOURTIECH',
                'slug' => 'hauts_de_seine',
            ),
            191 => 
            array (
                'code_departement' => 92,
                'cp' => 9200,
                'id' => 708,
                'insee' => 9119,
                'name' => 'EYCHEIL',
                'slug' => 'hauts_de_seine',
            ),
            192 => 
            array (
                'code_departement' => 91,
                'cp' => 9190,
                'id' => 709,
                'insee' => 9128,
                'name' => 'GAJAN',
                'slug' => 'essonne',
            ),
            193 => 
            array (
                'code_departement' => 93,
                'cp' => 9390,
                'id' => 710,
                'insee' => 9139,
                'name' => 'L HOSPITALET PRES L ANDORRE',
                'slug' => 'seine_saint_denis',
            ),
            194 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 711,
                'insee' => 9142,
                'name' => 'ILHAT',
                'slug' => 'seine_saint_denis',
            ),
            195 => 
            array (
                'code_departement' => 91,
                'cp' => 9130,
                'id' => 712,
                'insee' => 9151,
                'name' => 'LANOUX',
                'slug' => 'essonne',
            ),
            196 => 
            array (
                'code_departement' => 92,
                'cp' => 9240,
                'id' => 713,
                'insee' => 9154,
                'name' => 'LARBONT',
                'slug' => 'hauts_de_seine',
            ),
            197 => 
            array (
                'code_departement' => 93,
                'cp' => 9310,
                'id' => 714,
                'insee' => 9159,
                'name' => 'LASSUR',
                'slug' => 'seine_saint_denis',
            ),
            198 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 715,
                'insee' => 9163,
                'name' => 'LESCOUSSE',
                'slug' => 'essonne',
            ),
            199 => 
            array (
                'code_departement' => 94,
                'cp' => 9420,
                'id' => 716,
                'insee' => 9164,
                'name' => 'LESCURE',
                'slug' => 'val_de_marne',
            ),
            200 => 
            array (
                'code_departement' => 92,
                'cp' => 9250,
                'id' => 717,
                'insee' => 9171,
                'name' => 'LORDAT',
                'slug' => 'hauts_de_seine',
            ),
            201 => 
            array (
                'code_departement' => 91,
                'cp' => 9160,
                'id' => 718,
                'insee' => 9183,
                'name' => 'MAUVEZIN DE PRAT',
                'slug' => 'essonne',
            ),
            202 => 
            array (
                'code_departement' => 94,
                'cp' => 9460,
                'id' => 719,
                'insee' => 9193,
                'name' => 'MIJANES',
                'slug' => 'val_de_marne',
            ),
            203 => 
            array (
                'code_departement' => 92,
                'cp' => 9200,
                'id' => 720,
                'insee' => 9209,
                'name' => 'MONTJOIE EN COUSERANS',
                'slug' => 'hauts_de_seine',
            ),
            204 => 
            array (
                'code_departement' => 92,
                'cp' => 9240,
                'id' => 722,
                'insee' => 9212,
                'name' => 'MONTSERON',
                'slug' => 'hauts_de_seine',
            ),
            205 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 723,
                'insee' => 9217,
                'name' => 'NIAUX',
                'slug' => 'val_de_marne',
            ),
            206 => 
            array (
                'code_departement' => 91,
                'cp' => 9130,
                'id' => 724,
                'insee' => 9224,
                'name' => 'PAILHES',
                'slug' => 'essonne',
            ),
            207 => 
            array (
                'code_departement' => 91,
                'cp' => 9110,
                'id' => 725,
                'insee' => 9232,
                'name' => 'PRADES',
                'slug' => 'essonne',
            ),
            208 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 726,
                'insee' => 9244,
                'name' => 'RIEUCROS',
                'slug' => 'val_doise',
            ),
            209 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 727,
                'insee' => 9245,
                'name' => 'RIEUX DE PELLEPORT',
                'slug' => 'essonne',
            ),
            210 => 
            array (
                'code_departement' => 94,
                'cp' => 9420,
                'id' => 728,
                'insee' => 9246,
                'name' => 'RIMONT',
                'slug' => 'val_de_marne',
            ),
            211 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 729,
                'insee' => 9262,
                'name' => 'ST JEAN D AIGUES VIVES',
                'slug' => 'seine_saint_denis',
            ),
            212 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 730,
                'insee' => 9266,
                'name' => 'ST JULIEN DE GRAS CAPOU',
                'slug' => 'val_doise',
            ),
            213 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 731,
                'insee' => 9270,
                'name' => 'ST MARTIN D OYDES',
                'slug' => 'essonne',
            ),
            214 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 732,
                'insee' => 9271,
                'name' => 'ST MICHEL',
                'slug' => 'essonne',
            ),
            215 => 
            array (
                'code_departement' => 92,
                'cp' => 9240,
                'id' => 733,
                'insee' => 9292,
                'name' => 'SENTENAC DE SEROU',
                'slug' => 'hauts_de_seine',
            ),
            216 => 
            array (
                'code_departement' => 91,
                'cp' => 9140,
                'id' => 734,
                'insee' => 9299,
                'name' => 'SOUEIX ROGALLE',
                'slug' => 'essonne',
            ),
            217 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 736,
                'insee' => 9303,
                'name' => 'SURBA',
                'slug' => 'val_de_marne',
            ),
            218 => 
            array (
                'code_departement' => 93,
                'cp' => 9350,
                'id' => 737,
                'insee' => 9310,
                'name' => 'THOUARS SUR ARIZE',
                'slug' => 'seine_saint_denis',
            ),
            219 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 738,
                'insee' => 9312,
                'name' => 'LA TOUR DU CRIEU',
                'slug' => 'essonne',
            ),
            220 => 
            array (
                'code_departement' => 93,
                'cp' => 9310,
                'id' => 739,
                'insee' => 9320,
                'name' => 'URS',
                'slug' => 'seine_saint_denis',
            ),
            221 => 
            array (
                'code_departement' => 91,
                'cp' => 9110,
                'id' => 740,
                'insee' => 9325,
                'name' => 'VAYCHIS',
                'slug' => 'essonne',
            ),
            222 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 742,
                'insee' => 9339,
                'name' => 'VILLENEUVE DU PAREAGE',
                'slug' => 'essonne',
            ),
            223 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 743,
                'insee' => 9340,
                'name' => 'VIRA',
                'slug' => 'essonne',
            ),
            224 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 744,
                'insee' => 9341,
                'name' => 'VIVIES',
                'slug' => 'val_doise',
            ),
            225 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 745,
                'insee' => 91001,
                'name' => 'ABBEVILLE LA RIVIERE',
                'slug' => 'essonne',
            ),
            226 => 
            array (
                'code_departement' => 91,
                'cp' => 91290,
                'id' => 747,
                'insee' => 91021,
                'name' => 'ARPAJON',
                'slug' => 'essonne',
            ),
            227 => 
            array (
                'code_departement' => 91,
                'cp' => 91830,
                'id' => 748,
                'insee' => 91037,
                'name' => 'AUVERNAUX',
                'slug' => 'essonne',
            ),
            228 => 
            array (
                'code_departement' => 91,
                'cp' => 91580,
                'id' => 749,
                'insee' => 91038,
                'name' => 'AUVERS ST GEORGES',
                'slug' => 'essonne',
            ),
            229 => 
            array (
                'code_departement' => 91,
                'cp' => 91590,
                'id' => 750,
                'insee' => 91080,
                'name' => 'BOISSY LE CUTTE',
                'slug' => 'essonne',
            ),
            230 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 751,
                'insee' => 91098,
                'name' => 'BOUTERVILLIERS',
                'slug' => 'essonne',
            ),
            231 => 
            array (
                'code_departement' => 91,
                'cp' => 91820,
                'id' => 752,
                'insee' => 91099,
                'name' => 'BOUTIGNY SUR ESSONNE',
                'slug' => 'essonne',
            ),
            232 => 
            array (
                'code_departement' => 91,
                'cp' => 91740,
                'id' => 753,
                'insee' => 91131,
                'name' => 'CHALOU MOULINEUX',
                'slug' => 'essonne',
            ),
            233 => 
            array (
                'code_departement' => 91,
                'cp' => 91750,
                'id' => 754,
                'insee' => 91135,
                'name' => 'CHAMPCUEIL',
                'slug' => 'essonne',
            ),
            234 => 
            array (
                'code_departement' => 91,
                'cp' => 91160,
                'id' => 755,
                'insee' => 91136,
                'name' => 'CHAMPLAN',
                'slug' => 'essonne',
            ),
            235 => 
            array (
                'code_departement' => 91,
                'cp' => 91490,
                'id' => 756,
                'insee' => 91195,
                'name' => 'DANNEMOIS',
                'slug' => 'essonne',
            ),
            236 => 
            array (
                'code_departement' => 91,
                'cp' => 91410,
                'id' => 757,
                'insee' => 91200,
                'name' => 'DOURDAN',
                'slug' => 'essonne',
            ),
            237 => 
            array (
                'code_departement' => 91,
                'cp' => 91520,
                'id' => 758,
                'insee' => 91207,
                'name' => 'EGLY',
                'slug' => 'essonne',
            ),
            238 => 
            array (
                'code_departement' => 91,
                'cp' => 91590,
                'id' => 759,
                'insee' => 91232,
                'name' => 'LA FERTE ALAIS',
                'slug' => 'essonne',
            ),
            239 => 
            array (
                'code_departement' => 91,
                'cp' => 91700,
                'id' => 760,
                'insee' => 91235,
                'name' => 'FLEURY MEROGIS',
                'slug' => 'essonne',
            ),
            240 => 
            array (
                'code_departement' => 91,
                'cp' => 91410,
                'id' => 761,
                'insee' => 91247,
                'name' => 'LA FORET LE ROI',
                'slug' => 'essonne',
            ),
            241 => 
            array (
                'code_departement' => 91,
                'cp' => 91690,
                'id' => 762,
                'insee' => 91294,
                'name' => 'GUILLERVAL',
                'slug' => 'essonne',
            ),
            242 => 
            array (
                'code_departement' => 91,
                'cp' => 91640,
                'id' => 763,
                'insee' => 91319,
                'name' => 'JANVRY',
                'slug' => 'essonne',
            ),
            243 => 
            array (
                'code_departement' => 91,
                'cp' => 91160,
                'id' => 764,
                'insee' => 91345,
                'name' => 'LONGJUMEAU',
                'slug' => 'essonne',
            ),
            244 => 
            array (
                'code_departement' => 91,
                'cp' => 91720,
                'id' => 765,
                'insee' => 91359,
                'name' => 'MAISSE',
                'slug' => 'essonne',
            ),
            245 => 
            array (
                'code_departement' => 91,
                'cp' => 91460,
                'id' => 766,
                'insee' => 91363,
                'name' => 'MARCOUSSIS',
                'slug' => 'essonne',
            ),
            246 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 767,
                'insee' => 91399,
                'name' => 'MESPUITS',
                'slug' => 'essonne',
            ),
            247 => 
            array (
                'code_departement' => 91,
                'cp' => 91230,
                'id' => 768,
                'insee' => 91421,
                'name' => 'MONTGERON',
                'slug' => 'essonne',
            ),
            248 => 
            array (
                'code_departement' => 91,
                'cp' => 91250,
                'id' => 769,
                'insee' => 91435,
                'name' => 'MORSANG SUR SEINE',
                'slug' => 'essonne',
            ),
            249 => 
            array (
                'code_departement' => 91,
                'cp' => 91400,
                'id' => 770,
                'insee' => 91471,
                'name' => 'ORSAY',
                'slug' => 'essonne',
            ),
            250 => 
            array (
                'code_departement' => 91,
                'cp' => 91470,
                'id' => 771,
                'insee' => 91482,
                'name' => 'PECQUEUSE',
                'slug' => 'essonne',
            ),
            251 => 
            array (
                'code_departement' => 91,
                'cp' => 91740,
                'id' => 772,
                'insee' => 91511,
                'name' => 'PUSSAY',
                'slug' => 'essonne',
            ),
            252 => 
            array (
                'code_departement' => 91,
                'cp' => 91410,
                'id' => 773,
                'insee' => 91525,
                'name' => 'ROINVILLE',
                'slug' => 'essonne',
            ),
            253 => 
            array (
                'code_departement' => 91,
                'cp' => 91400,
                'id' => 774,
                'insee' => 91534,
                'name' => 'SACLAY',
                'slug' => 'essonne',
            ),
            254 => 
            array (
                'code_departement' => 91,
                'cp' => 91530,
                'id' => 775,
                'insee' => 91540,
                'name' => 'ST CHERON',
                'slug' => 'essonne',
            ),
            255 => 
            array (
                'code_departement' => 91,
                'cp' => 91250,
                'id' => 776,
                'insee' => 91553,
                'name' => 'ST GERMAIN LES CORBEIL',
                'slug' => 'essonne',
            ),
            256 => 
            array (
                'code_departement' => 91,
                'cp' => 91250,
                'id' => 778,
                'insee' => 91577,
                'name' => 'SAINTRY SUR SEINE',
                'slug' => 'essonne',
            ),
            257 => 
            array (
                'code_departement' => 91,
                'cp' => 91770,
                'id' => 779,
                'insee' => 91579,
                'name' => 'ST VRAIN',
                'slug' => 'essonne',
            ),
            258 => 
            array (
                'code_departement' => 91,
                'cp' => 91160,
                'id' => 780,
                'insee' => 91587,
                'name' => 'SAULX LES CHARTREUX',
                'slug' => 'essonne',
            ),
            259 => 
            array (
                'code_departement' => 91,
                'cp' => 91530,
                'id' => 781,
                'insee' => 91593,
                'name' => 'SERMAISE',
                'slug' => 'essonne',
            ),
            260 => 
            array (
                'code_departement' => 91,
                'cp' => 91740,
                'id' => 782,
                'insee' => 91613,
                'name' => 'CONGERVILLE THIONVILLE',
                'slug' => 'essonne',
            ),
            261 => 
            array (
                'code_departement' => 91,
                'cp' => 91720,
                'id' => 784,
                'insee' => 91629,
                'name' => 'VALPUISEAUX',
                'slug' => 'essonne',
            ),
            262 => 
            array (
                'code_departement' => 91,
                'cp' => 91890,
                'id' => 785,
                'insee' => 91654,
                'name' => 'VIDELLES',
                'slug' => 'essonne',
            ),
            263 => 
            array (
                'code_departement' => 91,
                'cp' => 91190,
                'id' => 786,
                'insee' => 91679,
                'name' => 'VILLIERS LE BACLE',
                'slug' => 'essonne',
            ),
            264 => 
            array (
                'code_departement' => 91,
                'cp' => 91940,
                'id' => 787,
                'insee' => 91692,
                'name' => 'LES ULIS',
                'slug' => 'essonne',
            ),
            265 => 
            array (
                'code_departement' => 92,
                'cp' => 92140,
                'id' => 788,
                'insee' => 92023,
                'name' => 'CLAMART',
                'slug' => 'hauts_de_seine',
            ),
            266 => 
            array (
                'code_departement' => 92,
                'cp' => 92230,
                'id' => 789,
                'insee' => 92036,
                'name' => 'GENNEVILLIERS',
                'slug' => 'hauts_de_seine',
            ),
            267 => 
            array (
                'code_departement' => 92,
                'cp' => 92360,
                'id' => 790,
                'insee' => 92048,
                'name' => 'MEUDON',
                'slug' => 'hauts_de_seine',
            ),
            268 => 
            array (
                'code_departement' => 92,
                'cp' => 92000,
                'id' => 791,
                'insee' => 92050,
                'name' => 'NANTERRE',
                'slug' => 'hauts_de_seine',
            ),
            269 => 
            array (
                'code_departement' => 93,
                'cp' => 93000,
                'id' => 793,
                'insee' => 93008,
                'name' => 'BOBIGNY',
                'slug' => 'seine_saint_denis',
            ),
            270 => 
            array (
                'code_departement' => 93,
                'cp' => 93260,
                'id' => 794,
                'insee' => 93045,
                'name' => 'LES LILAS',
                'slug' => 'seine_saint_denis',
            ),
            271 => 
            array (
                'code_departement' => 93,
                'cp' => 93380,
                'id' => 795,
                'insee' => 93059,
                'name' => 'PIERREFITTE SUR SEINE',
                'slug' => 'seine_saint_denis',
            ),
            272 => 
            array (
                'code_departement' => 93,
                'cp' => 93270,
                'id' => 796,
                'insee' => 93071,
                'name' => 'SEVRAN',
                'slug' => 'seine_saint_denis',
            ),
            273 => 
            array (
                'code_departement' => 93,
                'cp' => 93430,
                'id' => 797,
                'insee' => 93079,
                'name' => 'VILLETANEUSE',
                'slug' => 'seine_saint_denis',
            ),
            274 => 
            array (
                'code_departement' => 94,
                'cp' => 94140,
                'id' => 798,
                'insee' => 94002,
                'name' => 'ALFORTVILLE',
                'slug' => 'val_de_marne',
            ),
            275 => 
            array (
                'code_departement' => 94,
                'cp' => 94230,
                'id' => 799,
                'insee' => 94016,
                'name' => 'CACHAN',
                'slug' => 'val_de_marne',
            ),
            276 => 
            array (
                'code_departement' => 94,
                'cp' => 94220,
                'id' => 800,
                'insee' => 94018,
                'name' => 'CHARENTON LE PONT',
                'slug' => 'val_de_marne',
            ),
            277 => 
            array (
                'code_departement' => 94,
                'cp' => 94000,
                'id' => 801,
                'insee' => 94028,
                'name' => 'CRETEIL',
                'slug' => 'val_de_marne',
            ),
            278 => 
            array (
                'code_departement' => 94,
                'cp' => 94250,
                'id' => 802,
                'insee' => 94037,
                'name' => 'GENTILLY',
                'slug' => 'val_de_marne',
            ),
            279 => 
            array (
                'code_departement' => 94,
                'cp' => 94880,
                'id' => 803,
                'insee' => 94053,
                'name' => 'NOISEAU',
                'slug' => 'val_de_marne',
            ),
            280 => 
            array (
                'code_departement' => 94,
                'cp' => 94510,
                'id' => 804,
                'insee' => 94060,
                'name' => 'LA QUEUE EN BRIE',
                'slug' => 'val_de_marne',
            ),
            281 => 
            array (
                'code_departement' => 94,
                'cp' => 94150,
                'id' => 805,
                'insee' => 94065,
                'name' => 'RUNGIS',
                'slug' => 'val_de_marne',
            ),
            282 => 
            array (
                'code_departement' => 94,
                'cp' => 94460,
                'id' => 806,
                'insee' => 94074,
                'name' => 'VALENTON',
                'slug' => 'val_de_marne',
            ),
            283 => 
            array (
                'code_departement' => 94,
                'cp' => 94300,
                'id' => 807,
                'insee' => 94080,
                'name' => 'VINCENNES',
                'slug' => 'val_de_marne',
            ),
            284 => 
            array (
                'code_departement' => 94,
                'cp' => 94400,
                'id' => 808,
                'insee' => 94081,
                'name' => 'VITRY SUR SEINE',
                'slug' => 'val_de_marne',
            ),
            285 => 
            array (
                'code_departement' => 95,
                'cp' => 95100,
                'id' => 809,
                'insee' => 95018,
                'name' => 'ARGENTEUIL',
                'slug' => 'val_doise',
            ),
            286 => 
            array (
                'code_departement' => 95,
                'cp' => 95270,
                'id' => 810,
                'insee' => 95026,
                'name' => 'ASNIERES SUR OISE',
                'slug' => 'val_doise',
            ),
            287 => 
            array (
                'code_departement' => 95,
                'cp' => 95250,
                'id' => 811,
                'insee' => 95051,
                'name' => 'BEAUCHAMP',
                'slug' => 'val_doise',
            ),
            288 => 
            array (
                'code_departement' => 95,
                'cp' => 95340,
                'id' => 812,
                'insee' => 95058,
                'name' => 'BERNES SUR OISE',
                'slug' => 'val_doise',
            ),
            289 => 
            array (
                'code_departement' => 95,
                'cp' => 95720,
                'id' => 813,
                'insee' => 95094,
                'name' => 'BOUQUEVAL',
                'slug' => 'val_doise',
            ),
            290 => 
            array (
                'code_departement' => 95,
                'cp' => 95710,
                'id' => 814,
                'insee' => 95101,
                'name' => 'BRAY ET LU',
                'slug' => 'val_doise',
            ),
            291 => 
            array (
                'code_departement' => 95,
                'cp' => 95640,
                'id' => 815,
                'insee' => 95102,
                'name' => 'BREANCON',
                'slug' => 'val_doise',
            ),
            292 => 
            array (
                'code_departement' => 95,
                'cp' => 95820,
                'id' => 816,
                'insee' => 95116,
                'name' => 'BRUYERES SUR OISE',
                'slug' => 'val_doise',
            ),
            293 => 
            array (
                'code_departement' => 95,
                'cp' => 95770,
                'id' => 817,
                'insee' => 95119,
                'name' => 'BUHY',
                'slug' => 'val_doise',
            ),
            294 => 
            array (
                'code_departement' => 95,
                'cp' => 95750,
                'id' => 818,
                'insee' => 95142,
                'name' => 'CHARS',
                'slug' => 'val_doise',
            ),
            295 => 
            array (
                'code_departement' => 95,
                'cp' => 95190,
                'id' => 819,
                'insee' => 95144,
                'name' => 'CHATENAY EN FRANCE',
                'slug' => 'val_doise',
            ),
            296 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 820,
                'insee' => 95166,
                'name' => 'CLERY EN VEXIN',
                'slug' => 'val_doise',
            ),
            297 => 
            array (
                'code_departement' => 95,
                'cp' => 95380,
                'id' => 821,
                'insee' => 95212,
                'name' => 'EPIAIS LES LOUVRES',
                'slug' => 'val_doise',
            ),
            298 => 
            array (
                'code_departement' => 95,
                'cp' => 95610,
                'id' => 822,
                'insee' => 95218,
                'name' => 'ERAGNY',
                'slug' => 'val_doise',
            ),
            299 => 
            array (
                'code_departement' => 95,
                'cp' => 95470,
                'id' => 823,
                'insee' => 95250,
                'name' => 'FOSSES',
                'slug' => 'val_doise',
            ),
            300 => 
            array (
                'code_departement' => 95,
                'cp' => 95640,
                'id' => 824,
                'insee' => 95298,
                'name' => 'HARAVILLIERS',
                'slug' => 'val_doise',
            ),
            301 => 
            array (
                'code_departement' => 95,
                'cp' => 95780,
                'id' => 825,
                'insee' => 95301,
                'name' => 'HAUTE ISLE',
                'slug' => 'val_doise',
            ),
            302 => 
            array (
                'code_departement' => 95,
                'cp' => 95270,
                'id' => 827,
                'insee' => 95331,
                'name' => 'LASSY',
                'slug' => 'val_doise',
            ),
            303 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 828,
                'insee' => 95348,
                'name' => 'LONGUESSE',
                'slug' => 'val_doise',
            ),
            304 => 
            array (
                'code_departement' => 95,
                'cp' => 95630,
                'id' => 829,
                'insee' => 95392,
                'name' => 'MERIEL',
                'slug' => 'val_doise',
            ),
            305 => 
            array (
                'code_departement' => 95,
                'cp' => 95570,
                'id' => 830,
                'insee' => 95409,
                'name' => 'MOISSELLES',
                'slug' => 'val_doise',
            ),
            306 => 
            array (
                'code_departement' => 95,
                'cp' => 95770,
                'id' => 831,
                'insee' => 95429,
                'name' => 'MONTREUIL SUR EPTE',
                'slug' => 'val_doise',
            ),
            307 => 
            array (
                'code_departement' => 95,
                'cp' => 95560,
                'id' => 832,
                'insee' => 95430,
                'name' => 'MONTSOULT',
                'slug' => 'val_doise',
            ),
            308 => 
            array (
                'code_departement' => 95,
                'cp' => 95590,
                'id' => 833,
                'insee' => 95452,
                'name' => 'NOINTEL',
                'slug' => 'val_doise',
            ),
            309 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 834,
                'insee' => 95483,
                'name' => 'LE PERCHAY',
                'slug' => 'val_doise',
            ),
            310 => 
            array (
                'code_departement' => 95,
                'cp' => 95350,
                'id' => 835,
                'insee' => 95489,
                'name' => 'PISCOP',
                'slug' => 'val_doise',
            ),
            311 => 
            array (
                'code_departement' => 95,
                'cp' => 95000,
                'id' => 836,
                'insee' => 95500,
                'name' => 'PONTOISE',
                'slug' => 'val_doise',
            ),
            312 => 
            array (
                'code_departement' => 95,
                'cp' => 95780,
                'id' => 837,
                'insee' => 95523,
                'name' => 'LA ROCHE GUYON',
                'slug' => 'val_doise',
            ),
            313 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 838,
                'insee' => 95535,
                'name' => 'SAGY',
                'slug' => 'val_doise',
            ),
            314 => 
            array (
                'code_departement' => 95,
                'cp' => 95770,
                'id' => 839,
                'insee' => 95541,
                'name' => 'ST CLAIR SUR EPTE',
                'slug' => 'val_doise',
            ),
            315 => 
            array (
                'code_departement' => 95,
                'cp' => 95270,
                'id' => 840,
                'insee' => 95566,
                'name' => 'ST MARTIN DU TERTRE',
                'slug' => 'val_doise',
            ),
            316 => 
            array (
                'code_departement' => 95,
                'cp' => 95270,
                'id' => 841,
                'insee' => 95594,
                'name' => 'SEUGY',
                'slug' => 'val_doise',
            ),
            317 => 
            array (
                'code_departement' => 95,
                'cp' => 95150,
                'id' => 842,
                'insee' => 95607,
                'name' => 'TAVERNY',
                'slug' => 'val_doise',
            ),
            318 => 
            array (
                'code_departement' => 95,
                'cp' => 95810,
                'id' => 843,
                'insee' => 95627,
                'name' => 'VALLANGOUJARD',
                'slug' => 'val_doise',
            ),
            319 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 844,
                'insee' => 95658,
                'name' => 'VIGNY',
                'slug' => 'val_doise',
            ),
            320 => 
            array (
                'code_departement' => 95,
                'cp' => 95400,
                'id' => 845,
                'insee' => 95680,
                'name' => 'VILLIERS LE BEL',
                'slug' => 'val_doise',
            ),
            321 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 846,
                'insee' => 95690,
                'name' => 'WY DIT JOLI VILLAGE',
                'slug' => 'val_doise',
            ),
            322 => 
            array (
                'code_departement' => 75,
                'cp' => 75004,
                'id' => 847,
                'insee' => 75104,
                'name' => 'PARIS 04',
                'slug' => 'paris',
            ),
            323 => 
            array (
                'code_departement' => 75,
                'cp' => 75007,
                'id' => 848,
                'insee' => 75107,
                'name' => 'PARIS 07',
                'slug' => 'paris',
            ),
            324 => 
            array (
                'code_departement' => 75,
                'cp' => 75011,
                'id' => 849,
                'insee' => 75111,
                'name' => 'PARIS 11',
                'slug' => 'paris',
            ),
            325 => 
            array (
                'code_departement' => 75,
                'cp' => 75015,
                'id' => 850,
                'insee' => 75115,
                'name' => 'PARIS 15',
                'slug' => 'paris',
            ),
            326 => 
            array (
                'code_departement' => 75,
                'cp' => 75116,
                'id' => 851,
                'insee' => 75116,
                'name' => 'PARIS 16',
                'slug' => 'paris',
            ),
            327 => 
            array (
                'code_departement' => 75,
                'cp' => 75020,
                'id' => 852,
                'insee' => 75120,
                'name' => 'PARIS 20',
                'slug' => 'paris',
            ),
            328 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 853,
                'insee' => 77001,
                'name' => 'ACHERES LA FORET',
                'slug' => 'seine_et_marne',
            ),
            329 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 854,
                'insee' => 77003,
                'name' => 'AMPONVILLE',
                'slug' => 'seine_et_marne',
            ),
            330 => 
            array (
                'code_departement' => 77,
                'cp' => 77390,
                'id' => 855,
                'insee' => 77004,
                'name' => 'ANDREZEL',
                'slug' => 'seine_et_marne',
            ),
            331 => 
            array (
                'code_departement' => 77,
                'cp' => 77560,
                'id' => 856,
                'insee' => 77012,
                'name' => 'AUGERS EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            332 => 
            array (
                'code_departement' => 77,
                'cp' => 77210,
                'id' => 857,
                'insee' => 77014,
                'name' => 'AVON',
                'slug' => 'seine_et_marne',
            ),
            333 => 
            array (
                'code_departement' => 77,
                'cp' => 77390,
                'id' => 858,
                'insee' => 77029,
                'name' => 'BEAUVOIR',
                'slug' => 'seine_et_marne',
            ),
            334 => 
            array (
                'code_departement' => 77,
                'cp' => 77970,
                'id' => 859,
                'insee' => 77036,
                'name' => 'BOISDON',
                'slug' => 'seine_et_marne',
            ),
            335 => 
            array (
                'code_departement' => 77,
                'cp' => 77169,
                'id' => 860,
                'insee' => 77042,
                'name' => 'BOISSY LE CHATEL',
                'slug' => 'seine_et_marne',
            ),
            336 => 
            array (
                'code_departement' => 77,
                'cp' => 77750,
                'id' => 861,
                'insee' => 77043,
                'name' => 'BOITRON',
                'slug' => 'seine_et_marne',
            ),
            337 => 
            array (
                'code_departement' => 77,
                'cp' => 77780,
                'id' => 862,
                'insee' => 77048,
                'name' => 'BOURRON MARLOTTE',
                'slug' => 'seine_et_marne',
            ),
            338 => 
            array (
                'code_departement' => 77,
                'cp' => 77480,
                'id' => 863,
                'insee' => 77051,
                'name' => 'BRAY SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            339 => 
            array (
                'code_departement' => 77,
                'cp' => 77940,
                'id' => 864,
                'insee' => 77054,
                'name' => 'LA BROSSE MONTCEAUX',
                'slug' => 'seine_et_marne',
            ),
            340 => 
            array (
                'code_departement' => 77,
                'cp' => 77177,
                'id' => 865,
                'insee' => 77055,
                'name' => 'BROU SUR CHANTEREINE',
                'slug' => 'seine_et_marne',
            ),
            341 => 
            array (
                'code_departement' => 77,
                'cp' => 77130,
                'id' => 866,
                'insee' => 77061,
                'name' => 'CANNES ECLUSE',
                'slug' => 'seine_et_marne',
            ),
            342 => 
            array (
                'code_departement' => 77,
                'cp' => 77515,
                'id' => 867,
                'insee' => 77063,
                'name' => 'LA CELLE SUR MORIN',
                'slug' => 'seine_et_marne',
            ),
            343 => 
            array (
                'code_departement' => 77,
                'cp' => 77650,
                'id' => 868,
                'insee' => 77076,
                'name' => 'CHALMAISON',
                'slug' => 'seine_et_marne',
            ),
            344 => 
            array (
                'code_departement' => 77,
                'cp' => 77560,
                'id' => 869,
                'insee' => 77080,
                'name' => 'CHAMPCENEST',
                'slug' => 'seine_et_marne',
            ),
            345 => 
            array (
                'code_departement' => 77,
                'cp' => 77720,
                'id' => 870,
                'insee' => 77082,
                'name' => 'CHAMPEAUX',
                'slug' => 'seine_et_marne',
            ),
            346 => 
            array (
                'code_departement' => 77,
                'cp' => 77420,
                'id' => 871,
                'insee' => 77083,
                'name' => 'CHAMPS SUR MARNE',
                'slug' => 'seine_et_marne',
            ),
            347 => 
            array (
                'code_departement' => 77,
                'cp' => 77370,
                'id' => 872,
                'insee' => 77089,
                'name' => 'LA CHAPELLE RABLAIS',
                'slug' => 'seine_et_marne',
            ),
            348 => 
            array (
                'code_departement' => 77,
                'cp' => 77610,
                'id' => 873,
                'insee' => 77091,
                'name' => 'LES CHAPELLES BOURBON',
                'slug' => 'seine_et_marne',
            ),
            349 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 874,
                'insee' => 77093,
                'name' => 'LA CHAPELLE MOUTILS',
                'slug' => 'seine_et_marne',
            ),
            350 => 
            array (
                'code_departement' => 77,
                'cp' => 77410,
                'id' => 875,
                'insee' => 77094,
                'name' => 'CHARMENTRAY',
                'slug' => 'seine_et_marne',
            ),
            351 => 
            array (
                'code_departement' => 77,
                'cp' => 77570,
                'id' => 876,
                'insee' => 77099,
                'name' => 'CHATEAU LANDON',
                'slug' => 'seine_et_marne',
            ),
            352 => 
            array (
                'code_departement' => 77,
                'cp' => 77820,
                'id' => 877,
                'insee' => 77103,
                'name' => 'CHATILLON LA BORDE',
                'slug' => 'seine_et_marne',
            ),
            353 => 
            array (
                'code_departement' => 77,
                'cp' => 77610,
                'id' => 878,
                'insee' => 77104,
                'name' => 'CHATRES',
                'slug' => 'seine_et_marne',
            ),
            354 => 
            array (
                'code_departement' => 77,
                'cp' => 77173,
                'id' => 880,
                'insee' => 77114,
                'name' => 'CHEVRY COSSIGNY',
                'slug' => 'seine_et_marne',
            ),
            355 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 881,
                'insee' => 77116,
                'name' => 'CHOISY EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            356 => 
            array (
                'code_departement' => 77,
                'cp' => 77290,
                'id' => 882,
                'insee' => 77123,
                'name' => 'COMPANS',
                'slug' => 'seine_et_marne',
            ),
            357 => 
            array (
                'code_departement' => 77,
                'cp' => 77600,
                'id' => 883,
                'insee' => 77124,
                'name' => 'CONCHES SUR GONDOIRE',
                'slug' => 'seine_et_marne',
            ),
            358 => 
            array (
                'code_departement' => 77,
                'cp' => 77860,
                'id' => 884,
                'insee' => 77128,
                'name' => 'COUILLY PONT AUX DAMES',
                'slug' => 'seine_et_marne',
            ),
            359 => 
            array (
                'code_departement' => 77,
                'cp' => 77183,
                'id' => 885,
                'insee' => 77146,
                'name' => 'CROISSY BEAUBOURG',
                'slug' => 'seine_et_marne',
            ),
            360 => 
            array (
                'code_departement' => 77,
                'cp' => 77190,
                'id' => 887,
                'insee' => 77152,
                'name' => 'DAMMARIE LES LYS',
                'slug' => 'seine_et_marne',
            ),
            361 => 
            array (
                'code_departement' => 77,
                'cp' => 77230,
                'id' => 888,
                'insee' => 77153,
                'name' => 'DAMMARTIN EN GOELE',
                'slug' => 'seine_et_marne',
            ),
            362 => 
            array (
                'code_departement' => 77,
                'cp' => 77400,
                'id' => 889,
                'insee' => 77155,
                'name' => 'DAMPMART',
                'slug' => 'seine_et_marne',
            ),
            363 => 
            array (
                'code_departement' => 77,
                'cp' => 77520,
                'id' => 890,
                'insee' => 77159,
                'name' => 'DONNEMARIE DONTILLY',
                'slug' => 'seine_et_marne',
            ),
            364 => 
            array (
                'code_departement' => 77,
                'cp' => 77139,
                'id' => 891,
                'insee' => 77163,
                'name' => 'DOUY LA RAMEE',
                'slug' => 'seine_et_marne',
            ),
            365 => 
            array (
                'code_departement' => 77,
                'cp' => 77515,
                'id' => 893,
                'insee' => 77176,
                'name' => 'FAREMOUTIERS',
                'slug' => 'seine_et_marne',
            ),
            366 => 
            array (
                'code_departement' => 77,
                'cp' => 77133,
                'id' => 894,
                'insee' => 77179,
                'name' => 'FERICY',
                'slug' => 'seine_et_marne',
            ),
            367 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 895,
                'insee' => 77182,
                'name' => 'LA FERTE GAUCHER',
                'slug' => 'seine_et_marne',
            ),
            368 => 
            array (
                'code_departement' => 77,
                'cp' => 77370,
                'id' => 896,
                'insee' => 77191,
                'name' => 'FONTENAILLES',
                'slug' => 'seine_et_marne',
            ),
            369 => 
            array (
                'code_departement' => 77,
                'cp' => 77690,
                'id' => 897,
                'insee' => 77202,
                'name' => 'LA GENEVRAYE',
                'slug' => 'seine_et_marne',
            ),
            370 => 
            array (
                'code_departement' => 77,
                'cp' => 77840,
                'id' => 898,
                'insee' => 77204,
                'name' => 'GERMIGNY SOUS COULOMBS',
                'slug' => 'seine_et_marne',
            ),
            371 => 
            array (
                'code_departement' => 77,
                'cp' => 77114,
                'id' => 899,
                'insee' => 77208,
                'name' => 'GOUAIX',
                'slug' => 'seine_et_marne',
            ),
            372 => 
            array (
                'code_departement' => 77,
                'cp' => 77130,
                'id' => 900,
                'insee' => 77210,
                'name' => 'LA GRANDE PAROISSE',
                'slug' => 'seine_et_marne',
            ),
            373 => 
            array (
                'code_departement' => 77,
                'cp' => 77480,
                'id' => 902,
                'insee' => 77218,
                'name' => 'GRISY SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            374 => 
            array (
                'code_departement' => 77,
                'cp' => 77580,
                'id' => 903,
                'insee' => 77219,
                'name' => 'GUERARD',
                'slug' => 'seine_et_marne',
            ),
            375 => 
            array (
                'code_departement' => 77,
                'cp' => 77510,
                'id' => 905,
                'insee' => 77228,
                'name' => 'HONDEVILLIERS',
                'slug' => 'seine_et_marne',
            ),
            376 => 
            array (
                'code_departement' => 77,
                'cp' => 77450,
                'id' => 906,
                'insee' => 77232,
                'name' => 'ISLES LES VILLENOY',
                'slug' => 'seine_et_marne',
            ),
            377 => 
            array (
                'code_departement' => 77,
                'cp' => 77970,
                'id' => 907,
                'insee' => 77239,
                'name' => 'JOUY LE CHATEL',
                'slug' => 'seine_et_marne',
            ),
            378 => 
            array (
                'code_departement' => 77,
                'cp' => 77148,
                'id' => 908,
                'insee' => 77245,
                'name' => 'LAVAL EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            379 => 
            array (
                'code_departement' => 77,
                'cp' => 77127,
                'id' => 909,
                'insee' => 77251,
                'name' => 'LIEUSAINT',
                'slug' => 'seine_et_marne',
            ),
            380 => 
            array (
                'code_departement' => 77,
                'cp' => 77700,
                'id' => 912,
                'insee' => 77268,
                'name' => 'MAGNY LE HONGRE',
                'slug' => 'seine_et_marne',
            ),
            381 => 
            array (
                'code_departement' => 77,
                'cp' => 77139,
                'id' => 913,
                'insee' => 77274,
                'name' => 'MARCILLY',
                'slug' => 'seine_et_marne',
            ),
            382 => 
            array (
                'code_departement' => 77,
                'cp' => 77990,
                'id' => 914,
                'insee' => 77282,
                'name' => 'MAUREGARD',
                'slug' => 'seine_et_marne',
            ),
            383 => 
            array (
                'code_departement' => 77,
                'cp' => 77100,
                'id' => 915,
                'insee' => 77284,
                'name' => 'MEAUX',
                'slug' => 'seine_et_marne',
            ),
            384 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 916,
                'insee' => 77287,
                'name' => 'MEILLERAY',
                'slug' => 'seine_et_marne',
            ),
            385 => 
            array (
                'code_departement' => 77,
                'cp' => 77940,
                'id' => 917,
                'insee' => 77313,
                'name' => 'MONTMACHOUX',
                'slug' => 'seine_et_marne',
            ),
            386 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 918,
                'insee' => 77314,
                'name' => 'MONTOLIVET',
                'slug' => 'seine_et_marne',
            ),
            387 => 
            array (
                'code_departement' => 77,
                'cp' => 77230,
                'id' => 920,
                'insee' => 77322,
                'name' => 'MOUSSY LE NEUF',
                'slug' => 'seine_et_marne',
            ),
            388 => 
            array (
                'code_departement' => 77,
                'cp' => 77230,
                'id' => 921,
                'insee' => 77323,
                'name' => 'MOUSSY LE VIEUX',
                'slug' => 'seine_et_marne',
            ),
            389 => 
            array (
                'code_departement' => 77,
                'cp' => 77480,
                'id' => 922,
                'insee' => 77325,
                'name' => 'MOUY SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            390 => 
            array (
                'code_departement' => 77,
                'cp' => 77710,
                'id' => 923,
                'insee' => 77329,
                'name' => 'NANTEAU SUR LUNAIN',
                'slug' => 'seine_et_marne',
            ),
            391 => 
            array (
                'code_departement' => 77,
                'cp' => 77100,
                'id' => 924,
                'insee' => 77330,
                'name' => 'NANTEUIL LES MEAUX',
                'slug' => 'seine_et_marne',
            ),
            392 => 
            array (
                'code_departement' => 77,
                'cp' => 77124,
                'id' => 925,
                'insee' => 77335,
                'name' => 'CHAUCONIN NEUFMONTIERS',
                'slug' => 'seine_et_marne',
            ),
            393 => 
            array (
                'code_departement' => 77,
                'cp' => 77750,
                'id' => 926,
                'insee' => 77345,
                'name' => 'ORLY SUR MORIN',
                'slug' => 'seine_et_marne',
            ),
            394 => 
            array (
                'code_departement' => 77,
                'cp' => 77930,
                'id' => 927,
                'insee' => 77359,
                'name' => 'PERTHES',
                'slug' => 'seine_et_marne',
            ),
            395 => 
            array (
                'code_departement' => 77,
                'cp' => 77580,
                'id' => 928,
                'insee' => 77361,
                'name' => 'PIERRE LEVEE',
                'slug' => 'seine_et_marne',
            ),
            396 => 
            array (
                'code_departement' => 77,
                'cp' => 77135,
                'id' => 929,
                'insee' => 77374,
                'name' => 'PONTCARRE',
                'slug' => 'seine_et_marne',
            ),
            397 => 
            array (
                'code_departement' => 77,
                'cp' => 77720,
                'id' => 930,
                'insee' => 77381,
                'name' => 'QUIERS',
                'slug' => 'seine_et_marne',
            ),
            398 => 
            array (
                'code_departement' => 77,
                'cp' => 77000,
                'id' => 931,
                'insee' => 77389,
                'name' => 'LA ROCHETTE',
                'slug' => 'seine_et_marne',
            ),
            399 => 
            array (
                'code_departement' => 77,
                'cp' => 77230,
                'id' => 932,
                'insee' => 77392,
                'name' => 'ROUVRES',
                'slug' => 'seine_et_marne',
            ),
            400 => 
            array (
                'code_departement' => 77,
                'cp' => 77560,
                'id' => 933,
                'insee' => 77396,
                'name' => 'RUPEREUX',
                'slug' => 'seine_et_marne',
            ),
            401 => 
            array (
                'code_departement' => 77,
                'cp' => 77930,
                'id' => 935,
                'insee' => 77412,
                'name' => 'ST GERMAIN SUR ECOLE',
                'slug' => 'seine_et_marne',
            ),
            402 => 
            array (
                'code_departement' => 77,
                'cp' => 77860,
                'id' => 936,
                'insee' => 77413,
                'name' => 'ST GERMAIN SUR MORIN',
                'slug' => 'seine_et_marne',
            ),
            403 => 
            array (
                'code_departement' => 77,
                'cp' => 77160,
                'id' => 937,
                'insee' => 77414,
                'name' => 'ST HILLIERS',
                'slug' => 'seine_et_marne',
            ),
            404 => 
            array (
                'code_departement' => 77,
                'cp' => 77510,
                'id' => 938,
                'insee' => 77417,
                'name' => 'ST LEGER',
                'slug' => 'seine_et_marne',
            ),
            405 => 
            array (
                'code_departement' => 77,
                'cp' => 77670,
                'id' => 939,
                'insee' => 77419,
                'name' => 'ST MAMMES',
                'slug' => 'seine_et_marne',
            ),
            406 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 941,
                'insee' => 77432,
                'name' => 'ST REMY LA VANNE',
                'slug' => 'seine_et_marne',
            ),
            407 => 
            array (
                'code_departement' => 77,
                'cp' => 77120,
                'id' => 942,
                'insee' => 77433,
                'name' => 'BEAUTHEIL SAINTS',
                'slug' => 'seine_et_marne',
            ),
            408 => 
            array (
                'code_departement' => 77,
                'cp' => 77480,
                'id' => 943,
                'insee' => 77434,
                'name' => 'ST SAUVEUR LES BRAY',
                'slug' => 'seine_et_marne',
            ),
            409 => 
            array (
                'code_departement' => 77,
                'cp' => 77930,
                'id' => 944,
                'insee' => 77435,
                'name' => 'ST SAUVEUR SUR ECOLE',
                'slug' => 'seine_et_marne',
            ),
            410 => 
            array (
                'code_departement' => 77,
                'cp' => 77260,
                'id' => 945,
                'insee' => 77440,
                'name' => 'SAMMERON',
                'slug' => 'seine_et_marne',
            ),
            411 => 
            array (
                'code_departement' => 77,
                'cp' => 77210,
                'id' => 946,
                'insee' => 77442,
                'name' => 'SAMOREAU',
                'slug' => 'seine_et_marne',
            ),
            412 => 
            array (
                'code_departement' => 77,
                'cp' => 77260,
                'id' => 947,
                'insee' => 77448,
                'name' => 'SEPT SORTS',
                'slug' => 'seine_et_marne',
            ),
            413 => 
            array (
                'code_departement' => 77,
                'cp' => 77111,
                'id' => 948,
                'insee' => 77457,
                'name' => 'SOLERS',
                'slug' => 'seine_et_marne',
            ),
            414 => 
            array (
                'code_departement' => 77,
                'cp' => 77171,
                'id' => 949,
                'insee' => 77459,
                'name' => 'SOURDUN',
                'slug' => 'seine_et_marne',
            ),
            415 => 
            array (
                'code_departement' => 77,
                'cp' => 77200,
                'id' => 950,
                'insee' => 77468,
                'name' => 'TORCY',
                'slug' => 'seine_et_marne',
            ),
            416 => 
            array (
                'code_departement' => 77,
                'cp' => 77220,
                'id' => 951,
                'insee' => 77470,
                'name' => 'TOURNAN EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            417 => 
            array (
                'code_departement' => 77,
                'cp' => 77123,
                'id' => 952,
                'insee' => 77485,
                'name' => 'LE VAUDOUE',
                'slug' => 'seine_et_marne',
            ),
            418 => 
            array (
                'code_departement' => 77,
                'cp' => 77141,
                'id' => 953,
                'insee' => 77486,
                'name' => 'VAUDOY EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            419 => 
            array (
                'code_departement' => 77,
                'cp' => 77710,
                'id' => 954,
                'insee' => 77489,
                'name' => 'VAUX SUR LUNAIN',
                'slug' => 'seine_et_marne',
            ),
            420 => 
            array (
                'code_departement' => 77,
                'cp' => 77124,
                'id' => 955,
                'insee' => 77513,
                'name' => 'VILLENOY',
                'slug' => 'seine_et_marne',
            ),
            421 => 
            array (
                'code_departement' => 77,
                'cp' => 77410,
                'id' => 956,
                'insee' => 77515,
                'name' => 'VILLEROY',
                'slug' => 'seine_et_marne',
            ),
            422 => 
            array (
                'code_departement' => 77,
                'cp' => 77560,
                'id' => 957,
                'insee' => 77519,
                'name' => 'VILLIERS ST GEORGES',
                'slug' => 'seine_et_marne',
            ),
            423 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 958,
                'insee' => 77520,
                'name' => 'VILLIERS SOUS GREZ',
                'slug' => 'seine_et_marne',
            ),
            424 => 
            array (
                'code_departement' => 77,
                'cp' => 77230,
                'id' => 959,
                'insee' => 77525,
                'name' => 'VINANTES',
                'slug' => 'seine_et_marne',
            ),
            425 => 
            array (
                'code_departement' => 77,
                'cp' => 77560,
                'id' => 960,
                'insee' => 77530,
                'name' => 'VOULTON',
                'slug' => 'seine_et_marne',
            ),
            426 => 
            array (
                'code_departement' => 77,
                'cp' => 77940,
                'id' => 961,
                'insee' => 77531,
                'name' => 'VOULX',
                'slug' => 'seine_et_marne',
            ),
            427 => 
            array (
                'code_departement' => 78,
                'cp' => 78660,
                'id' => 962,
                'insee' => 78003,
                'name' => 'ABLIS',
                'slug' => 'yvelines',
            ),
            428 => 
            array (
                'code_departement' => 78,
                'cp' => 78410,
                'id' => 963,
                'insee' => 78029,
                'name' => 'AUBERGENVILLE',
                'slug' => 'yvelines',
            ),
            429 => 
            array (
                'code_departement' => 78,
                'cp' => 78610,
                'id' => 964,
                'insee' => 78030,
                'name' => 'AUFFARGIS',
                'slug' => 'yvelines',
            ),
            430 => 
            array (
                'code_departement' => 78,
                'cp' => 78770,
                'id' => 965,
                'insee' => 78036,
                'name' => 'AUTOUILLET',
                'slug' => 'yvelines',
            ),
            431 => 
            array (
                'code_departement' => 78,
                'cp' => 78870,
                'id' => 966,
                'insee' => 78043,
                'name' => 'BAILLY',
                'slug' => 'yvelines',
            ),
            432 => 
            array (
                'code_departement' => 78,
                'cp' => 78270,
                'id' => 967,
                'insee' => 78057,
                'name' => 'BENNECOURT',
                'slug' => 'yvelines',
            ),
            433 => 
            array (
                'code_departement' => 78,
                'cp' => 78270,
                'id' => 968,
                'insee' => 78068,
                'name' => 'BLARU',
                'slug' => 'yvelines',
            ),
            434 => 
            array (
                'code_departement' => 78,
                'cp' => 78113,
                'id' => 969,
                'insee' => 78096,
                'name' => 'BOURDONNE',
                'slug' => 'yvelines',
            ),
            435 => 
            array (
                'code_departement' => 78,
                'cp' => 78980,
                'id' => 970,
                'insee' => 78107,
                'name' => 'BREVAL',
                'slug' => 'yvelines',
            ),
            436 => 
            array (
                'code_departement' => 78,
                'cp' => 78440,
                'id' => 971,
                'insee' => 78113,
                'name' => 'BRUEIL EN VEXIN',
                'slug' => 'yvelines',
            ),
            437 => 
            array (
                'code_departement' => 78,
                'cp' => 78530,
                'id' => 972,
                'insee' => 78117,
                'name' => 'BUC',
                'slug' => 'yvelines',
            ),
            438 => 
            array (
                'code_departement' => 78,
                'cp' => 78420,
                'id' => 973,
                'insee' => 78124,
                'name' => 'CARRIERES SUR SEINE',
                'slug' => 'yvelines',
            ),
            439 => 
            array (
                'code_departement' => 78,
                'cp' => 78170,
                'id' => 974,
                'insee' => 78126,
                'name' => 'LA CELLE ST CLOUD',
                'slug' => 'yvelines',
            ),
            440 => 
            array (
                'code_departement' => 78,
                'cp' => 78460,
                'id' => 976,
                'insee' => 78160,
                'name' => 'CHEVREUSE',
                'slug' => 'yvelines',
            ),
            441 => 
            array (
                'code_departement' => 78,
                'cp' => 78120,
                'id' => 977,
                'insee' => 78164,
                'name' => 'CLAIREFONTAINE EN YVELINES',
                'slug' => 'yvelines',
            ),
            442 => 
            array (
                'code_departement' => 78,
                'cp' => 78270,
                'id' => 978,
                'insee' => 78188,
                'name' => 'CRAVENT',
                'slug' => 'yvelines',
            ),
            443 => 
            array (
                'code_departement' => 78,
                'cp' => 78290,
                'id' => 979,
                'insee' => 78190,
                'name' => 'CROISSY SUR SEINE',
                'slug' => 'yvelines',
            ),
            444 => 
            array (
                'code_departement' => 78,
                'cp' => 78690,
                'id' => 980,
                'insee' => 78220,
                'name' => 'LES ESSARTS LE ROI',
                'slug' => 'yvelines',
            ),
            445 => 
            array (
                'code_departement' => 78,
                'cp' => 78200,
                'id' => 981,
                'insee' => 78231,
                'name' => 'FAVRIEUX',
                'slug' => 'yvelines',
            ),
            446 => 
            array (
                'code_departement' => 78,
                'cp' => 78200,
                'id' => 982,
                'insee' => 78234,
                'name' => 'FLACOURT',
                'slug' => 'yvelines',
            ),
            447 => 
            array (
                'code_departement' => 78,
                'cp' => 78250,
                'id' => 983,
                'insee' => 78261,
                'name' => 'GAILLON SUR MONTCIENT',
                'slug' => 'yvelines',
            ),
            448 => 
            array (
                'code_departement' => 78,
                'cp' => 78490,
                'id' => 984,
                'insee' => 78289,
                'name' => 'GROSROUVRE',
                'slug' => 'yvelines',
            ),
            449 => 
            array (
                'code_departement' => 78,
                'cp' => 78520,
                'id' => 985,
                'insee' => 78290,
                'name' => 'GUERNES',
                'slug' => 'yvelines',
            ),
            450 => 
            array (
                'code_departement' => 78,
                'cp' => 78113,
                'id' => 986,
                'insee' => 78302,
                'name' => 'LA HAUTEVILLE',
                'slug' => 'yvelines',
            ),
            451 => 
            array (
                'code_departement' => 78,
                'cp' => 78550,
                'id' => 987,
                'insee' => 78310,
                'name' => 'HOUDAN',
                'slug' => 'yvelines',
            ),
            452 => 
            array (
                'code_departement' => 78,
                'cp' => 78320,
                'id' => 988,
                'insee' => 78334,
                'name' => 'LEVIS ST NOM',
                'slug' => 'yvelines',
            ),
            453 => 
            array (
                'code_departement' => 78,
                'cp' => 78270,
                'id' => 989,
                'insee' => 78344,
                'name' => 'LOMMOYE',
                'slug' => 'yvelines',
            ),
            454 => 
            array (
                'code_departement' => 78,
                'cp' => 78430,
                'id' => 990,
                'insee' => 78350,
                'name' => 'LOUVECIENNES',
                'slug' => 'yvelines',
            ),
            455 => 
            array (
                'code_departement' => 78,
                'cp' => 78200,
                'id' => 991,
                'insee' => 78354,
                'name' => 'MAGNANVILLE',
                'slug' => 'yvelines',
            ),
            456 => 
            array (
                'code_departement' => 78,
                'cp' => 78770,
                'id' => 993,
                'insee' => 78364,
                'name' => 'MARCQ',
                'slug' => 'yvelines',
            ),
            457 => 
            array (
                'code_departement' => 78,
                'cp' => 78750,
                'id' => 994,
                'insee' => 78367,
                'name' => 'MAREIL MARLY',
                'slug' => 'yvelines',
            ),
            458 => 
            array (
                'code_departement' => 78,
                'cp' => 78780,
                'id' => 995,
                'insee' => 78382,
                'name' => 'MAURECOURT',
                'slug' => 'yvelines',
            ),
            459 => 
            array (
                'code_departement' => 78,
                'cp' => 78310,
                'id' => 996,
                'insee' => 78383,
                'name' => 'MAUREPAS',
                'slug' => 'yvelines',
            ),
            460 => 
            array (
                'code_departement' => 78,
                'cp' => 78125,
                'id' => 997,
                'insee' => 78407,
                'name' => 'MITTAINVILLE',
                'slug' => 'yvelines',
            ),
            461 => 
            array (
                'code_departement' => 78,
                'cp' => 78840,
                'id' => 998,
                'insee' => 78410,
                'name' => 'MOISSON',
                'slug' => 'yvelines',
            ),
            462 => 
            array (
                'code_departement' => 78,
                'cp' => 78980,
                'id' => 999,
                'insee' => 78413,
                'name' => 'MONDREVILLE',
                'slug' => 'yvelines',
            ),
            463 => 
            array (
                'code_departement' => 78,
                'cp' => 78360,
                'id' => 1000,
                'insee' => 78418,
                'name' => 'MONTESSON',
                'slug' => 'yvelines',
            ),
            464 => 
            array (
                'code_departement' => 78,
                'cp' => 78790,
                'id' => 1001,
                'insee' => 78439,
                'name' => 'MULCENT',
                'slug' => 'yvelines',
            ),
            465 => 
            array (
                'code_departement' => 78,
                'cp' => 78980,
                'id' => 1002,
                'insee' => 78444,
                'name' => 'NEAUPHLETTE',
                'slug' => 'yvelines',
            ),
            466 => 
            array (
                'code_departement' => 78,
                'cp' => 78590,
                'id' => 1003,
                'insee' => 78455,
                'name' => 'NOISY LE ROI',
                'slug' => 'yvelines',
            ),
            467 => 
            array (
                'code_departement' => 78,
                'cp' => 78630,
                'id' => 1004,
                'insee' => 78466,
                'name' => 'ORGEVAL',
                'slug' => 'yvelines',
            ),
            468 => 
            array (
                'code_departement' => 78,
                'cp' => 78125,
                'id' => 1005,
                'insee' => 78470,
                'name' => 'ORPHIN',
                'slug' => 'yvelines',
            ),
            469 => 
            array (
                'code_departement' => 78,
                'cp' => 78660,
                'id' => 1006,
                'insee' => 78478,
                'name' => 'PARAY DOUAVILLE',
                'slug' => 'yvelines',
            ),
            470 => 
            array (
                'code_departement' => 78,
                'cp' => 78610,
                'id' => 1007,
                'insee' => 78486,
                'name' => 'LE PERRAY EN YVELINES',
                'slug' => 'yvelines',
            ),
            471 => 
            array (
                'code_departement' => 78,
                'cp' => 78370,
                'id' => 1008,
                'insee' => 78490,
                'name' => 'PLAISIR',
                'slug' => 'yvelines',
            ),
            472 => 
            array (
                'code_departement' => 78,
                'cp' => 78910,
                'id' => 1009,
                'insee' => 78505,
                'name' => 'PRUNAY LE TEMPLE',
                'slug' => 'yvelines',
            ),
            473 => 
            array (
                'code_departement' => 78,
                'cp' => 78660,
                'id' => 1010,
                'insee' => 78506,
                'name' => 'PRUNAY EN YVELINES',
                'slug' => 'yvelines',
            ),
            474 => 
            array (
                'code_departement' => 78,
                'cp' => 78940,
                'id' => 1011,
                'insee' => 78513,
                'name' => 'LA QUEUE LES YVELINES',
                'slug' => 'yvelines',
            ),
            475 => 
            array (
                'code_departement' => 78,
                'cp' => 78730,
                'id' => 1012,
                'insee' => 78522,
                'name' => 'ROCHEFORT EN YVELINES',
                'slug' => 'yvelines',
            ),
            476 => 
            array (
                'code_departement' => 78,
                'cp' => 78790,
                'id' => 1013,
                'insee' => 78530,
                'name' => 'ROSAY',
                'slug' => 'yvelines',
            ),
            477 => 
            array (
                'code_departement' => 78,
                'cp' => 78710,
                'id' => 1014,
                'insee' => 78531,
                'name' => 'ROSNY SUR SEINE',
                'slug' => 'yvelines',
            ),
            478 => 
            array (
                'code_departement' => 78,
                'cp' => 78730,
                'id' => 1015,
                'insee' => 78537,
                'name' => 'ST ARNOULT EN YVELINES',
                'slug' => 'yvelines',
            ),
            479 => 
            array (
                'code_departement' => 78,
                'cp' => 78980,
                'id' => 1016,
                'insee' => 78558,
                'name' => 'ST ILLIERS LA VILLE',
                'slug' => 'yvelines',
            ),
            480 => 
            array (
                'code_departement' => 78,
                'cp' => 78660,
                'id' => 1017,
                'insee' => 78564,
                'name' => 'ST MARTIN DE BRETHENCOURT',
                'slug' => 'yvelines',
            ),
            481 => 
            array (
                'code_departement' => 78,
                'cp' => 78690,
                'id' => 1019,
                'insee' => 78576,
                'name' => 'ST REMY L HONORE',
                'slug' => 'yvelines',
            ),
            482 => 
            array (
                'code_departement' => 78,
                'cp' => 78200,
                'id' => 1020,
                'insee' => 78597,
                'name' => 'SOINDRES',
                'slug' => 'yvelines',
            ),
            483 => 
            array (
                'code_departement' => 78,
                'cp' => 78850,
                'id' => 1021,
                'insee' => 78615,
                'name' => 'THIVERVAL GRIGNON',
                'slug' => 'yvelines',
            ),
            484 => 
            array (
                'code_departement' => 78,
                'cp' => 78510,
                'id' => 1022,
                'insee' => 78624,
                'name' => 'TRIEL SUR SEINE',
                'slug' => 'yvelines',
            ),
            485 => 
            array (
                'code_departement' => 78,
                'cp' => 78320,
                'id' => 1024,
                'insee' => 78644,
                'name' => 'LA VERRIERE',
                'slug' => 'yvelines',
            ),
            486 => 
            array (
                'code_departement' => 78,
                'cp' => 78000,
                'id' => 1025,
                'insee' => 78646,
                'name' => 'VERSAILLES',
                'slug' => 'yvelines',
            ),
            487 => 
            array (
                'code_departement' => 78,
                'cp' => 78930,
                'id' => 1026,
                'insee' => 78647,
                'name' => 'VERT',
                'slug' => 'yvelines',
            ),
            488 => 
            array (
                'code_departement' => 78,
                'cp' => 78270,
                'id' => 1027,
                'insee' => 78668,
                'name' => 'LA VILLENEUVE EN CHEVRIE',
                'slug' => 'yvelines',
            ),
            489 => 
            array (
                'code_departement' => 78,
                'cp' => 78670,
                'id' => 1028,
                'insee' => 78672,
                'name' => 'VILLENNES SUR SEINE',
                'slug' => 'yvelines',
            ),
            490 => 
            array (
                'code_departement' => 78,
                'cp' => 78220,
                'id' => 1029,
                'insee' => 78686,
                'name' => 'VIROFLAY',
                'slug' => 'yvelines',
            ),
            491 => 
            array (
                'code_departement' => 78,
                'cp' => 7800,
                'id' => 1030,
                'insee' => 7055,
                'name' => 'CHARMES SUR RHONE',
                'slug' => 'yvelines',
            ),
            492 => 
            array (
                'code_departement' => 75,
                'cp' => 7530,
                'id' => 1031,
                'insee' => 7120,
                'name' => 'LACHAMP RAPHAEL',
                'slug' => 'paris',
            ),
            493 => 
            array (
                'code_departement' => 77,
                'cp' => 7790,
                'id' => 1032,
                'insee' => 7205,
                'name' => 'ST ALBAN D AY',
                'slug' => 'seine_et_marne',
            ),
            494 => 
            array (
                'code_departement' => 75,
                'cp' => 7590,
                'id' => 1033,
                'insee' => 7206,
                'name' => 'ST ALBAN EN MONTAGNE',
                'slug' => 'paris',
            ),
            495 => 
            array (
                'code_departement' => 75,
                'cp' => 7580,
                'id' => 1034,
                'insee' => 7242,
                'name' => 'ST GINEYS EN COIRON',
                'slug' => 'paris',
            ),
            496 => 
            array (
                'code_departement' => 75,
                'cp' => 7590,
                'id' => 1035,
                'insee' => 7262,
                'name' => 'ST LAURENT BAINS LAVAL D AURELLE',
                'slug' => 'paris',
            ),
            497 => 
            array (
                'code_departement' => 75,
                'cp' => 7580,
                'id' => 1036,
                'insee' => 7287,
                'name' => 'ST PONS',
                'slug' => 'paris',
            ),
            498 => 
            array (
                'code_departement' => 92,
                'cp' => 9200,
                'id' => 1037,
                'insee' => 9008,
                'name' => 'ALOS',
                'slug' => 'hauts_de_seine',
            ),
            499 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 1038,
                'insee' => 9015,
                'name' => 'ARIGNAC',
                'slug' => 'val_de_marne',
            ),
        ));
        \DB::table('ville')->insert(array (
            0 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 1039,
                'insee' => 9016,
                'name' => 'ARNAVE',
                'slug' => 'val_de_marne',
            ),
            1 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 1040,
                'insee' => 9021,
                'name' => 'ARTIX',
                'slug' => 'essonne',
            ),
            2 => 
            array (
                'code_departement' => 91,
                'cp' => 9140,
                'id' => 1041,
                'insee' => 9029,
                'name' => 'AULUS LES BAINS',
                'slug' => 'essonne',
            ),
            3 => 
            array (
                'code_departement' => 92,
                'cp' => 9230,
                'id' => 1042,
                'insee' => 9046,
                'name' => 'BEDEILLE',
                'slug' => 'hauts_de_seine',
            ),
            4 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 1043,
                'insee' => 9056,
                'name' => 'BEZAC',
                'slug' => 'essonne',
            ),
            5 => 
            array (
                'code_departement' => 93,
                'cp' => 9350,
                'id' => 1044,
                'insee' => 9061,
                'name' => 'LES BORDES SUR ARIZE',
                'slug' => 'seine_saint_denis',
            ),
            6 => 
            array (
                'code_departement' => 93,
                'cp' => 9310,
                'id' => 1045,
                'insee' => 9064,
                'name' => 'BOUAN',
                'slug' => 'seine_saint_denis',
            ),
            7 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 1046,
                'insee' => 9077,
                'name' => 'CAPOULET ET JUNAC',
                'slug' => 'val_de_marne',
            ),
            8 => 
            array (
                'code_departement' => 94,
                'cp' => 9460,
                'id' => 1047,
                'insee' => 9078,
                'name' => 'CARCANIERES',
                'slug' => 'val_de_marne',
            ),
            9 => 
            array (
                'code_departement' => 91,
                'cp' => 9130,
                'id' => 1048,
                'insee' => 9079,
                'name' => 'CARLA BAYLE',
                'slug' => 'essonne',
            ),
            10 => 
            array (
                'code_departement' => 91,
                'cp' => 9130,
                'id' => 1049,
                'insee' => 9083,
                'name' => 'CASTERAS',
                'slug' => 'essonne',
            ),
            11 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 1050,
                'insee' => 9092,
                'name' => 'CAZENAVE SERRES ET ALLENS',
                'slug' => 'val_de_marne',
            ),
            12 => 
            array (
                'code_departement' => 93,
                'cp' => 9310,
                'id' => 1051,
                'insee' => 9096,
                'name' => 'CHATEAU VERDUN',
                'slug' => 'seine_saint_denis',
            ),
            13 => 
            array (
                'code_departement' => 92,
                'cp' => 9230,
                'id' => 1052,
                'insee' => 9098,
                'name' => 'CONTRAZY',
                'slug' => 'hauts_de_seine',
            ),
            14 => 
            array (
                'code_departement' => 91,
                'cp' => 9140,
                'id' => 1053,
                'insee' => 9100,
                'name' => 'COUFLENS',
                'slug' => 'essonne',
            ),
            15 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 1055,
                'insee' => 9116,
                'name' => 'ESCOSSE',
                'slug' => 'essonne',
            ),
            16 => 
            array (
                'code_departement' => 94,
                'cp' => 9420,
                'id' => 1056,
                'insee' => 9118,
                'name' => 'ESPLAS DE SEROU',
                'slug' => 'val_de_marne',
            ),
            17 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 1057,
                'insee' => 9126,
                'name' => 'FREYCHENET',
                'slug' => 'seine_saint_denis',
            ),
            18 => 
            array (
                'code_departement' => 92,
                'cp' => 9290,
                'id' => 1058,
                'insee' => 9127,
                'name' => 'GABRE',
                'slug' => 'hauts_de_seine',
            ),
            19 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 1059,
                'insee' => 9133,
                'name' => 'GENAT',
                'slug' => 'val_de_marne',
            ),
            20 => 
            array (
                'code_departement' => 92,
                'cp' => 9220,
                'id' => 1060,
                'insee' => 9134,
                'name' => 'GESTIES',
                'slug' => 'hauts_de_seine',
            ),
            21 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 1061,
                'insee' => 9137,
                'name' => 'GUDAS',
                'slug' => 'essonne',
            ),
            22 => 
            array (
                'code_departement' => 91,
                'cp' => 9160,
                'id' => 1062,
                'insee' => 9148,
                'name' => 'LACAVE',
                'slug' => 'essonne',
            ),
            23 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 1063,
                'insee' => 9152,
                'name' => 'LAPEGE',
                'slug' => 'val_de_marne',
            ),
            24 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 1064,
                'insee' => 9153,
                'name' => 'LAPENNE',
                'slug' => 'val_doise',
            ),
            25 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 1065,
                'insee' => 9160,
                'name' => 'LAVELANET',
                'slug' => 'seine_saint_denis',
            ),
            26 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 1066,
                'insee' => 9180,
                'name' => 'MANSES',
                'slug' => 'val_doise',
            ),
            27 => 
            array (
                'code_departement' => 92,
                'cp' => 9270,
                'id' => 1067,
                'insee' => 9185,
                'name' => 'MAZERES',
                'slug' => 'hauts_de_seine',
            ),
            28 => 
            array (
                'code_departement' => 91,
                'cp' => 9160,
                'id' => 1068,
                'insee' => 9187,
                'name' => 'MERCENAC',
                'slug' => 'essonne',
            ),
            29 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 1069,
                'insee' => 9188,
                'name' => 'MERCUS GARRABET',
                'slug' => 'val_de_marne',
            ),
            30 => 
            array (
                'code_departement' => 91,
                'cp' => 9110,
                'id' => 1070,
                'insee' => 9197,
                'name' => 'MONTAILLOU',
                'slug' => 'essonne',
            ),
            31 => 
            array (
                'code_departement' => 92,
                'cp' => 9200,
                'id' => 1071,
                'insee' => 9201,
                'name' => 'MONTEGUT EN COUSERANS',
                'slug' => 'hauts_de_seine',
            ),
            32 => 
            array (
                'code_departement' => 91,
                'cp' => 9160,
                'id' => 1072,
                'insee' => 9208,
                'name' => 'MONTGAUCH',
                'slug' => 'essonne',
            ),
            33 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 1073,
                'insee' => 9215,
                'name' => 'NALZEN',
                'slug' => 'seine_saint_denis',
            ),
            34 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 1074,
                'insee' => 9221,
                'name' => 'ORNOLAC USSAT LES BAINS',
                'slug' => 'val_de_marne',
            ),
            35 => 
            array (
                'code_departement' => 92,
                'cp' => 9220,
                'id' => 1075,
                'insee' => 9222,
                'name' => 'ORUS',
                'slug' => 'hauts_de_seine',
            ),
            36 => 
            array (
                'code_departement' => 94,
                'cp' => 9460,
                'id' => 1076,
                'insee' => 9230,
                'name' => 'LE PLA',
                'slug' => 'val_de_marne',
            ),
            37 => 
            array (
                'code_departement' => 93,
                'cp' => 9320,
                'id' => 1077,
                'insee' => 9231,
                'name' => 'LE PORT',
                'slug' => 'seine_saint_denis',
            ),
            38 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 1078,
                'insee' => 9242,
                'name' => 'RAISSAC',
                'slug' => 'seine_saint_denis',
            ),
            39 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 1079,
                'insee' => 9250,
                'name' => 'ROQUEFORT LES CASCADES',
                'slug' => 'seine_saint_denis',
            ),
            40 => 
            array (
                'code_departement' => 94,
                'cp' => 9460,
                'id' => 1080,
                'insee' => 9252,
                'name' => 'ROUZE',
                'slug' => 'val_de_marne',
            ),
            41 => 
            array (
                'code_departement' => 92,
                'cp' => 9230,
                'id' => 1081,
                'insee' => 9257,
                'name' => 'STE CROIX VOLVESTRE',
                'slug' => 'hauts_de_seine',
            ),
            42 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 1082,
                'insee' => 9259,
                'name' => 'ST FELIX DE TOURNEGAT',
                'slug' => 'val_doise',
            ),
            43 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 1083,
                'insee' => 9276,
                'name' => 'ST VICTOR ROUZAUD',
                'slug' => 'essonne',
            ),
            44 => 
            array (
                'code_departement' => 91,
                'cp' => 9140,
                'id' => 1084,
                'insee' => 9285,
                'name' => 'SEIX',
                'slug' => 'essonne',
            ),
            45 => 
            array (
                'code_departement' => 92,
                'cp' => 9250,
                'id' => 1085,
                'insee' => 9287,
                'name' => 'SENCONAC',
                'slug' => 'hauts_de_seine',
            ),
            46 => 
            array (
                'code_departement' => 91,
                'cp' => 9190,
                'id' => 1086,
                'insee' => 9289,
                'name' => 'LORP SENTARAILLE',
                'slug' => 'essonne',
            ),
            47 => 
            array (
                'code_departement' => 92,
                'cp' => 9220,
                'id' => 1087,
                'insee' => 9295,
                'name' => 'SIGUER',
                'slug' => 'hauts_de_seine',
            ),
            48 => 
            array (
                'code_departement' => 92,
                'cp' => 9250,
                'id' => 1088,
                'insee' => 9318,
                'name' => 'UNAC',
                'slug' => 'hauts_de_seine',
            ),
            49 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 1089,
                'insee' => 9319,
                'name' => 'UNZENT',
                'slug' => 'essonne',
            ),
            50 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 1091,
                'insee' => 9324,
                'name' => 'VARILHES',
                'slug' => 'essonne',
            ),
            51 => 
            array (
                'code_departement' => 92,
                'cp' => 9250,
                'id' => 1092,
                'insee' => 9330,
                'name' => 'VERNAUX',
                'slug' => 'hauts_de_seine',
            ),
            52 => 
            array (
                'code_departement' => 78,
                'cp' => 7800,
                'id' => 1093,
                'insee' => 7027,
                'name' => 'BEAUCHASTEL',
                'slug' => 'yvelines',
            ),
            53 => 
            array (
                'code_departement' => 77,
                'cp' => 7700,
                'id' => 1094,
                'insee' => 7042,
                'name' => 'BOURG ST ANDEOL',
                'slug' => 'seine_et_marne',
            ),
            54 => 
            array (
                'code_departement' => 75,
                'cp' => 7510,
                'id' => 1095,
                'insee' => 7105,
                'name' => 'ISSANLAS',
                'slug' => 'paris',
            ),
            55 => 
            array (
                'code_departement' => 75,
                'cp' => 7530,
                'id' => 1096,
                'insee' => 7158,
                'name' => 'MEZILHAC',
                'slug' => 'paris',
            ),
            56 => 
            array (
                'code_departement' => 78,
                'cp' => 7800,
                'id' => 1097,
                'insee' => 7198,
                'name' => 'ROMPON',
                'slug' => 'yvelines',
            ),
            57 => 
            array (
                'code_departement' => 75,
                'cp' => 7530,
                'id' => 1098,
                'insee' => 7251,
                'name' => 'ST JOSEPH DES BANCS',
                'slug' => 'paris',
            ),
            58 => 
            array (
                'code_departement' => 77,
                'cp' => 7700,
                'id' => 1100,
                'insee' => 7264,
                'name' => 'ST MARCEL D ARDECHE',
                'slug' => 'seine_et_marne',
            ),
            59 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 1101,
                'insee' => 9003,
                'name' => 'L AIGUILLON',
                'slug' => 'seine_saint_denis',
            ),
            60 => 
            array (
                'code_departement' => 93,
                'cp' => 9320,
                'id' => 1102,
                'insee' => 9005,
                'name' => 'ALEU',
                'slug' => 'seine_saint_denis',
            ),
            61 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 1103,
                'insee' => 9006,
                'name' => 'ALLIAT',
                'slug' => 'val_de_marne',
            ),
            62 => 
            array (
                'code_departement' => 91,
                'cp' => 9130,
                'id' => 1104,
                'insee' => 9019,
                'name' => 'ARTIGAT',
                'slug' => 'essonne',
            ),
            63 => 
            array (
                'code_departement' => 92,
                'cp' => 9250,
                'id' => 1105,
                'insee' => 9031,
                'name' => 'AXIAT',
                'slug' => 'hauts_de_seine',
            ),
            64 => 
            array (
                'code_departement' => 91,
                'cp' => 9110,
                'id' => 1106,
                'insee' => 9032,
                'name' => 'AX LES THERMES',
                'slug' => 'essonne',
            ),
            65 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 1107,
                'insee' => 9039,
                'name' => 'LA BASTIDE DE BOUSIGNAC',
                'slug' => 'val_doise',
            ),
            66 => 
            array (
                'code_departement' => 91,
                'cp' => 9160,
                'id' => 1108,
                'insee' => 9041,
                'name' => 'LA BASTIDE DU SALAT',
                'slug' => 'essonne',
            ),
            67 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 1109,
                'insee' => 9045,
                'name' => 'BEDEILHAC ET AYNAT',
                'slug' => 'val_de_marne',
            ),
            68 => 
            array (
                'code_departement' => 93,
                'cp' => 9320,
                'id' => 1110,
                'insee' => 9065,
                'name' => 'BOUSSENAC',
                'slug' => 'seine_saint_denis',
            ),
            69 => 
            array (
                'code_departement' => 92,
                'cp' => 9290,
                'id' => 1111,
                'insee' => 9073,
                'name' => 'CAMARADE',
                'slug' => 'hauts_de_seine',
            ),
            70 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 1112,
                'insee' => 9081,
                'name' => 'LE CARLARET',
                'slug' => 'essonne',
            ),
            71 => 
            array (
                'code_departement' => 93,
                'cp' => 9350,
                'id' => 1113,
                'insee' => 9084,
                'name' => 'CASTEX',
                'slug' => 'seine_saint_denis',
            ),
            72 => 
            array (
                'code_departement' => 92,
                'cp' => 9250,
                'id' => 1114,
                'insee' => 9087,
                'name' => 'CAUSSOU',
                'slug' => 'hauts_de_seine',
            ),
            73 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 1115,
                'insee' => 9101,
                'name' => 'COUSSA',
                'slug' => 'essonne',
            ),
            74 => 
            array (
                'code_departement' => 91,
                'cp' => 9140,
                'id' => 1116,
                'insee' => 9113,
                'name' => 'ERCE',
                'slug' => 'essonne',
            ),
            75 => 
            array (
                'code_departement' => 91,
                'cp' => 9130,
                'id' => 1117,
                'insee' => 9124,
                'name' => 'LE FOSSAT',
                'slug' => 'essonne',
            ),
            76 => 
            array (
                'code_departement' => 92,
                'cp' => 9220,
                'id' => 1118,
                'insee' => 9143,
                'name' => 'ILLIER ET LARAMADE',
                'slug' => 'hauts_de_seine',
            ),
            77 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 1119,
                'insee' => 9165,
                'name' => 'LESPARROU',
                'slug' => 'seine_saint_denis',
            ),
            78 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 1120,
                'insee' => 9175,
                'name' => 'LUDIES',
                'slug' => 'essonne',
            ),
            79 => 
            array (
                'code_departement' => 92,
                'cp' => 9230,
                'id' => 1121,
                'insee' => 9184,
                'name' => 'MAUVEZIN DE STE CROIX',
                'slug' => 'hauts_de_seine',
            ),
            80 => 
            array (
                'code_departement' => 91,
                'cp' => 9110,
                'id' => 1123,
                'insee' => 9189,
                'name' => 'MERENS LES VALS',
                'slug' => 'essonne',
            ),
            81 => 
            array (
                'code_departement' => 92,
                'cp' => 9240,
                'id' => 1124,
                'insee' => 9196,
                'name' => 'MONTAGAGNE',
                'slug' => 'hauts_de_seine',
            ),
            82 => 
            array (
                'code_departement' => 92,
                'cp' => 9230,
                'id' => 1125,
                'insee' => 9198,
                'name' => 'MONTARDIT',
                'slug' => 'hauts_de_seine',
            ),
            83 => 
            array (
                'code_departement' => 92,
                'cp' => 9240,
                'id' => 1126,
                'insee' => 9203,
                'name' => 'MONTELS',
                'slug' => 'hauts_de_seine',
            ),
            84 => 
            array (
                'code_departement' => 92,
                'cp' => 9200,
                'id' => 1127,
                'insee' => 9214,
                'name' => 'MOULIS',
                'slug' => 'hauts_de_seine',
            ),
            85 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 1128,
                'insee' => 9238,
                'name' => 'LES PUJOLS',
                'slug' => 'essonne',
            ),
            86 => 
            array (
                'code_departement' => 94,
                'cp' => 9460,
                'id' => 1129,
                'insee' => 9239,
                'name' => 'QUERIGUT',
                'slug' => 'val_de_marne',
            ),
            87 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 1130,
                'insee' => 9251,
                'name' => 'ROUMENGOUX',
                'slug' => 'val_doise',
            ),
            88 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 1131,
                'insee' => 9254,
                'name' => 'ST AMADOU',
                'slug' => 'essonne',
            ),
            89 => 
            array (
                'code_departement' => 91,
                'cp' => 9100,
                'id' => 1132,
                'insee' => 9255,
                'name' => 'ST AMANS',
                'slug' => 'essonne',
            ),
            90 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 1133,
                'insee' => 9258,
                'name' => 'ST FELIX DE RIEUTORD',
                'slug' => 'essonne',
            ),
            91 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 1134,
                'insee' => 9260,
                'name' => 'STE FOI',
                'slug' => 'val_doise',
            ),
            92 => 
            array (
                'code_departement' => 92,
                'cp' => 9200,
                'id' => 1135,
                'insee' => 9261,
                'name' => 'ST GIRONS',
                'slug' => 'hauts_de_seine',
            ),
            93 => 
            array (
                'code_departement' => 91,
                'cp' => 9190,
                'id' => 1136,
                'insee' => 9268,
                'name' => 'ST LIZIER',
                'slug' => 'essonne',
            ),
            94 => 
            array (
                'code_departement' => 91,
                'cp' => 9120,
                'id' => 1138,
                'insee' => 9284,
                'name' => 'SEGURA',
                'slug' => 'essonne',
            ),
            95 => 
            array (
                'code_departement' => 91,
                'cp' => 9140,
                'id' => 1139,
                'insee' => 9291,
                'name' => 'SENTENAC D OUST',
                'slug' => 'essonne',
            ),
            96 => 
            array (
                'code_departement' => 91,
                'cp' => 9130,
                'id' => 1140,
                'insee' => 9294,
                'name' => 'SIEURAS',
                'slug' => 'essonne',
            ),
            97 => 
            array (
                'code_departement' => 91,
                'cp' => 9190,
                'id' => 1143,
                'insee' => 9308,
                'name' => 'TAURIGNAN VIEUX',
                'slug' => 'essonne',
            ),
            98 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 1144,
                'insee' => 9309,
                'name' => 'TEILHET',
                'slug' => 'val_doise',
            ),
            99 => 
            array (
                'code_departement' => 91,
                'cp' => 9110,
                'id' => 1145,
                'insee' => 9311,
                'name' => 'TIGNAC',
                'slug' => 'essonne',
            ),
            100 => 
            array (
                'code_departement' => 92,
                'cp' => 9230,
                'id' => 1146,
                'insee' => 9313,
                'name' => 'TOURTOUSE',
                'slug' => 'hauts_de_seine',
            ),
            101 => 
            array (
                'code_departement' => 95,
                'cp' => 9500,
                'id' => 1147,
                'insee' => 9316,
                'name' => 'TROYE D ARIEGE',
                'slug' => 'val_doise',
            ),
            102 => 
            array (
                'code_departement' => 94,
                'cp' => 9400,
                'id' => 1148,
                'insee' => 9321,
                'name' => 'USSAT',
                'slug' => 'val_de_marne',
            ),
            103 => 
            array (
                'code_departement' => 93,
                'cp' => 9310,
                'id' => 1149,
                'insee' => 9326,
                'name' => 'VEBRE',
                'slug' => 'seine_saint_denis',
            ),
            104 => 
            array (
                'code_departement' => 93,
                'cp' => 9300,
                'id' => 1151,
                'insee' => 9336,
                'name' => 'VILLENEUVE D OLMES',
                'slug' => 'seine_saint_denis',
            ),
            105 => 
            array (
                'code_departement' => 91,
                'cp' => 91470,
                'id' => 1152,
                'insee' => 91017,
                'name' => 'ANGERVILLIERS',
                'slug' => 'essonne',
            ),
            106 => 
            array (
                'code_departement' => 91,
                'cp' => 91200,
                'id' => 1153,
                'insee' => 91027,
                'name' => 'ATHIS MONS',
                'slug' => 'essonne',
            ),
            107 => 
            array (
                'code_departement' => 91,
                'cp' => 91410,
                'id' => 1154,
                'insee' => 91035,
                'name' => 'AUTHON LA PLAINE',
                'slug' => 'essonne',
            ),
            108 => 
            array (
                'code_departement' => 91,
                'cp' => 91630,
                'id' => 1155,
                'insee' => 91041,
                'name' => 'AVRAINVILLE',
                'slug' => 'essonne',
            ),
            109 => 
            array (
                'code_departement' => 91,
                'cp' => 91590,
                'id' => 1156,
                'insee' => 91047,
                'name' => 'BAULNE',
                'slug' => 'essonne',
            ),
            110 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 1157,
                'insee' => 91067,
                'name' => 'BLANDY',
                'slug' => 'essonne',
            ),
            111 => 
            array (
                'code_departement' => 91,
                'cp' => 91720,
                'id' => 1158,
                'insee' => 91069,
                'name' => 'BOIGNEVILLE',
                'slug' => 'essonne',
            ),
            112 => 
            array (
                'code_departement' => 91,
                'cp' => 91690,
                'id' => 1159,
                'insee' => 91079,
                'name' => 'BOISSY LA RIVIERE',
                'slug' => 'essonne',
            ),
            113 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 1160,
                'insee' => 91109,
                'name' => 'BRIERES LES SCELLES',
                'slug' => 'essonne',
            ),
            114 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 1161,
                'insee' => 91112,
                'name' => 'BROUY',
                'slug' => 'essonne',
            ),
            115 => 
            array (
                'code_departement' => 91,
                'cp' => 91580,
                'id' => 1163,
                'insee' => 91148,
                'name' => 'CHAUFFOUR LES ETRECHY',
                'slug' => 'essonne',
            ),
            116 => 
            array (
                'code_departement' => 91,
                'cp' => 91210,
                'id' => 1164,
                'insee' => 91201,
                'name' => 'DRAVEIL',
                'slug' => 'essonne',
            ),
            117 => 
            array (
                'code_departement' => 91,
                'cp' => 91360,
                'id' => 1166,
                'insee' => 91216,
                'name' => 'EPINAY SUR ORGE',
                'slug' => 'essonne',
            ),
            118 => 
            array (
                'code_departement' => 91,
                'cp' => 91580,
                'id' => 1167,
                'insee' => 91226,
                'name' => 'ETRECHY',
                'slug' => 'essonne',
            ),
            119 => 
            array (
                'code_departement' => 91,
                'cp' => 91470,
                'id' => 1168,
                'insee' => 91249,
                'name' => 'FORGES LES BAINS',
                'slug' => 'essonne',
            ),
            120 => 
            array (
                'code_departement' => 91,
                'cp' => 91590,
                'id' => 1169,
                'insee' => 91293,
                'name' => 'GUIGNEVILLE SUR ESSONNE',
                'slug' => 'essonne',
            ),
            121 => 
            array (
                'code_departement' => 91,
                'cp' => 91430,
                'id' => 1170,
                'insee' => 91312,
                'name' => 'IGNY',
                'slug' => 'essonne',
            ),
            122 => 
            array (
                'code_departement' => 91,
                'cp' => 91630,
                'id' => 1171,
                'insee' => 91332,
                'name' => 'LEUDEVILLE',
                'slug' => 'essonne',
            ),
            123 => 
            array (
                'code_departement' => 91,
                'cp' => 91490,
                'id' => 1174,
                'insee' => 91408,
                'name' => 'MOIGNY SUR ECOLE',
                'slug' => 'essonne',
            ),
            124 => 
            array (
                'code_departement' => 91,
                'cp' => 91310,
                'id' => 1175,
                'insee' => 91425,
                'name' => 'MONTLHERY',
                'slug' => 'essonne',
            ),
            125 => 
            array (
                'code_departement' => 91,
                'cp' => 91390,
                'id' => 1176,
                'insee' => 91434,
                'name' => 'MORSANG SUR ORGE',
                'slug' => 'essonne',
            ),
            126 => 
            array (
                'code_departement' => 91,
                'cp' => 91590,
                'id' => 1177,
                'insee' => 91473,
                'name' => 'ORVEAU',
                'slug' => 'essonne',
            ),
            127 => 
            array (
                'code_departement' => 91,
                'cp' => 91550,
                'id' => 1178,
                'insee' => 91479,
                'name' => 'PARAY VIEILLE POSTE',
                'slug' => 'essonne',
            ),
            128 => 
            array (
                'code_departement' => 91,
                'cp' => 91410,
                'id' => 1179,
                'insee' => 91495,
                'name' => 'PLESSIS ST BENOIST',
                'slug' => 'essonne',
            ),
            129 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 1180,
                'insee' => 91508,
                'name' => 'PUISELET LE MARAIS',
                'slug' => 'essonne',
            ),
            130 => 
            array (
                'code_departement' => 91,
                'cp' => 91690,
                'id' => 1181,
                'insee' => 91533,
                'name' => 'SACLAS',
                'slug' => 'essonne',
            ),
            131 => 
            array (
                'code_departement' => 91,
                'cp' => 91190,
                'id' => 1183,
                'insee' => 91538,
                'name' => 'ST AUBIN',
                'slug' => 'essonne',
            ),
            132 => 
            array (
                'code_departement' => 91,
                'cp' => 91410,
                'id' => 1185,
                'insee' => 91546,
                'name' => 'ST CYR SOUS DOURDAN',
                'slug' => 'essonne',
            ),
            133 => 
            array (
                'code_departement' => 91,
                'cp' => 91410,
                'id' => 1186,
                'insee' => 91547,
                'name' => 'ST ESCOBILLE',
                'slug' => 'essonne',
            ),
            134 => 
            array (
                'code_departement' => 91,
                'cp' => 91280,
                'id' => 1187,
                'insee' => 91573,
                'name' => 'ST PIERRE DU PERRAY',
                'slug' => 'essonne',
            ),
            135 => 
            array (
                'code_departement' => 91,
                'cp' => 91650,
                'id' => 1188,
                'insee' => 91581,
                'name' => 'ST YON',
                'slug' => 'essonne',
            ),
            136 => 
            array (
                'code_departement' => 91,
                'cp' => 91600,
                'id' => 1189,
                'insee' => 91589,
                'name' => 'SAVIGNY SUR ORGE',
                'slug' => 'essonne',
            ),
            137 => 
            array (
                'code_departement' => 91,
                'cp' => 91250,
                'id' => 1190,
                'insee' => 91617,
                'name' => 'TIGERY',
                'slug' => 'essonne',
            ),
            138 => 
            array (
                'code_departement' => 91,
                'cp' => 91430,
                'id' => 1191,
                'insee' => 91635,
                'name' => 'VAUHALLAN',
                'slug' => 'essonne',
            ),
            139 => 
            array (
                'code_departement' => 91,
                'cp' => 91370,
                'id' => 1192,
                'insee' => 91645,
                'name' => 'VERRIERES LE BUISSON',
                'slug' => 'essonne',
            ),
            140 => 
            array (
                'code_departement' => 91,
                'cp' => 91710,
                'id' => 1193,
                'insee' => 91649,
                'name' => 'VERT LE PETIT',
                'slug' => 'essonne',
            ),
            141 => 
            array (
                'code_departement' => 91,
                'cp' => 91580,
                'id' => 1194,
                'insee' => 91662,
                'name' => 'VILLECONIN',
                'slug' => 'essonne',
            ),
            142 => 
            array (
                'code_departement' => 91,
                'cp' => 91360,
                'id' => 1195,
                'insee' => 91667,
                'name' => 'VILLEMOISSON SUR ORGE',
                'slug' => 'essonne',
            ),
            143 => 
            array (
                'code_departement' => 91,
                'cp' => 91330,
                'id' => 1196,
                'insee' => 91691,
                'name' => 'YERRES',
                'slug' => 'essonne',
            ),
            144 => 
            array (
                'code_departement' => 92,
                'cp' => 92220,
                'id' => 1197,
                'insee' => 92007,
                'name' => 'BAGNEUX',
                'slug' => 'hauts_de_seine',
            ),
            145 => 
            array (
                'code_departement' => 92,
                'cp' => 92320,
                'id' => 1198,
                'insee' => 92020,
                'name' => 'CHATILLON',
                'slug' => 'hauts_de_seine',
            ),
            146 => 
            array (
                'code_departement' => 92,
                'cp' => 92430,
                'id' => 1199,
                'insee' => 92047,
                'name' => 'MARNES LA COQUETTE',
                'slug' => 'hauts_de_seine',
            ),
            147 => 
            array (
                'code_departement' => 92,
                'cp' => 92190,
                'id' => 1200,
                'insee' => 92048,
                'name' => 'MEUDON',
                'slug' => 'hauts_de_seine',
            ),
            148 => 
            array (
                'code_departement' => 92,
                'cp' => 92200,
                'id' => 1201,
                'insee' => 92051,
                'name' => 'NEUILLY SUR SEINE',
                'slug' => 'hauts_de_seine',
            ),
            149 => 
            array (
                'code_departement' => 92,
                'cp' => 92420,
                'id' => 1202,
                'insee' => 92076,
                'name' => 'VAUCRESSON',
                'slug' => 'hauts_de_seine',
            ),
            150 => 
            array (
                'code_departement' => 92,
                'cp' => 92410,
                'id' => 1203,
                'insee' => 92077,
                'name' => 'VILLE D AVRAY',
                'slug' => 'hauts_de_seine',
            ),
            151 => 
            array (
                'code_departement' => 93,
                'cp' => 93600,
                'id' => 1204,
                'insee' => 93005,
                'name' => 'AULNAY SOUS BOIS',
                'slug' => 'seine_saint_denis',
            ),
            152 => 
            array (
                'code_departement' => 93,
                'cp' => 93140,
                'id' => 1205,
                'insee' => 93010,
                'name' => 'BONDY',
                'slug' => 'seine_saint_denis',
            ),
            153 => 
            array (
                'code_departement' => 93,
                'cp' => 93700,
                'id' => 1206,
                'insee' => 93029,
                'name' => 'DRANCY',
                'slug' => 'seine_saint_denis',
            ),
            154 => 
            array (
                'code_departement' => 93,
                'cp' => 93190,
                'id' => 1207,
                'insee' => 93046,
                'name' => 'LIVRY GARGAN',
                'slug' => 'seine_saint_denis',
            ),
            155 => 
            array (
                'code_departement' => 93,
                'cp' => 93340,
                'id' => 1208,
                'insee' => 93062,
                'name' => 'LE RAINCY',
                'slug' => 'seine_saint_denis',
            ),
            156 => 
            array (
                'code_departement' => 93,
                'cp' => 93230,
                'id' => 1209,
                'insee' => 93063,
                'name' => 'ROMAINVILLE',
                'slug' => 'seine_saint_denis',
            ),
            157 => 
            array (
                'code_departement' => 93,
                'cp' => 93290,
                'id' => 1210,
                'insee' => 93073,
                'name' => 'TREMBLAY EN FRANCE',
                'slug' => 'seine_saint_denis',
            ),
            158 => 
            array (
                'code_departement' => 93,
                'cp' => 93250,
                'id' => 1211,
                'insee' => 93077,
                'name' => 'VILLEMOMBLE',
                'slug' => 'seine_saint_denis',
            ),
            159 => 
            array (
                'code_departement' => 93,
                'cp' => 93420,
                'id' => 1212,
                'insee' => 93078,
                'name' => 'VILLEPINTE',
                'slug' => 'seine_saint_denis',
            ),
            160 => 
            array (
                'code_departement' => 94,
                'cp' => 94110,
                'id' => 1213,
                'insee' => 94003,
                'name' => 'ARCUEIL',
                'slug' => 'val_de_marne',
            ),
            161 => 
            array (
                'code_departement' => 94,
                'cp' => 94360,
                'id' => 1214,
                'insee' => 94015,
                'name' => 'BRY SUR MARNE',
                'slug' => 'val_de_marne',
            ),
            162 => 
            array (
                'code_departement' => 94,
                'cp' => 94270,
                'id' => 1215,
                'insee' => 94043,
                'name' => 'LE KREMLIN BICETRE',
                'slug' => 'val_de_marne',
            ),
            163 => 
            array (
                'code_departement' => 94,
                'cp' => 94450,
                'id' => 1216,
                'insee' => 94044,
                'name' => 'LIMEIL BREVANNES',
                'slug' => 'val_de_marne',
            ),
            164 => 
            array (
                'code_departement' => 94,
                'cp' => 94440,
                'id' => 1217,
                'insee' => 94048,
                'name' => 'MAROLLES EN BRIE',
                'slug' => 'val_de_marne',
            ),
            165 => 
            array (
                'code_departement' => 94,
                'cp' => 94310,
                'id' => 1218,
                'insee' => 94054,
                'name' => 'ORLY',
                'slug' => 'val_de_marne',
            ),
            166 => 
            array (
                'code_departement' => 94,
                'cp' => 94520,
                'id' => 1219,
                'insee' => 94056,
                'name' => 'PERIGNY',
                'slug' => 'val_de_marne',
            ),
            167 => 
            array (
                'code_departement' => 94,
                'cp' => 94440,
                'id' => 1221,
                'insee' => 94075,
                'name' => 'VILLECRESNES',
                'slug' => 'val_de_marne',
            ),
            168 => 
            array (
                'code_departement' => 94,
                'cp' => 94290,
                'id' => 1222,
                'insee' => 94077,
                'name' => 'VILLENEUVE LE ROI',
                'slug' => 'val_de_marne',
            ),
            169 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 1223,
                'insee' => 95011,
                'name' => 'AMBLEVILLE',
                'slug' => 'val_doise',
            ),
            170 => 
            array (
                'code_departement' => 95,
                'cp' => 95400,
                'id' => 1224,
                'insee' => 95019,
                'name' => 'ARNOUVILLE',
                'slug' => 'val_doise',
            ),
            171 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 1225,
                'insee' => 95024,
                'name' => 'ARTHIES',
                'slug' => 'val_doise',
            ),
            172 => 
            array (
                'code_departement' => 95,
                'cp' => 95750,
                'id' => 1226,
                'insee' => 95054,
                'name' => 'LE BELLAY EN VEXIN',
                'slug' => 'val_doise',
            ),
            173 => 
            array (
                'code_departement' => 95,
                'cp' => 95550,
                'id' => 1227,
                'insee' => 95060,
                'name' => 'BESSANCOURT',
                'slug' => 'val_doise',
            ),
            174 => 
            array (
                'code_departement' => 95,
                'cp' => 95840,
                'id' => 1228,
                'insee' => 95061,
                'name' => 'BETHEMONT LA FORET',
                'slug' => 'val_doise',
            ),
            175 => 
            array (
                'code_departement' => 95,
                'cp' => 95650,
                'id' => 1229,
                'insee' => 95078,
                'name' => 'BOISSY L AILLERIE',
                'slug' => 'val_doise',
            ),
            176 => 
            array (
                'code_departement' => 95,
                'cp' => 95640,
                'id' => 1230,
                'insee' => 95110,
                'name' => 'BRIGNANCOURT',
                'slug' => 'val_doise',
            ),
            177 => 
            array (
                'code_departement' => 95,
                'cp' => 95430,
                'id' => 1231,
                'insee' => 95120,
                'name' => 'BUTRY SUR OISE',
                'slug' => 'val_doise',
            ),
            178 => 
            array (
                'code_departement' => 95,
                'cp' => 95270,
                'id' => 1233,
                'insee' => 95149,
                'name' => 'CHAUMONTEL',
                'slug' => 'val_doise',
            ),
            179 => 
            array (
                'code_departement' => 95,
                'cp' => 95560,
                'id' => 1234,
                'insee' => 95151,
                'name' => 'CHAUVRY',
                'slug' => 'val_doise',
            ),
            180 => 
            array (
                'code_departement' => 95,
                'cp' => 95510,
                'id' => 1235,
                'insee' => 95157,
                'name' => 'CHERENCE',
                'slug' => 'val_doise',
            ),
            181 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 1236,
                'insee' => 95169,
                'name' => 'COMMENY',
                'slug' => 'val_doise',
            ),
            182 => 
            array (
                'code_departement' => 95,
                'cp' => 95600,
                'id' => 1237,
                'insee' => 95203,
                'name' => 'EAUBONNE',
                'slug' => 'val_doise',
            ),
            183 => 
            array (
                'code_departement' => 95,
                'cp' => 95130,
                'id' => 1238,
                'insee' => 95252,
                'name' => 'FRANCONVILLE',
                'slug' => 'val_doise',
            ),
            184 => 
            array (
                'code_departement' => 95,
                'cp' => 95500,
                'id' => 1239,
                'insee' => 95277,
                'name' => 'GONESSE',
                'slug' => 'val_doise',
            ),
            185 => 
            array (
                'code_departement' => 95,
                'cp' => 95190,
                'id' => 1240,
                'insee' => 95280,
                'name' => 'GOUSSAINVILLE',
                'slug' => 'val_doise',
            ),
            186 => 
            array (
                'code_departement' => 95,
                'cp' => 95810,
                'id' => 1241,
                'insee' => 95287,
                'name' => 'GRISY LES PLATRES',
                'slug' => 'val_doise',
            ),
            187 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 1242,
                'insee' => 95295,
                'name' => 'GUIRY EN VEXIN',
                'slug' => 'val_doise',
            ),
            188 => 
            array (
                'code_departement' => 95,
                'cp' => 95850,
                'id' => 1243,
                'insee' => 95316,
                'name' => 'JAGNY SOUS BOIS',
                'slug' => 'val_doise',
            ),
            189 => 
            array (
                'code_departement' => 95,
                'cp' => 95380,
                'id' => 1245,
                'insee' => 95351,
                'name' => 'LOUVRES',
                'slug' => 'val_doise',
            ),
            190 => 
            array (
                'code_departement' => 95,
                'cp' => 95270,
                'id' => 1246,
                'insee' => 95352,
                'name' => 'LUZARCHES',
                'slug' => 'val_doise',
            ),
            191 => 
            array (
                'code_departement' => 95,
                'cp' => 95540,
                'id' => 1247,
                'insee' => 95394,
                'name' => 'MERY SUR OISE',
                'slug' => 'val_doise',
            ),
            192 => 
            array (
                'code_departement' => 95,
                'cp' => 95720,
                'id' => 1248,
                'insee' => 95395,
                'name' => 'LE MESNIL AUBRY',
                'slug' => 'val_doise',
            ),
            193 => 
            array (
                'code_departement' => 95,
                'cp' => 95640,
                'id' => 1249,
                'insee' => 95438,
                'name' => 'MOUSSY',
                'slug' => 'val_doise',
            ),
            194 => 
            array (
                'code_departement' => 95,
                'cp' => 95590,
                'id' => 1250,
                'insee' => 95445,
                'name' => 'NERVILLE LA FORET',
                'slug' => 'val_doise',
            ),
            195 => 
            array (
                'code_departement' => 95,
                'cp' => 95690,
                'id' => 1251,
                'insee' => 95446,
                'name' => 'NESLES LA VALLEE',
                'slug' => 'val_doise',
            ),
            196 => 
            array (
                'code_departement' => 95,
                'cp' => 95270,
                'id' => 1252,
                'insee' => 95456,
                'name' => 'NOISY SUR OISE',
                'slug' => 'val_doise',
            ),
            197 => 
            array (
                'code_departement' => 95,
                'cp' => 95340,
                'id' => 1253,
                'insee' => 95487,
                'name' => 'PERSAN',
                'slug' => 'val_doise',
            ),
            198 => 
            array (
                'code_departement' => 95,
                'cp' => 95220,
                'id' => 1254,
                'insee' => 95488,
                'name' => 'PIERRELAYE',
                'slug' => 'val_doise',
            ),
            199 => 
            array (
                'code_departement' => 95,
                'cp' => 95480,
                'id' => 1255,
                'insee' => 95488,
                'name' => 'PIERRELAYE',
                'slug' => 'val_doise',
            ),
            200 => 
            array (
                'code_departement' => 95,
                'cp' => 95130,
                'id' => 1256,
                'insee' => 95491,
                'name' => 'LE PLESSIS BOUCHARD',
                'slug' => 'val_doise',
            ),
            201 => 
            array (
                'code_departement' => 95,
                'cp' => 95590,
                'id' => 1257,
                'insee' => 95504,
                'name' => 'PRESLES',
                'slug' => 'val_doise',
            ),
            202 => 
            array (
                'code_departement' => 95,
                'cp' => 95380,
                'id' => 1258,
                'insee' => 95509,
                'name' => 'PUISEUX EN FRANCE',
                'slug' => 'val_doise',
            ),
            203 => 
            array (
                'code_departement' => 95,
                'cp' => 95200,
                'id' => 1260,
                'insee' => 95585,
                'name' => 'SARCELLES',
                'slug' => 'val_doise',
            ),
            204 => 
            array (
                'code_departement' => 95,
                'cp' => 95470,
                'id' => 1261,
                'insee' => 95604,
                'name' => 'SURVILLIERS',
                'slug' => 'val_doise',
            ),
            205 => 
            array (
                'code_departement' => 95,
                'cp' => 95450,
                'id' => 1262,
                'insee' => 95610,
                'name' => 'THEMERICOURT',
                'slug' => 'val_doise',
            ),
            206 => 
            array (
                'code_departement' => 95,
                'cp' => 95570,
                'id' => 1263,
                'insee' => 95660,
                'name' => 'VILLAINES SOUS BOIS',
                'slug' => 'val_doise',
            ),
            207 => 
            array (
                'code_departement' => 75,
                'cp' => 75006,
                'id' => 1264,
                'insee' => 75106,
                'name' => 'PARIS 06',
                'slug' => 'paris',
            ),
            208 => 
            array (
                'code_departement' => 75,
                'cp' => 75012,
                'id' => 1265,
                'insee' => 75112,
                'name' => 'PARIS 12',
                'slug' => 'paris',
            ),
            209 => 
            array (
                'code_departement' => 75,
                'cp' => 75013,
                'id' => 1266,
                'insee' => 75113,
                'name' => 'PARIS 13',
                'slug' => 'paris',
            ),
            210 => 
            array (
                'code_departement' => 75,
                'cp' => 75018,
                'id' => 1267,
                'insee' => 75118,
                'name' => 'PARIS 18',
                'slug' => 'paris',
            ),
            211 => 
            array (
                'code_departement' => 91,
                'cp' => 91160,
                'id' => 1268,
                'insee' => 91044,
                'name' => 'BALLAINVILLIERS',
                'slug' => 'essonne',
            ),
            212 => 
            array (
                'code_departement' => 91,
                'cp' => 91570,
                'id' => 1269,
                'insee' => 91064,
                'name' => 'BIEVRES',
                'slug' => 'essonne',
            ),
            213 => 
            array (
                'code_departement' => 91,
                'cp' => 91470,
                'id' => 1270,
                'insee' => 91093,
                'name' => 'BOULLAY LES TROUX',
                'slug' => 'essonne',
            ),
            214 => 
            array (
                'code_departement' => 91,
                'cp' => 91800,
                'id' => 1271,
                'insee' => 91097,
                'name' => 'BOUSSY ST ANTOINE',
                'slug' => 'essonne',
            ),
            215 => 
            array (
                'code_departement' => 91,
                'cp' => 91880,
                'id' => 1272,
                'insee' => 91100,
                'name' => 'BOUVILLE',
                'slug' => 'essonne',
            ),
            216 => 
            array (
                'code_departement' => 91,
                'cp' => 91220,
                'id' => 1273,
                'insee' => 91103,
                'name' => 'BRETIGNY SUR ORGE',
                'slug' => 'essonne',
            ),
            217 => 
            array (
                'code_departement' => 91,
                'cp' => 91640,
                'id' => 1274,
                'insee' => 91111,
                'name' => 'BRIIS SOUS FORGES',
                'slug' => 'essonne',
            ),
            218 => 
            array (
                'code_departement' => 91,
                'cp' => 91680,
                'id' => 1275,
                'insee' => 91115,
                'name' => 'BRUYERES LE CHATEL',
                'slug' => 'essonne',
            ),
            219 => 
            array (
                'code_departement' => 91,
                'cp' => 91590,
                'id' => 1276,
                'insee' => 91129,
                'name' => 'CERNY',
                'slug' => 'essonne',
            ),
            220 => 
            array (
                'code_departement' => 91,
                'cp' => 91780,
                'id' => 1277,
                'insee' => 91130,
                'name' => 'CHALO ST MARS',
                'slug' => 'essonne',
            ),
            221 => 
            array (
                'code_departement' => 91,
                'cp' => 91380,
                'id' => 1278,
                'insee' => 91161,
                'name' => 'CHILLY MAZARIN',
                'slug' => 'essonne',
            ),
            222 => 
            array (
                'code_departement' => 91,
                'cp' => 91830,
                'id' => 1279,
                'insee' => 91179,
                'name' => 'LE COUDRAY MONTCEAUX',
                'slug' => 'essonne',
            ),
            223 => 
            array (
                'code_departement' => 91,
                'cp' => 91490,
                'id' => 1281,
                'insee' => 91180,
                'name' => 'COURANCES',
                'slug' => 'essonne',
            ),
            224 => 
            array (
                'code_departement' => 91,
                'cp' => 91720,
                'id' => 1282,
                'insee' => 91184,
                'name' => 'COURDIMANCHE SUR ESSONNE',
                'slug' => 'essonne',
            ),
            225 => 
            array (
                'code_departement' => 91,
                'cp' => 91680,
                'id' => 1283,
                'insee' => 91186,
                'name' => 'COURSON MONTELOUP',
                'slug' => 'essonne',
            ),
            226 => 
            array (
                'code_departement' => 91,
                'cp' => 91590,
                'id' => 1284,
                'insee' => 91198,
                'name' => 'D HUISON LONGUEVILLE',
                'slug' => 'essonne',
            ),
            227 => 
            array (
                'code_departement' => 91,
                'cp' => 91540,
                'id' => 1285,
                'insee' => 91204,
                'name' => 'ECHARCON',
                'slug' => 'essonne',
            ),
            228 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 1286,
                'insee' => 91223,
                'name' => 'ETAMPES',
                'slug' => 'essonne',
            ),
            229 => 
            array (
                'code_departement' => 91,
                'cp' => 91450,
                'id' => 1287,
                'insee' => 91225,
                'name' => 'ETIOLLES',
                'slug' => 'essonne',
            ),
            230 => 
            array (
                'code_departement' => 91,
                'cp' => 91640,
                'id' => 1288,
                'insee' => 91243,
                'name' => 'FONTENAY LES BRIIS',
                'slug' => 'essonne',
            ),
            231 => 
            array (
                'code_departement' => 91,
                'cp' => 91540,
                'id' => 1289,
                'insee' => 91244,
                'name' => 'FONTENAY LE VICOMTE',
                'slug' => 'essonne',
            ),
            232 => 
            array (
                'code_departement' => 91,
                'cp' => 91940,
                'id' => 1290,
                'insee' => 91275,
                'name' => 'GOMETZ LE CHATEL',
                'slug' => 'essonne',
            ),
            233 => 
            array (
                'code_departement' => 91,
                'cp' => 91410,
                'id' => 1291,
                'insee' => 91284,
                'name' => 'LES GRANGES LE ROI',
                'slug' => 'essonne',
            ),
            234 => 
            array (
                'code_departement' => 91,
                'cp' => 91630,
                'id' => 1292,
                'insee' => 91292,
                'name' => 'GUIBEVILLE',
                'slug' => 'essonne',
            ),
            235 => 
            array (
                'code_departement' => 91,
                'cp' => 91510,
                'id' => 1294,
                'insee' => 91318,
                'name' => 'JANVILLE SUR JUINE',
                'slug' => 'essonne',
            ),
            236 => 
            array (
                'code_departement' => 91,
                'cp' => 91310,
                'id' => 1295,
                'insee' => 91339,
                'name' => 'LINAS',
                'slug' => 'essonne',
            ),
            237 => 
            array (
                'code_departement' => 91,
                'cp' => 91300,
                'id' => 1296,
                'insee' => 91377,
                'name' => 'MASSY',
                'slug' => 'essonne',
            ),
            238 => 
            array (
                'code_departement' => 91,
                'cp' => 91540,
                'id' => 1297,
                'insee' => 91386,
                'name' => 'MENNECY',
                'slug' => 'essonne',
            ),
            239 => 
            array (
                'code_departement' => 91,
                'cp' => 91780,
                'id' => 1298,
                'insee' => 91393,
                'name' => 'MEROBERT',
                'slug' => 'essonne',
            ),
            240 => 
            array (
                'code_departement' => 91,
                'cp' => 91490,
                'id' => 1299,
                'insee' => 91405,
                'name' => 'MILLY LA FORET',
                'slug' => 'essonne',
            ),
            241 => 
            array (
                'code_departement' => 91,
                'cp' => 91590,
                'id' => 1300,
                'insee' => 91412,
                'name' => 'MONDEVILLE',
                'slug' => 'essonne',
            ),
            242 => 
            array (
                'code_departement' => 91,
                'cp' => 91150,
                'id' => 1301,
                'insee' => 91433,
                'name' => 'MORIGNY CHAMPIGNY',
                'slug' => 'essonne',
            ),
            243 => 
            array (
                'code_departement' => 91,
                'cp' => 91750,
                'id' => 1302,
                'insee' => 91441,
                'name' => 'NAINVILLE LES ROCHES',
                'slug' => 'essonne',
            ),
            244 => 
            array (
                'code_departement' => 91,
                'cp' => 91340,
                'id' => 1303,
                'insee' => 91461,
                'name' => 'OLLAINVILLE',
                'slug' => 'essonne',
            ),
            245 => 
            array (
                'code_departement' => 94,
                'cp' => 94390,
                'id' => 1305,
                'insee' => 91479,
                'name' => 'PARAY VIEILLE POSTE',
                'slug' => 'val_de_marne',
            ),
            246 => 
            array (
                'code_departement' => 91,
                'cp' => 91690,
                'id' => 1306,
                'insee' => 91544,
                'name' => 'ST CYR LA RIVIERE',
                'slug' => 'essonne',
            ),
            247 => 
            array (
                'code_departement' => 91,
                'cp' => 91180,
                'id' => 1307,
                'insee' => 91552,
                'name' => 'ST GERMAIN LES ARPAJON',
                'slug' => 'essonne',
            ),
            248 => 
            array (
                'code_departement' => 91,
                'cp' => 91530,
                'id' => 1308,
                'insee' => 91568,
                'name' => 'ST MAURICE MONTCOURONNE',
                'slug' => 'essonne',
            ),
            249 => 
            array (
                'code_departement' => 91,
                'cp' => 91910,
                'id' => 1309,
                'insee' => 91578,
                'name' => 'ST SULPICE DE FAVIERES',
                'slug' => 'essonne',
            ),
            250 => 
            array (
                'code_departement' => 91,
                'cp' => 91450,
                'id' => 1310,
                'insee' => 91600,
                'name' => 'SOISY SUR SEINE',
                'slug' => 'essonne',
            ),
            251 => 
            array (
                'code_departement' => 91,
                'cp' => 91580,
                'id' => 1311,
                'insee' => 91602,
                'name' => 'SOUZY LA BRICHE',
                'slug' => 'essonne',
            ),
            252 => 
            array (
                'code_departement' => 91,
                'cp' => 91640,
                'id' => 1312,
                'insee' => 91634,
                'name' => 'VAUGRIGNEUSE',
                'slug' => 'essonne',
            ),
            253 => 
            array (
                'code_departement' => 91,
                'cp' => 91810,
                'id' => 1313,
                'insee' => 91648,
                'name' => 'VERT LE GRAND',
                'slug' => 'essonne',
            ),
            254 => 
            array (
                'code_departement' => 91,
                'cp' => 91100,
                'id' => 1314,
                'insee' => 91659,
                'name' => 'VILLABE',
                'slug' => 'essonne',
            ),
            255 => 
            array (
                'code_departement' => 91,
                'cp' => 91140,
                'id' => 1315,
                'insee' => 91661,
                'name' => 'VILLEBON SUR YVETTE',
                'slug' => 'essonne',
            ),
            256 => 
            array (
                'code_departement' => 91,
                'cp' => 91170,
                'id' => 1316,
                'insee' => 91687,
                'name' => 'VIRY CHATILLON',
                'slug' => 'essonne',
            ),
            257 => 
            array (
                'code_departement' => 92,
                'cp' => 92290,
                'id' => 1317,
                'insee' => 92019,
                'name' => 'CHATENAY MALABRY',
                'slug' => 'hauts_de_seine',
            ),
            258 => 
            array (
                'code_departement' => 92,
                'cp' => 92110,
                'id' => 1319,
                'insee' => 92024,
                'name' => 'CLICHY',
                'slug' => 'hauts_de_seine',
            ),
            259 => 
            array (
                'code_departement' => 92,
                'cp' => 92130,
                'id' => 1320,
                'insee' => 92040,
                'name' => 'ISSY LES MOULINEAUX',
                'slug' => 'hauts_de_seine',
            ),
            260 => 
            array (
                'code_departement' => 92,
                'cp' => 92120,
                'id' => 1321,
                'insee' => 92049,
                'name' => 'MONTROUGE',
                'slug' => 'hauts_de_seine',
            ),
            261 => 
            array (
                'code_departement' => 92,
                'cp' => 92350,
                'id' => 1322,
                'insee' => 92060,
                'name' => 'LE PLESSIS ROBINSON',
                'slug' => 'hauts_de_seine',
            ),
            262 => 
            array (
                'code_departement' => 92,
                'cp' => 92170,
                'id' => 1324,
                'insee' => 92075,
                'name' => 'VANVES',
                'slug' => 'hauts_de_seine',
            ),
            263 => 
            array (
                'code_departement' => 92,
                'cp' => 92390,
                'id' => 1325,
                'insee' => 92078,
                'name' => 'VILLENEUVE LA GARENNE',
                'slug' => 'hauts_de_seine',
            ),
            264 => 
            array (
                'code_departement' => 93,
                'cp' => 93300,
                'id' => 1326,
                'insee' => 93001,
                'name' => 'AUBERVILLIERS',
                'slug' => 'seine_saint_denis',
            ),
            265 => 
            array (
                'code_departement' => 93,
                'cp' => 93170,
                'id' => 1327,
                'insee' => 93006,
                'name' => 'BAGNOLET',
                'slug' => 'seine_saint_denis',
            ),
            266 => 
            array (
                'code_departement' => 93,
                'cp' => 93440,
                'id' => 1328,
                'insee' => 93030,
                'name' => 'DUGNY',
                'slug' => 'seine_saint_denis',
            ),
            267 => 
            array (
                'code_departement' => 93,
                'cp' => 93800,
                'id' => 1329,
                'insee' => 93031,
                'name' => 'EPINAY SUR SEINE',
                'slug' => 'seine_saint_denis',
            ),
            268 => 
            array (
                'code_departement' => 93,
                'cp' => 93450,
                'id' => 1330,
                'insee' => 93039,
                'name' => 'L ILE ST DENIS',
                'slug' => 'seine_saint_denis',
            ),
            269 => 
            array (
                'code_departement' => 93,
                'cp' => 93100,
                'id' => 1331,
                'insee' => 93048,
                'name' => 'MONTREUIL',
                'slug' => 'seine_saint_denis',
            ),
            270 => 
            array (
                'code_departement' => 93,
                'cp' => 93360,
                'id' => 1332,
                'insee' => 93049,
                'name' => 'NEUILLY PLAISANCE',
                'slug' => 'seine_saint_denis',
            ),
            271 => 
            array (
                'code_departement' => 93,
                'cp' => 93160,
                'id' => 1333,
                'insee' => 93051,
                'name' => 'NOISY LE GRAND',
                'slug' => 'seine_saint_denis',
            ),
            272 => 
            array (
                'code_departement' => 93,
                'cp' => 93110,
                'id' => 1334,
                'insee' => 93064,
                'name' => 'ROSNY SOUS BOIS',
                'slug' => 'seine_saint_denis',
            ),
            273 => 
            array (
                'code_departement' => 93,
                'cp' => 93200,
                'id' => 1335,
                'insee' => 93066,
                'name' => 'ST DENIS',
                'slug' => 'seine_saint_denis',
            ),
            274 => 
            array (
                'code_departement' => 93,
                'cp' => 93400,
                'id' => 1336,
                'insee' => 93070,
                'name' => 'ST OUEN SUR SEINE',
                'slug' => 'seine_saint_denis',
            ),
            275 => 
            array (
                'code_departement' => 93,
                'cp' => 93240,
                'id' => 1337,
                'insee' => 93072,
                'name' => 'STAINS',
                'slug' => 'seine_saint_denis',
            ),
            276 => 
            array (
                'code_departement' => 94,
                'cp' => 94470,
                'id' => 1338,
                'insee' => 94004,
                'name' => 'BOISSY ST LEGER',
                'slug' => 'val_de_marne',
            ),
            277 => 
            array (
                'code_departement' => 94,
                'cp' => 94380,
                'id' => 1339,
                'insee' => 94011,
                'name' => 'BONNEUIL SUR MARNE',
                'slug' => 'val_de_marne',
            ),
            278 => 
            array (
                'code_departement' => 94,
                'cp' => 94600,
                'id' => 1341,
                'insee' => 94022,
                'name' => 'CHOISY LE ROI',
                'slug' => 'val_de_marne',
            ),
            279 => 
            array (
                'code_departement' => 94,
                'cp' => 94260,
                'id' => 1342,
                'insee' => 94034,
                'name' => 'FRESNES',
                'slug' => 'val_de_marne',
            ),
            280 => 
            array (
                'code_departement' => 94,
                'cp' => 94490,
                'id' => 1343,
                'insee' => 94055,
                'name' => 'ORMESSON SUR MARNE',
                'slug' => 'val_de_marne',
            ),
            281 => 
            array (
                'code_departement' => 94,
                'cp' => 94170,
                'id' => 1344,
                'insee' => 94058,
                'name' => 'LE PERREUX SUR MARNE',
                'slug' => 'val_de_marne',
            ),
            282 => 
            array (
                'code_departement' => 94,
                'cp' => 94420,
                'id' => 1345,
                'insee' => 94059,
                'name' => 'LE PLESSIS TREVISE',
                'slug' => 'val_de_marne',
            ),
            283 => 
            array (
                'code_departement' => 94,
                'cp' => 94100,
                'id' => 1346,
                'insee' => 94068,
                'name' => 'ST MAUR DES FOSSES',
                'slug' => 'val_de_marne',
            ),
            284 => 
            array (
                'code_departement' => 94,
                'cp' => 94410,
                'id' => 1347,
                'insee' => 94069,
                'name' => 'ST MAURICE',
                'slug' => 'val_de_marne',
            ),
            285 => 
            array (
                'code_departement' => 94,
                'cp' => 94350,
                'id' => 1348,
                'insee' => 94079,
                'name' => 'VILLIERS SUR MARNE',
                'slug' => 'val_de_marne',
            ),
            286 => 
            array (
                'code_departement' => 95,
                'cp' => 95710,
                'id' => 1349,
                'insee' => 95011,
                'name' => 'AMBLEVILLE',
                'slug' => 'val_doise',
            ),
            287 => 
            array (
                'code_departement' => 95,
                'cp' => 95570,
                'id' => 1350,
                'insee' => 95028,
                'name' => 'ATTAINVILLE',
                'slug' => 'val_doise',
            ),
            288 => 
            array (
                'code_departement' => 95,
                'cp' => 95560,
                'id' => 1351,
                'insee' => 95042,
                'name' => 'BAILLET EN FRANCE',
                'slug' => 'val_doise',
            ),
            289 => 
            array (
                'code_departement' => 95,
                'cp' => 95260,
                'id' => 1352,
                'insee' => 95052,
                'name' => 'BEAUMONT SUR OISE',
                'slug' => 'val_doise',
            ),
            290 => 
            array (
                'code_departement' => 95,
                'cp' => 95800,
                'id' => 1353,
                'insee' => 95127,
                'name' => 'CERGY',
                'slug' => 'val_doise',
            ),
            291 => 
            array (
                'code_departement' => 95,
                'cp' => 95660,
                'id' => 1354,
                'insee' => 95134,
                'name' => 'CHAMPAGNE SUR OISE',
                'slug' => 'val_doise',
            ),
            292 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 1355,
                'insee' => 95139,
                'name' => 'LA CHAPELLE EN VEXIN',
                'slug' => 'val_doise',
            ),
            293 => 
            array (
                'code_departement' => 95,
                'cp' => 95380,
                'id' => 1356,
                'insee' => 95154,
                'name' => 'CHENNEVIERES LES LOUVRES',
                'slug' => 'val_doise',
            ),
            294 => 
            array (
                'code_departement' => 95,
                'cp' => 95830,
                'id' => 1357,
                'insee' => 95177,
                'name' => 'CORMEILLES EN VEXIN',
                'slug' => 'val_doise',
            ),
            295 => 
            array (
                'code_departement' => 95,
                'cp' => 95650,
                'id' => 1358,
                'insee' => 95181,
                'name' => 'COURCELLES SUR VIOSNE',
                'slug' => 'val_doise',
            ),
            296 => 
            array (
                'code_departement' => 95,
                'cp' => 95800,
                'id' => 1359,
                'insee' => 95183,
                'name' => 'COURDIMANCHE',
                'slug' => 'val_doise',
            ),
            297 => 
            array (
                'code_departement' => 95,
                'cp' => 95300,
                'id' => 1360,
                'insee' => 95211,
                'name' => 'ENNERY',
                'slug' => 'val_doise',
            ),
            298 => 
            array (
                'code_departement' => 95,
                'cp' => 95270,
                'id' => 1361,
                'insee' => 95214,
                'name' => 'EPINAY CHAMPLATREUX',
                'slug' => 'val_doise',
            ),
            299 => 
            array (
                'code_departement' => 95,
                'cp' => 95460,
                'id' => 1362,
                'insee' => 95229,
                'name' => 'EZANVILLE',
                'slug' => 'val_doise',
            ),
            300 => 
            array (
                'code_departement' => 77,
                'cp' => 77890,
                'id' => 1363,
                'insee' => 77009,
                'name' => 'ARVILLE',
                'slug' => 'seine_et_marne',
            ),
            301 => 
            array (
                'code_departement' => 95,
                'cp' => 95650,
                'id' => 1364,
                'insee' => 95271,
                'name' => 'GENICOURT',
                'slug' => 'val_doise',
            ),
            302 => 
            array (
                'code_departement' => 95,
                'cp' => 95410,
                'id' => 1366,
                'insee' => 95288,
                'name' => 'GROSLAY',
                'slug' => 'val_doise',
            ),
            303 => 
            array (
                'code_departement' => 77,
                'cp' => 77480,
                'id' => 1367,
                'insee' => 77015,
                'name' => 'BABY',
                'slug' => 'seine_et_marne',
            ),
            304 => 
            array (
                'code_departement' => 95,
                'cp' => 95300,
                'id' => 1368,
                'insee' => 95308,
                'name' => 'HEROUVILLE EN VEXIN',
                'slug' => 'val_doise',
            ),
            305 => 
            array (
                'code_departement' => 77,
                'cp' => 77700,
                'id' => 1369,
                'insee' => 77018,
                'name' => 'BAILLY ROMAINVILLIERS',
                'slug' => 'seine_et_marne',
            ),
            306 => 
            array (
                'code_departement' => 95,
                'cp' => 95300,
                'id' => 1370,
                'insee' => 95341,
                'name' => 'LIVILLIERS',
                'slug' => 'val_doise',
            ),
            307 => 
            array (
                'code_departement' => 77,
                'cp' => 77560,
                'id' => 1371,
                'insee' => 77026,
                'name' => 'BEAUCHERY ST MARTIN',
                'slug' => 'seine_et_marne',
            ),
            308 => 
            array (
                'code_departement' => 95,
                'cp' => 95560,
                'id' => 1372,
                'insee' => 95353,
                'name' => 'MAFFLIERS',
                'slug' => 'val_doise',
            ),
            309 => 
            array (
                'code_departement' => 77,
                'cp' => 77115,
                'id' => 1373,
                'insee' => 77034,
                'name' => 'BLANDY',
                'slug' => 'seine_et_marne',
            ),
            310 => 
            array (
                'code_departement' => 95,
                'cp' => 95850,
                'id' => 1374,
                'insee' => 95365,
                'name' => 'MAREIL EN FRANCE',
                'slug' => 'val_doise',
            ),
            311 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 1375,
                'insee' => 77041,
                'name' => 'BOISSY AUX CAILLES',
                'slug' => 'seine_et_marne',
            ),
            312 => 
            array (
                'code_departement' => 95,
                'cp' => 95670,
                'id' => 1376,
                'insee' => 95371,
                'name' => 'MARLY LA VILLE',
                'slug' => 'val_doise',
            ),
            313 => 
            array (
                'code_departement' => 77,
                'cp' => 77580,
                'id' => 1377,
                'insee' => 77047,
                'name' => 'BOULEURS',
                'slug' => 'seine_et_marne',
            ),
            314 => 
            array (
                'code_departement' => 95,
                'cp' => 95180,
                'id' => 1378,
                'insee' => 95388,
                'name' => 'MENUCOURT',
                'slug' => 'val_doise',
            ),
            315 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 1379,
                'insee' => 77066,
                'name' => 'CERNEUX',
                'slug' => 'seine_et_marne',
            ),
            316 => 
            array (
                'code_departement' => 95,
                'cp' => 95160,
                'id' => 1380,
                'insee' => 95428,
                'name' => 'MONTMORENCY',
                'slug' => 'val_doise',
            ),
            317 => 
            array (
                'code_departement' => 77,
                'cp' => 77930,
                'id' => 1381,
                'insee' => 77069,
                'name' => 'CHAILLY EN BIERE',
                'slug' => 'seine_et_marne',
            ),
            318 => 
            array (
                'code_departement' => 95,
                'cp' => 95260,
                'id' => 1382,
                'insee' => 95436,
                'name' => 'MOURS',
                'slug' => 'val_doise',
            ),
            319 => 
            array (
                'code_departement' => 77,
                'cp' => 77390,
                'id' => 1383,
                'insee' => 77081,
                'name' => 'CHAMPDEUIL',
                'slug' => 'seine_et_marne',
            ),
            320 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 1384,
                'insee' => 95459,
                'name' => 'NUCOURT',
                'slug' => 'val_doise',
            ),
            321 => 
            array (
                'code_departement' => 77,
                'cp' => 77720,
                'id' => 1385,
                'insee' => 77086,
                'name' => 'LA CHAPELLE GAUTHIER',
                'slug' => 'seine_et_marne',
            ),
            322 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 1386,
                'insee' => 95462,
                'name' => 'OMERVILLE',
                'slug' => 'val_doise',
            ),
            323 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 1387,
                'insee' => 77088,
                'name' => 'LA CHAPELLE LA REINE',
                'slug' => 'seine_et_marne',
            ),
            324 => 
            array (
                'code_departement' => 95,
                'cp' => 95520,
                'id' => 1388,
                'insee' => 95476,
                'name' => 'OSNY',
                'slug' => 'val_doise',
            ),
            325 => 
            array (
                'code_departement' => 77,
                'cp' => 77410,
                'id' => 1389,
                'insee' => 77095,
                'name' => 'CHARNY',
                'slug' => 'seine_et_marne',
            ),
            326 => 
            array (
                'code_departement' => 95,
                'cp' => 95720,
                'id' => 1390,
                'insee' => 95492,
                'name' => 'LE PLESSIS GASSOT',
                'slug' => 'val_doise',
            ),
            327 => 
            array (
                'code_departement' => 77,
                'cp' => 77820,
                'id' => 1391,
                'insee' => 77100,
                'name' => 'LE CHATELET EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            328 => 
            array (
                'code_departement' => 95,
                'cp' => 95270,
                'id' => 1392,
                'insee' => 95493,
                'name' => 'LE PLESSIS LUZARCHES',
                'slug' => 'val_doise',
            ),
            329 => 
            array (
                'code_departement' => 77,
                'cp' => 77126,
                'id' => 1393,
                'insee' => 77101,
                'name' => 'CHATENAY SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            330 => 
            array (
                'code_departement' => 95,
                'cp' => 95650,
                'id' => 1394,
                'insee' => 95510,
                'name' => 'PUISEUX PONTOISE',
                'slug' => 'val_doise',
            ),
            331 => 
            array (
                'code_departement' => 95,
                'cp' => 95420,
                'id' => 1396,
                'insee' => 95554,
                'name' => 'ST GERVAIS',
                'slug' => 'val_doise',
            ),
            332 => 
            array (
                'code_departement' => 95,
                'cp' => 95210,
                'id' => 1398,
                'insee' => 95555,
                'name' => 'ST GRATIEN',
                'slug' => 'val_doise',
            ),
            333 => 
            array (
                'code_departement' => 77,
                'cp' => 77440,
                'id' => 1399,
                'insee' => 77126,
                'name' => 'CONGIS SUR THEROUANNE',
                'slug' => 'seine_et_marne',
            ),
            334 => 
            array (
                'code_departement' => 95,
                'cp' => 95310,
                'id' => 1400,
                'insee' => 95572,
                'name' => 'ST OUEN L AUMONE',
                'slug' => 'val_doise',
            ),
            335 => 
            array (
                'code_departement' => 77,
                'cp' => 77170,
                'id' => 1401,
                'insee' => 77127,
                'name' => 'COUBERT',
                'slug' => 'seine_et_marne',
            ),
            336 => 
            array (
                'code_departement' => 95,
                'cp' => 95390,
                'id' => 1402,
                'insee' => 95574,
                'name' => 'ST PRIX',
                'slug' => 'val_doise',
            ),
            337 => 
            array (
                'code_departement' => 77,
                'cp' => 77450,
                'id' => 1403,
                'insee' => 77171,
                'name' => 'ESBLY',
                'slug' => 'seine_et_marne',
            ),
            338 => 
            array (
                'code_departement' => 95,
                'cp' => 95470,
                'id' => 1404,
                'insee' => 95580,
                'name' => 'ST WITZ',
                'slug' => 'val_doise',
            ),
            339 => 
            array (
                'code_departement' => 77,
                'cp' => 77139,
                'id' => 1405,
                'insee' => 77173,
                'name' => 'ETREPILLY',
                'slug' => 'seine_et_marne',
            ),
            340 => 
            array (
                'code_departement' => 95,
                'cp' => 95490,
                'id' => 1406,
                'insee' => 95637,
                'name' => 'VAUREAL',
                'slug' => 'val_doise',
            ),
            341 => 
            array (
                'code_departement' => 77,
                'cp' => 77166,
                'id' => 1407,
                'insee' => 77175,
                'name' => 'EVRY GREGY SUR YERRE',
                'slug' => 'seine_et_marne',
            ),
            342 => 
            array (
                'code_departement' => 95,
                'cp' => 95510,
                'id' => 1408,
                'insee' => 95651,
                'name' => 'VETHEUIL',
                'slug' => 'val_doise',
            ),
            343 => 
            array (
                'code_departement' => 77,
                'cp' => 77590,
                'id' => 1409,
                'insee' => 77188,
                'name' => 'FONTAINE LE PORT',
                'slug' => 'seine_et_marne',
            ),
            344 => 
            array (
                'code_departement' => 95,
                'cp' => 95380,
                'id' => 1410,
                'insee' => 95675,
                'name' => 'VILLERON',
                'slug' => 'val_doise',
            ),
            345 => 
            array (
                'code_departement' => 77,
                'cp' => 77390,
                'id' => 1411,
                'insee' => 77195,
                'name' => 'FOUJU',
                'slug' => 'seine_et_marne',
            ),
            346 => 
            array (
                'code_departement' => 95,
                'cp' => 95510,
                'id' => 1412,
                'insee' => 95676,
                'name' => 'VILLERS EN ARTHIES',
                'slug' => 'val_doise',
            ),
            347 => 
            array (
                'code_departement' => 77,
                'cp' => 77910,
                'id' => 1413,
                'insee' => 77203,
                'name' => 'GERMIGNY L EVEQUE',
                'slug' => 'seine_et_marne',
            ),
            348 => 
            array (
                'code_departement' => 77,
                'cp' => 77118,
                'id' => 1415,
                'insee' => 77212,
                'name' => 'GRAVON',
                'slug' => 'seine_et_marne',
            ),
            349 => 
            array (
                'code_departement' => 77,
                'cp' => 77850,
                'id' => 1416,
                'insee' => 77226,
                'name' => 'HERICY',
                'slug' => 'seine_et_marne',
            ),
            350 => 
            array (
                'code_departement' => 77,
                'cp' => 77114,
                'id' => 1417,
                'insee' => 77227,
                'name' => 'HERME',
                'slug' => 'seine_et_marne',
            ),
            351 => 
            array (
                'code_departement' => 77,
                'cp' => 77600,
                'id' => 1418,
                'insee' => 77237,
                'name' => 'JOSSIGNY',
                'slug' => 'seine_et_marne',
            ),
            352 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 1419,
                'insee' => 77240,
                'name' => 'JOUY SUR MORIN',
                'slug' => 'seine_et_marne',
            ),
            353 => 
            array (
                'code_departement' => 77,
                'cp' => 77400,
                'id' => 1420,
                'insee' => 77243,
                'name' => 'LAGNY SUR MARNE',
                'slug' => 'seine_et_marne',
            ),
            354 => 
            array (
                'code_departement' => 77,
                'cp' => 77220,
                'id' => 1421,
                'insee' => 77254,
                'name' => 'LIVERDY EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            355 => 
            array (
                'code_departement' => 77,
                'cp' => 77230,
                'id' => 1422,
                'insee' => 77259,
                'name' => 'LONGPERRIER',
                'slug' => 'seine_et_marne',
            ),
            356 => 
            array (
                'code_departement' => 77,
                'cp' => 77570,
                'id' => 1423,
                'insee' => 77267,
                'name' => 'LA MADELEINE SUR LOING',
                'slug' => 'seine_et_marne',
            ),
            357 => 
            array (
                'code_departement' => 77,
                'cp' => 77120,
                'id' => 1424,
                'insee' => 77278,
                'name' => 'MAROLLES EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            358 => 
            array (
                'code_departement' => 77,
                'cp' => 77130,
                'id' => 1425,
                'insee' => 77279,
                'name' => 'MAROLLES SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            359 => 
            array (
                'code_departement' => 77,
                'cp' => 77145,
                'id' => 1426,
                'insee' => 77283,
                'name' => 'MAY EN MULTIEN',
                'slug' => 'seine_et_marne',
            ),
            360 => 
            array (
                'code_departement' => 77,
                'cp' => 77410,
                'id' => 1427,
                'insee' => 77292,
                'name' => 'MESSY',
                'slug' => 'seine_et_marne',
            ),
            361 => 
            array (
                'code_departement' => 77,
                'cp' => 77130,
                'id' => 1428,
                'insee' => 77293,
                'name' => 'MISY SUR YONNE',
                'slug' => 'seine_et_marne',
            ),
            362 => 
            array (
                'code_departement' => 77,
                'cp' => 77950,
                'id' => 1429,
                'insee' => 77295,
                'name' => 'MOISENAY',
                'slug' => 'seine_et_marne',
            ),
            363 => 
            array (
                'code_departement' => 77,
                'cp' => 77140,
                'id' => 1430,
                'insee' => 77302,
                'name' => 'MONTCOURT FROMONVILLE',
                'slug' => 'seine_et_marne',
            ),
            364 => 
            array (
                'code_departement' => 77,
                'cp' => 77122,
                'id' => 1431,
                'insee' => 77309,
                'name' => 'MONTHYON',
                'slug' => 'seine_et_marne',
            ),
            365 => 
            array (
                'code_departement' => 77,
                'cp' => 77480,
                'id' => 1432,
                'insee' => 77310,
                'name' => 'MONTIGNY LE GUESDIER',
                'slug' => 'seine_et_marne',
            ),
            366 => 
            array (
                'code_departement' => 77,
                'cp' => 77520,
                'id' => 1433,
                'insee' => 77311,
                'name' => 'MONTIGNY LENCOUP',
                'slug' => 'seine_et_marne',
            ),
            367 => 
            array (
                'code_departement' => 77,
                'cp' => 77450,
                'id' => 1434,
                'insee' => 77315,
                'name' => 'MONTRY',
                'slug' => 'seine_et_marne',
            ),
            368 => 
            array (
                'code_departement' => 77,
                'cp' => 77480,
                'id' => 1436,
                'insee' => 77321,
                'name' => 'MOUSSEAUX LES BRAY',
                'slug' => 'seine_et_marne',
            ),
            369 => 
            array (
                'code_departement' => 77,
                'cp' => 77176,
                'id' => 1437,
                'insee' => 77326,
                'name' => 'NANDY',
                'slug' => 'seine_et_marne',
            ),
            370 => 
            array (
                'code_departement' => 77,
                'cp' => 77940,
                'id' => 1439,
                'insee' => 77338,
                'name' => 'NOISY RUDIGNON',
                'slug' => 'seine_et_marne',
            ),
            371 => 
            array (
                'code_departement' => 77,
                'cp' => 77890,
                'id' => 1440,
                'insee' => 77342,
                'name' => 'OBSONVILLE',
                'slug' => 'seine_et_marne',
            ),
            372 => 
            array (
                'code_departement' => 77,
                'cp' => 77134,
                'id' => 1441,
                'insee' => 77347,
                'name' => 'LES ORMES SUR VOULZIE',
                'slug' => 'seine_et_marne',
            ),
            373 => 
            array (
                'code_departement' => 77,
                'cp' => 77710,
                'id' => 1442,
                'insee' => 77353,
                'name' => 'PALEY',
                'slug' => 'seine_et_marne',
            ),
            374 => 
            array (
                'code_departement' => 77,
                'cp' => 77124,
                'id' => 1443,
                'insee' => 77358,
                'name' => 'PENCHARD',
                'slug' => 'seine_et_marne',
            ),
            375 => 
            array (
                'code_departement' => 77,
                'cp' => 77165,
                'id' => 1444,
                'insee' => 77364,
                'name' => 'LE PLESSIS AUX BOIS',
                'slug' => 'seine_et_marne',
            ),
            376 => 
            array (
                'code_departement' => 77,
                'cp' => 77340,
                'id' => 1446,
                'insee' => 77373,
                'name' => 'PONTAULT COMBAULT',
                'slug' => 'seine_et_marne',
            ),
            377 => 
            array (
                'code_departement' => 77,
                'cp' => 77370,
                'id' => 1447,
                'insee' => 77383,
                'name' => 'RAMPILLON',
                'slug' => 'seine_et_marne',
            ),
            378 => 
            array (
                'code_departement' => 77,
                'cp' => 77680,
                'id' => 1448,
                'insee' => 77390,
                'name' => 'ROISSY EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            379 => 
            array (
                'code_departement' => 77,
                'cp' => 77510,
                'id' => 1449,
                'insee' => 77398,
                'name' => 'SABLONNIERES',
                'slug' => 'seine_et_marne',
            ),
            380 => 
            array (
                'code_departement' => 77,
                'cp' => 77510,
                'id' => 1450,
                'insee' => 77406,
                'name' => 'ST DENIS LES REBAIS',
                'slug' => 'seine_et_marne',
            ),
            381 => 
            array (
                'code_departement' => 77,
                'cp' => 77130,
                'id' => 1451,
                'insee' => 77409,
                'name' => 'ST GERMAIN LAVAL',
                'slug' => 'seine_et_marne',
            ),
            382 => 
            array (
                'code_departement' => 77,
                'cp' => 77230,
                'id' => 1452,
                'insee' => 77420,
                'name' => 'ST MARD',
                'slug' => 'seine_et_marne',
            ),
            383 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 1453,
                'insee' => 77424,
                'name' => 'ST MARTIN DU BOSCHET',
                'slug' => 'seine_et_marne',
            ),
            384 => 
            array (
                'code_departement' => 77,
                'cp' => 77720,
                'id' => 1454,
                'insee' => 77426,
                'name' => 'ST MERY',
                'slug' => 'seine_et_marne',
            ),
            385 => 
            array (
                'code_departement' => 77,
                'cp' => 77165,
                'id' => 1456,
                'insee' => 77437,
                'name' => 'ST SOUPPLETS',
                'slug' => 'seine_et_marne',
            ),
            386 => 
            array (
                'code_departement' => 77,
                'cp' => 77920,
                'id' => 1457,
                'insee' => 77441,
                'name' => 'SAMOIS SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            387 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 1458,
                'insee' => 77444,
                'name' => 'SANCY LES PROVINS',
                'slug' => 'seine_et_marne',
            ),
            388 => 
            array (
                'code_departement' => 77,
                'cp' => 77650,
                'id' => 1459,
                'insee' => 77446,
                'name' => 'SAVINS',
                'slug' => 'seine_et_marne',
            ),
            389 => 
            array (
                'code_departement' => 77,
                'cp' => 77170,
                'id' => 1460,
                'insee' => 77450,
                'name' => 'SERVON',
                'slug' => 'seine_et_marne',
            ),
            390 => 
            array (
                'code_departement' => 77,
                'cp' => 77520,
                'id' => 1461,
                'insee' => 77452,
                'name' => 'SIGY',
                'slug' => 'seine_et_marne',
            ),
            391 => 
            array (
                'code_departement' => 77,
                'cp' => 77111,
                'id' => 1462,
                'insee' => 77455,
                'name' => 'SOIGNOLLES EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            392 => 
            array (
                'code_departement' => 77,
                'cp' => 77650,
                'id' => 1463,
                'insee' => 77456,
                'name' => 'SOISY BOUY',
                'slug' => 'seine_et_marne',
            ),
            393 => 
            array (
                'code_departement' => 77,
                'cp' => 77440,
                'id' => 1464,
                'insee' => 77460,
                'name' => 'TANCROU',
                'slug' => 'seine_et_marne',
            ),
            394 => 
            array (
                'code_departement' => 77,
                'cp' => 77810,
                'id' => 1465,
                'insee' => 77463,
                'name' => 'THOMERY',
                'slug' => 'seine_et_marne',
            ),
            395 => 
            array (
                'code_departement' => 77,
                'cp' => 77400,
                'id' => 1466,
                'insee' => 77464,
                'name' => 'THORIGNY SUR MARNE',
                'slug' => 'seine_et_marne',
            ),
            396 => 
            array (
                'code_departement' => 77,
                'cp' => 77940,
                'id' => 1467,
                'insee' => 77465,
                'name' => 'THOURY FEROTTES',
                'slug' => 'seine_et_marne',
            ),
            397 => 
            array (
                'code_departement' => 77,
                'cp' => 77510,
                'id' => 1468,
                'insee' => 77472,
                'name' => 'LA TRETOIRE',
                'slug' => 'seine_et_marne',
            ),
            398 => 
            array (
                'code_departement' => 77,
                'cp' => 77450,
                'id' => 1469,
                'insee' => 77474,
                'name' => 'TRILBARDOU',
                'slug' => 'seine_et_marne',
            ),
            399 => 
            array (
                'code_departement' => 77,
                'cp' => 77440,
                'id' => 1470,
                'insee' => 77476,
                'name' => 'TROCY EN MULTIEN',
                'slug' => 'seine_et_marne',
            ),
            400 => 
            array (
                'code_departement' => 77,
                'cp' => 77130,
                'id' => 1471,
                'insee' => 77482,
                'name' => 'VARENNES SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            401 => 
            array (
                'code_departement' => 77,
                'cp' => 77000,
                'id' => 1472,
                'insee' => 77487,
                'name' => 'VAUX LE PENIL',
                'slug' => 'seine_et_marne',
            ),
            402 => 
            array (
                'code_departement' => 77,
                'cp' => 77510,
                'id' => 1473,
                'insee' => 77492,
                'name' => 'VERDELOT',
                'slug' => 'seine_et_marne',
            ),
            403 => 
            array (
                'code_departement' => 78,
                'cp' => 78570,
                'id' => 1475,
                'insee' => 78015,
                'name' => 'ANDRESY',
                'slug' => 'yvelines',
            ),
            404 => 
            array (
                'code_departement' => 78,
                'cp' => 78200,
                'id' => 1479,
                'insee' => 78072,
                'name' => 'BOINVILLIERS',
                'slug' => 'yvelines',
            ),
            405 => 
            array (
                'code_departement' => 78,
                'cp' => 78270,
                'id' => 1480,
                'insee' => 78089,
                'name' => 'BONNIERES SUR SEINE',
                'slug' => 'yvelines',
            ),
            406 => 
            array (
                'code_departement' => 78,
                'cp' => 78200,
                'id' => 1481,
                'insee' => 78118,
                'name' => 'BUCHELAY',
                'slug' => 'yvelines',
            ),
            407 => 
            array (
                'code_departement' => 78,
                'cp' => 78130,
                'id' => 1482,
                'insee' => 78140,
                'name' => 'CHAPET',
                'slug' => 'yvelines',
            ),
            408 => 
            array (
                'code_departement' => 78,
                'cp' => 78400,
                'id' => 1483,
                'insee' => 78146,
                'name' => 'CHATOU',
                'slug' => 'yvelines',
            ),
            409 => 
            array (
                'code_departement' => 78,
                'cp' => 78450,
                'id' => 1484,
                'insee' => 78152,
                'name' => 'CHAVENAY',
                'slug' => 'yvelines',
            ),
            410 => 
            array (
                'code_departement' => 78,
                'cp' => 78700,
                'id' => 1485,
                'insee' => 78172,
                'name' => 'CONFLANS STE HONORINE',
                'slug' => 'yvelines',
            ),
            411 => 
            array (
                'code_departement' => 78,
                'cp' => 78550,
                'id' => 1486,
                'insee' => 78194,
                'name' => 'DANNEMARIE',
                'slug' => 'yvelines',
            ),
            412 => 
            array (
                'code_departement' => 78,
                'cp' => 78440,
                'id' => 1487,
                'insee' => 78202,
                'name' => 'DROCOURT',
                'slug' => 'yvelines',
            ),
            413 => 
            array (
                'code_departement' => 78,
                'cp' => 78920,
                'id' => 1488,
                'insee' => 78206,
                'name' => 'ECQUEVILLY',
                'slug' => 'yvelines',
            ),
            414 => 
            array (
                'code_departement' => 78,
                'cp' => 78990,
                'id' => 1489,
                'insee' => 78208,
                'name' => 'ELANCOURT',
                'slug' => 'yvelines',
            ),
            415 => 
            array (
                'code_departement' => 78,
                'cp' => 78910,
                'id' => 1490,
                'insee' => 78236,
                'name' => 'FLEXANVILLE',
                'slug' => 'yvelines',
            ),
            416 => 
            array (
                'code_departement' => 78,
                'cp' => 78840,
                'id' => 1491,
                'insee' => 78255,
                'name' => 'FRENEUSE',
                'slug' => 'yvelines',
            ),
            417 => 
            array (
                'code_departement' => 78,
                'cp' => 78270,
                'id' => 1492,
                'insee' => 78276,
                'name' => 'GOMMECOURT',
                'slug' => 'yvelines',
            ),
            418 => 
            array (
                'code_departement' => 78,
                'cp' => 78790,
                'id' => 1493,
                'insee' => 78300,
                'name' => 'HARGEVILLE',
                'slug' => 'yvelines',
            ),
            419 => 
            array (
                'code_departement' => 78,
                'cp' => 78580,
                'id' => 1494,
                'insee' => 78305,
                'name' => 'HERBEVILLE',
                'slug' => 'yvelines',
            ),
            420 => 
            array (
                'code_departement' => 78,
                'cp' => 78800,
                'id' => 1495,
                'insee' => 78311,
                'name' => 'HOUILLES',
                'slug' => 'yvelines',
            ),
            421 => 
            array (
                'code_departement' => 78,
                'cp' => 78440,
                'id' => 1496,
                'insee' => 78314,
                'name' => 'ISSOU',
                'slug' => 'yvelines',
            ),
            422 => 
            array (
                'code_departement' => 78,
                'cp' => 78350,
                'id' => 1498,
                'insee' => 78322,
                'name' => 'JOUY EN JOSAS',
                'slug' => 'yvelines',
            ),
            423 => 
            array (
                'code_departement' => 78,
                'cp' => 78520,
                'id' => 1499,
                'insee' => 78335,
                'name' => 'LIMAY',
                'slug' => 'yvelines',
            ),
            424 => 
            array (
                'code_departement' => 78,
                'cp' => 78350,
                'id' => 1500,
                'insee' => 78343,
                'name' => 'LES LOGES EN JOSAS',
                'slug' => 'yvelines',
            ),
            425 => 
            array (
                'code_departement' => 78,
                'cp' => 78730,
                'id' => 1501,
                'insee' => 78349,
                'name' => 'LONGVILLIERS',
                'slug' => 'yvelines',
            ),
            426 => 
            array (
                'code_departement' => 78,
                'cp' => 78200,
                'id' => 1502,
                'insee' => 78361,
                'name' => 'MANTES LA JOLIE',
                'slug' => 'yvelines',
            ),
            427 => 
            array (
                'code_departement' => 78,
                'cp' => 78124,
                'id' => 1503,
                'insee' => 78368,
                'name' => 'MAREIL SUR MAULDRE',
                'slug' => 'yvelines',
            ),
            428 => 
            array (
                'code_departement' => 78,
                'cp' => 78200,
                'id' => 1505,
                'insee' => 78385,
                'name' => 'MENERVILLE',
                'slug' => 'yvelines',
            ),
            429 => 
            array (
                'code_departement' => 78,
                'cp' => 78270,
                'id' => 1506,
                'insee' => 78391,
                'name' => 'MERICOURT',
                'slug' => 'yvelines',
            ),
            430 => 
            array (
                'code_departement' => 78,
                'cp' => 78440,
                'id' => 1508,
                'insee' => 78416,
                'name' => 'MONTALET LE BOIS',
                'slug' => 'yvelines',
            ),
            431 => 
            array (
                'code_departement' => 78,
                'cp' => 78180,
                'id' => 1509,
                'insee' => 78423,
                'name' => 'MONTIGNY LE BRETONNEUX',
                'slug' => 'yvelines',
            ),
            432 => 
            array (
                'code_departement' => 78,
                'cp' => 78410,
                'id' => 1510,
                'insee' => 78451,
                'name' => 'NEZEL',
                'slug' => 'yvelines',
            ),
            433 => 
            array (
                'code_departement' => 78,
                'cp' => 78200,
                'id' => 1511,
                'insee' => 78484,
                'name' => 'PERDREAUVILLE',
                'slug' => 'yvelines',
            ),
            434 => 
            array (
                'code_departement' => 78,
                'cp' => 78125,
                'id' => 1513,
                'insee' => 78516,
                'name' => 'RAIZEUX',
                'slug' => 'yvelines',
            ),
            435 => 
            array (
                'code_departement' => 78,
                'cp' => 78440,
                'id' => 1514,
                'insee' => 78536,
                'name' => 'SAILLY',
                'slug' => 'yvelines',
            ),
            436 => 
            array (
                'code_departement' => 78,
                'cp' => 78125,
                'id' => 1515,
                'insee' => 78557,
                'name' => 'ST HILARION',
                'slug' => 'yvelines',
            ),
            437 => 
            array (
                'code_departement' => 78,
                'cp' => 78470,
                'id' => 1516,
                'insee' => 78561,
                'name' => 'ST LAMBERT',
                'slug' => 'yvelines',
            ),
            438 => 
            array (
                'code_departement' => 78,
                'cp' => 78790,
                'id' => 1517,
                'insee' => 78565,
                'name' => 'ST MARTIN DES CHAMPS',
                'slug' => 'yvelines',
            ),
            439 => 
            array (
                'code_departement' => 78,
                'cp' => 78910,
                'id' => 1519,
                'insee' => 78605,
                'name' => 'TACOIGNIERES',
                'slug' => 'yvelines',
            ),
            440 => 
            array (
                'code_departement' => 78,
                'cp' => 78770,
                'id' => 1520,
                'insee' => 78616,
                'name' => 'THOIRY',
                'slug' => 'yvelines',
            ),
            441 => 
            array (
                'code_departement' => 78,
                'cp' => 78190,
                'id' => 1521,
                'insee' => 78621,
                'name' => 'TRAPPES',
                'slug' => 'yvelines',
            ),
            442 => 
            array (
                'code_departement' => 78,
                'cp' => 78490,
                'id' => 1522,
                'insee' => 78623,
                'name' => 'LE TREMBLAY SUR MAULDRE',
                'slug' => 'yvelines',
            ),
            443 => 
            array (
                'code_departement' => 78,
                'cp' => 78110,
                'id' => 1525,
                'insee' => 78650,
                'name' => 'LE VESINET',
                'slug' => 'yvelines',
            ),
            444 => 
            array (
                'code_departement' => 78,
                'cp' => 78960,
                'id' => 1526,
                'insee' => 78688,
                'name' => 'VOISINS LE BRETONNEUX',
                'slug' => 'yvelines',
            ),
            445 => 
            array (
                'code_departement' => 77,
                'cp' => 77390,
                'id' => 1527,
                'insee' => 77007,
                'name' => 'ARGENTIERES',
                'slug' => 'seine_et_marne',
            ),
            446 => 
            array (
                'code_departement' => 77,
                'cp' => 77570,
                'id' => 1528,
                'insee' => 77011,
                'name' => 'AUFFERVILLE',
                'slug' => 'seine_et_marne',
            ),
            447 => 
            array (
                'code_departement' => 77,
                'cp' => 77167,
                'id' => 1529,
                'insee' => 77016,
                'name' => 'BAGNEAUX SUR LOING',
                'slug' => 'seine_et_marne',
            ),
            448 => 
            array (
                'code_departement' => 77,
                'cp' => 77130,
                'id' => 1531,
                'insee' => 77021,
                'name' => 'BARBEY',
                'slug' => 'seine_et_marne',
            ),
            449 => 
            array (
                'code_departement' => 77,
                'cp' => 77910,
                'id' => 1532,
                'insee' => 77023,
                'name' => 'BARCY',
                'slug' => 'seine_et_marne',
            ),
            450 => 
            array (
                'code_departement' => 77,
                'cp' => 77510,
                'id' => 1534,
                'insee' => 77030,
                'name' => 'BELLOT',
                'slug' => 'seine_et_marne',
            ),
            451 => 
            array (
                'code_departement' => 77,
                'cp' => 77570,
                'id' => 1535,
                'insee' => 77045,
                'name' => 'BOUGLIGNY',
                'slug' => 'seine_et_marne',
            ),
            452 => 
            array (
                'code_departement' => 77,
                'cp' => 77240,
                'id' => 1536,
                'insee' => 77067,
                'name' => 'CESSON',
                'slug' => 'seine_et_marne',
            ),
            453 => 
            array (
                'code_departement' => 77,
                'cp' => 77520,
                'id' => 1537,
                'insee' => 77068,
                'name' => 'CESSOY EN MONTOIS',
                'slug' => 'seine_et_marne',
            ),
            454 => 
            array (
                'code_departement' => 77,
                'cp' => 77120,
                'id' => 1538,
                'insee' => 77070,
                'name' => 'CHAILLY EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            455 => 
            array (
                'code_departement' => 77,
                'cp' => 77540,
                'id' => 1539,
                'insee' => 77087,
                'name' => 'LA CHAPELLE IGER',
                'slug' => 'seine_et_marne',
            ),
            456 => 
            array (
                'code_departement' => 77,
                'cp' => 77370,
                'id' => 1541,
                'insee' => 77098,
                'name' => 'CHATEAUBLEAU',
                'slug' => 'seine_et_marne',
            ),
            457 => 
            array (
                'code_departement' => 77,
                'cp' => 77167,
                'id' => 1542,
                'insee' => 77102,
                'name' => 'CHATENOY',
                'slug' => 'seine_et_marne',
            ),
            458 => 
            array (
                'code_departement' => 77,
                'cp' => 77570,
                'id' => 1543,
                'insee' => 77110,
                'name' => 'CHENOU',
                'slug' => 'seine_et_marne',
            ),
            459 => 
            array (
                'code_departement' => 77,
                'cp' => 77710,
                'id' => 1544,
                'insee' => 77115,
                'name' => 'CHEVRY EN SEREINE',
                'slug' => 'seine_et_marne',
            ),
            460 => 
            array (
                'code_departement' => 77,
                'cp' => 77730,
                'id' => 1545,
                'insee' => 77117,
                'name' => 'CITRY',
                'slug' => 'seine_et_marne',
            ),
            461 => 
            array (
                'code_departement' => 77,
                'cp' => 77126,
                'id' => 1546,
                'insee' => 77133,
                'name' => 'COURCELLES EN BASSEE',
                'slug' => 'seine_et_marne',
            ),
            462 => 
            array (
                'code_departement' => 77,
                'cp' => 77540,
                'id' => 1547,
                'insee' => 77135,
                'name' => 'COURPALAY',
                'slug' => 'seine_et_marne',
            ),
            463 => 
            array (
                'code_departement' => 77,
                'cp' => 77390,
                'id' => 1548,
                'insee' => 77136,
                'name' => 'COURQUETAINE',
                'slug' => 'seine_et_marne',
            ),
            464 => 
            array (
                'code_departement' => 77,
                'cp' => 77154,
                'id' => 1549,
                'insee' => 77140,
                'name' => 'COUTENCON',
                'slug' => 'seine_et_marne',
            ),
            465 => 
            array (
                'code_departement' => 77,
                'cp' => 77580,
                'id' => 1550,
                'insee' => 77141,
                'name' => 'COUTEVROULT',
                'slug' => 'seine_et_marne',
            ),
            466 => 
            array (
                'code_departement' => 77,
                'cp' => 77124,
                'id' => 1551,
                'insee' => 77143,
                'name' => 'CREGY LES MEAUX',
                'slug' => 'seine_et_marne',
            ),
            467 => 
            array (
                'code_departement' => 77,
                'cp' => 77390,
                'id' => 1552,
                'insee' => 77145,
                'name' => 'CRISENOY',
                'slug' => 'seine_et_marne',
            ),
            468 => 
            array (
                'code_departement' => 77,
                'cp' => 77370,
                'id' => 1553,
                'insee' => 77147,
                'name' => 'LA CROIX EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            469 => 
            array (
                'code_departement' => 77,
                'cp' => 77165,
                'id' => 1554,
                'insee' => 77150,
                'name' => 'CUISY',
                'slug' => 'seine_et_marne',
            ),
            470 => 
            array (
                'code_departement' => 77,
                'cp' => 77163,
                'id' => 1555,
                'insee' => 77154,
                'name' => 'DAMMARTIN SUR TIGEAUX',
                'slug' => 'seine_et_marne',
            ),
            471 => 
            array (
                'code_departement' => 77,
                'cp' => 77140,
                'id' => 1556,
                'insee' => 77156,
                'name' => 'DARVAULT',
                'slug' => 'seine_et_marne',
            ),
            472 => 
            array (
                'code_departement' => 77,
                'cp' => 77126,
                'id' => 1557,
                'insee' => 77167,
                'name' => 'EGLIGNY',
                'slug' => 'seine_et_marne',
            ),
            473 => 
            array (
                'code_departement' => 77,
                'cp' => 77620,
                'id' => 1558,
                'insee' => 77168,
                'name' => 'EGREVILLE',
                'slug' => 'seine_et_marne',
            ),
            474 => 
            array (
                'code_departement' => 77,
                'cp' => 77940,
                'id' => 1559,
                'insee' => 77172,
                'name' => 'ESMANS',
                'slug' => 'seine_et_marne',
            ),
            475 => 
            array (
                'code_departement' => 77,
                'cp' => 77940,
                'id' => 1561,
                'insee' => 77184,
                'name' => 'FLAGY',
                'slug' => 'seine_et_marne',
            ),
            476 => 
            array (
                'code_departement' => 77,
                'cp' => 77930,
                'id' => 1562,
                'insee' => 77185,
                'name' => 'FLEURY EN BIERE',
                'slug' => 'seine_et_marne',
            ),
            477 => 
            array (
                'code_departement' => 77,
                'cp' => 77370,
                'id' => 1563,
                'insee' => 77190,
                'name' => 'FONTAINS',
                'slug' => 'seine_et_marne',
            ),
            478 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 1564,
                'insee' => 77197,
                'name' => 'FRETOY',
                'slug' => 'seine_et_marne',
            ),
            479 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 1565,
                'insee' => 77198,
                'name' => 'FROMONT',
                'slug' => 'seine_et_marne',
            ),
            480 => 
            array (
                'code_departement' => 77,
                'cp' => 77470,
                'id' => 1566,
                'insee' => 77199,
                'name' => 'FUBLAINES',
                'slug' => 'seine_et_marne',
            ),
            481 => 
            array (
                'code_departement' => 77,
                'cp' => 77610,
                'id' => 1567,
                'insee' => 77229,
                'name' => 'LA HOUSSAYE EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            482 => 
            array (
                'code_departement' => 77,
                'cp' => 77165,
                'id' => 1568,
                'insee' => 77233,
                'name' => 'IVERNY',
                'slug' => 'seine_et_marne',
            ),
            483 => 
            array (
                'code_departement' => 77,
                'cp' => 77450,
                'id' => 1569,
                'insee' => 77234,
                'name' => 'JABLINES',
                'slug' => 'seine_et_marne',
            ),
            484 => 
            array (
                'code_departement' => 77,
                'cp' => 77440,
                'id' => 1570,
                'insee' => 77235,
                'name' => 'JAIGNES',
                'slug' => 'seine_et_marne',
            ),
            485 => 
            array (
                'code_departement' => 77,
                'cp' => 77171,
                'id' => 1571,
                'insee' => 77246,
                'name' => 'LECHELLE',
                'slug' => 'seine_et_marne',
            ),
            486 => 
            array (
                'code_departement' => 77,
                'cp' => 77550,
                'id' => 1572,
                'insee' => 77252,
                'name' => 'LIMOGES FOURCHES',
                'slug' => 'seine_et_marne',
            ),
            487 => 
            array (
                'code_departement' => 77,
                'cp' => 77950,
                'id' => 1574,
                'insee' => 77269,
                'name' => 'MAINCY',
                'slug' => 'seine_et_marne',
            ),
            488 => 
            array (
                'code_departement' => 77,
                'cp' => 77370,
                'id' => 1575,
                'insee' => 77272,
                'name' => 'MAISON ROUGE',
                'slug' => 'seine_et_marne',
            ),
            489 => 
            array (
                'code_departement' => 77,
                'cp' => 77100,
                'id' => 1576,
                'insee' => 77276,
                'name' => 'MAREUIL LES MEAUX',
                'slug' => 'seine_et_marne',
            ),
            490 => 
            array (
                'code_departement' => 77,
                'cp' => 77610,
                'id' => 1577,
                'insee' => 77277,
                'name' => 'MARLES EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            491 => 
            array (
                'code_departement' => 77,
                'cp' => 77350,
                'id' => 1578,
                'insee' => 77285,
                'name' => 'LE MEE SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            492 => 
            array (
                'code_departement' => 77,
                'cp' => 77000,
                'id' => 1579,
                'insee' => 77288,
                'name' => 'MELUN',
                'slug' => 'seine_et_marne',
            ),
            493 => 
            array (
                'code_departement' => 77,
                'cp' => 77171,
                'id' => 1580,
                'insee' => 77289,
                'name' => 'MELZ SUR SEINE',
                'slug' => 'seine_et_marne',
            ),
            494 => 
            array (
                'code_departement' => 77,
                'cp' => 77520,
                'id' => 1581,
                'insee' => 77298,
                'name' => 'MONS EN MONTOIS',
                'slug' => 'seine_et_marne',
            ),
            495 => 
            array (
                'code_departement' => 77,
                'cp' => 77470,
                'id' => 1582,
                'insee' => 77300,
                'name' => 'MONTCEAUX LES MEAUX',
                'slug' => 'seine_et_marne',
            ),
            496 => 
            array (
                'code_departement' => 77,
                'cp' => 77151,
                'id' => 1583,
                'insee' => 77301,
                'name' => 'MONTCEAUX LES PROVINS',
                'slug' => 'seine_et_marne',
            ),
            497 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 1584,
                'insee' => 77304,
                'name' => 'MONTENILS',
                'slug' => 'seine_et_marne',
            ),
            498 => 
            array (
                'code_departement' => 77,
                'cp' => 77130,
                'id' => 1585,
                'insee' => 77305,
                'name' => 'MONTEREAU FAULT YONNE',
                'slug' => 'seine_et_marne',
            ),
            499 => 
            array (
                'code_departement' => 77,
                'cp' => 77230,
                'id' => 1586,
                'insee' => 77308,
                'name' => 'MONTGE EN GOELE',
                'slug' => 'seine_et_marne',
            ),
        ));
        \DB::table('ville')->insert(array (
            0 => 
            array (
                'code_departement' => 77,
                'cp' => 77610,
                'id' => 1589,
                'insee' => 77336,
                'name' => 'NEUFMOUTIERS EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            1 => 
            array (
                'code_departement' => 77,
                'cp' => 77123,
                'id' => 1590,
                'insee' => 77339,
                'name' => 'NOISY SUR ECOLE',
                'slug' => 'seine_et_marne',
            ),
            2 => 
            array (
                'code_departement' => 77,
                'cp' => 77440,
                'id' => 1591,
                'insee' => 77343,
                'name' => 'OCQUERRE',
                'slug' => 'seine_et_marne',
            ),
            3 => 
            array (
                'code_departement' => 77,
                'cp' => 77280,
                'id' => 1592,
                'insee' => 77349,
                'name' => 'OTHIS',
                'slug' => 'seine_et_marne',
            ),
            4 => 
            array (
                'code_departement' => 77,
                'cp' => 77330,
                'id' => 1593,
                'insee' => 77350,
                'name' => 'OZOIR LA FERRIERE',
                'slug' => 'seine_et_marne',
            ),
            5 => 
            array (
                'code_departement' => 77,
                'cp' => 77390,
                'id' => 1594,
                'insee' => 77352,
                'name' => 'OZOUER LE VOULGIS',
                'slug' => 'seine_et_marne',
            ),
            6 => 
            array (
                'code_departement' => 77,
                'cp' => 77131,
                'id' => 1595,
                'insee' => 77360,
                'name' => 'PEZARCHES',
                'slug' => 'seine_et_marne',
            ),
            7 => 
            array (
                'code_departement' => 77,
                'cp' => 77181,
                'id' => 1596,
                'insee' => 77363,
                'name' => 'LE PIN',
                'slug' => 'seine_et_marne',
            ),
            8 => 
            array (
                'code_departement' => 77,
                'cp' => 77540,
                'id' => 1597,
                'insee' => 77365,
                'name' => 'LE PLESSIS FEU AUSSOUX',
                'slug' => 'seine_et_marne',
            ),
            9 => 
            array (
                'code_departement' => 77,
                'cp' => 77160,
                'id' => 1598,
                'insee' => 77368,
                'name' => 'POIGNY',
                'slug' => 'seine_et_marne',
            ),
            10 => 
            array (
                'code_departement' => 77,
                'cp' => 77515,
                'id' => 1599,
                'insee' => 77371,
                'name' => 'POMMEUSE',
                'slug' => 'seine_et_marne',
            ),
            11 => 
            array (
                'code_departement' => 77,
                'cp' => 77410,
                'id' => 1601,
                'insee' => 77376,
                'name' => 'PRECY SUR MARNE',
                'slug' => 'seine_et_marne',
            ),
            12 => 
            array (
                'code_departement' => 77,
                'cp' => 77220,
                'id' => 1602,
                'insee' => 77377,
                'name' => 'PRESLES EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            13 => 
            array (
                'code_departement' => 77,
                'cp' => 77160,
                'id' => 1603,
                'insee' => 77379,
                'name' => 'PROVINS',
                'slug' => 'seine_et_marne',
            ),
            14 => 
            array (
                'code_departement' => 77,
                'cp' => 77139,
                'id' => 1604,
                'insee' => 77380,
                'name' => 'PUISIEUX',
                'slug' => 'seine_et_marne',
            ),
            15 => 
            array (
                'code_departement' => 77,
                'cp' => 77550,
                'id' => 1605,
                'insee' => 77384,
                'name' => 'REAU',
                'slug' => 'seine_et_marne',
            ),
            16 => 
            array (
                'code_departement' => 77,
                'cp' => 77160,
                'id' => 1606,
                'insee' => 77391,
                'name' => 'ROUILLY',
                'slug' => 'seine_et_marne',
            ),
            17 => 
            array (
                'code_departement' => 77,
                'cp' => 77760,
                'id' => 1607,
                'insee' => 77395,
                'name' => 'RUMONT',
                'slug' => 'seine_et_marne',
            ),
            18 => 
            array (
                'code_departement' => 77,
                'cp' => 77730,
                'id' => 1608,
                'insee' => 77397,
                'name' => 'SAACY SUR MARNE',
                'slug' => 'seine_et_marne',
            ),
            19 => 
            array (
                'code_departement' => 77,
                'cp' => 77260,
                'id' => 1609,
                'insee' => 77401,
                'name' => 'STE AULDE',
                'slug' => 'seine_et_marne',
            ),
            20 => 
            array (
                'code_departement' => 77,
                'cp' => 77650,
                'id' => 1610,
                'insee' => 77404,
                'name' => 'STE COLOMBE',
                'slug' => 'seine_et_marne',
            ),
            21 => 
            array (
                'code_departement' => 77,
                'cp' => 77750,
                'id' => 1611,
                'insee' => 77405,
                'name' => 'ST CYR SUR MORIN',
                'slug' => 'seine_et_marne',
            ),
            22 => 
            array (
                'code_departement' => 77,
                'cp' => 77950,
                'id' => 1612,
                'insee' => 77410,
                'name' => 'ST GERMAIN LAXIS',
                'slug' => 'seine_et_marne',
            ),
            23 => 
            array (
                'code_departement' => 77,
                'cp' => 77320,
                'id' => 1613,
                'insee' => 77423,
                'name' => 'ST MARTIN DES CHAMPS',
                'slug' => 'seine_et_marne',
            ),
            24 => 
            array (
                'code_departement' => 77,
                'cp' => 77630,
                'id' => 1614,
                'insee' => 77425,
                'name' => 'ST MARTIN EN BIERE',
                'slug' => 'seine_et_marne',
            ),
            25 => 
            array (
                'code_departement' => 77,
                'cp' => 77720,
                'id' => 1615,
                'insee' => 77428,
                'name' => 'ST OUEN EN BRIE',
                'slug' => 'seine_et_marne',
            ),
            26 => 
            array (
                'code_departement' => 77,
                'cp' => 77178,
                'id' => 1616,
                'insee' => 77430,
                'name' => 'ST PATHUS',
                'slug' => 'seine_et_marne',
            ),
            27 => 
            array (
                'code_departement' => 77,
                'cp' => 77140,
                'id' => 1617,
                'insee' => 77431,
                'name' => 'ST PIERRE LES NEMOURS',
                'slug' => 'seine_et_marne',
            ),
            28 => 
            array (
                'code_departement' => 77,
                'cp' => 77400,
                'id' => 1618,
                'insee' => 77438,
                'name' => 'ST THIBAULT DES VIGNES',
                'slug' => 'seine_et_marne',
            ),
            29 => 
            array (
                'code_departement' => 77,
                'cp' => 77148,
                'id' => 1619,
                'insee' => 77439,
                'name' => 'SALINS',
                'slug' => 'seine_et_marne',
            ),
            30 => 
            array (
                'code_departement' => 77,
                'cp' => 77115,
                'id' => 1620,
                'insee' => 77453,
                'name' => 'SIVRY COURTRY',
                'slug' => 'seine_et_marne',
            ),
            31 => 
            array (
                'code_departement' => 77,
                'cp' => 77163,
                'id' => 1621,
                'insee' => 77466,
                'name' => 'TIGEAUX',
                'slug' => 'seine_et_marne',
            ),
            32 => 
            array (
                'code_departement' => 77,
                'cp' => 77130,
                'id' => 1622,
                'insee' => 77467,
                'name' => 'LA TOMBE',
                'slug' => 'seine_et_marne',
            ),
            33 => 
            array (
                'code_departement' => 77,
                'cp' => 77710,
                'id' => 1623,
                'insee' => 77473,
                'name' => 'TREUZY LEVELAY',
                'slug' => 'seine_et_marne',
            ),
            34 => 
            array (
                'code_departement' => 77,
                'cp' => 77470,
                'id' => 1624,
                'insee' => 77475,
                'name' => 'TRILPORT',
                'slug' => 'seine_et_marne',
            ),
            35 => 
            array (
                'code_departement' => 77,
                'cp' => 77260,
                'id' => 1625,
                'insee' => 77478,
                'name' => 'USSY SUR MARNE',
                'slug' => 'seine_et_marne',
            ),
            36 => 
            array (
                'code_departement' => 77,
                'cp' => 77360,
                'id' => 1626,
                'insee' => 77479,
                'name' => 'VAIRES SUR MARNE',
                'slug' => 'seine_et_marne',
            ),
            37 => 
            array (
                'code_departement' => 77,
                'cp' => 77580,
                'id' => 1627,
                'insee' => 77484,
                'name' => 'VAUCOURTOIS',
                'slug' => 'seine_et_marne',
            ),
            38 => 
            array (
                'code_departement' => 77,
                'cp' => 77390,
                'id' => 1628,
                'insee' => 77493,
                'name' => 'VERNEUIL L ETANG',
                'slug' => 'seine_et_marne',
            ),
            39 => 
            array (
                'code_departement' => 77,
                'cp' => 77370,
                'id' => 1629,
                'insee' => 77496,
                'name' => 'VIEUX CHAMPAGNE',
                'slug' => 'seine_et_marne',
            ),
            40 => 
            array (
                'code_departement' => 77,
                'cp' => 77250,
                'id' => 1630,
                'insee' => 77501,
                'name' => 'VILLECERF',
                'slug' => 'seine_et_marne',
            ),
            41 => 
            array (
                'code_departement' => 77,
                'cp' => 77470,
                'id' => 1632,
                'insee' => 77505,
                'name' => 'VILLEMAREUIL',
                'slug' => 'seine_et_marne',
            ),
            42 => 
            array (
                'code_departement' => 77,
                'cp' => 77154,
                'id' => 1633,
                'insee' => 77509,
                'name' => 'VILLENEUVE LES BORDES',
                'slug' => 'seine_et_marne',
            ),
            43 => 
            array (
                'code_departement' => 77,
                'cp' => 77510,
                'id' => 1634,
                'insee' => 77512,
                'name' => 'VILLENEUVE SUR BELLOT',
                'slug' => 'seine_et_marne',
            ),
            44 => 
            array (
                'code_departement' => 77,
                'cp' => 77270,
                'id' => 1635,
                'insee' => 77514,
                'name' => 'VILLEPARISIS',
                'slug' => 'seine_et_marne',
            ),
            45 => 
            array (
                'code_departement' => 77,
                'cp' => 77580,
                'id' => 1636,
                'insee' => 77521,
                'name' => 'VILLIERS SUR MORIN',
                'slug' => 'seine_et_marne',
            ),
            46 => 
            array (
                'code_departement' => 77,
                'cp' => 77950,
                'id' => 1637,
                'insee' => 77528,
                'name' => 'VOISENON',
                'slug' => 'seine_et_marne',
            ),
            47 => 
            array (
                'code_departement' => 77,
                'cp' => 77160,
                'id' => 1638,
                'insee' => 77532,
                'name' => 'VULAINES LES PROVINS',
                'slug' => 'seine_et_marne',
            ),
            48 => 
            array (
                'code_departement' => 78,
                'cp' => 78113,
                'id' => 1639,
                'insee' => 78006,
                'name' => 'ADAINVILLE',
                'slug' => 'yvelines',
            ),
            49 => 
            array (
                'code_departement' => 78,
                'cp' => 78240,
                'id' => 1640,
                'insee' => 78007,
                'name' => 'AIGREMONT',
                'slug' => 'yvelines',
            ),
            50 => 
            array (
                'code_departement' => 78,
                'cp' => 78580,
                'id' => 1641,
                'insee' => 78010,
                'name' => 'LES ALLUETS LE ROI',
                'slug' => 'yvelines',
            ),
            51 => 
            array (
                'code_departement' => 78,
                'cp' => 78790,
                'id' => 1642,
                'insee' => 78020,
                'name' => 'ARNOUVILLE LES MANTES',
                'slug' => 'yvelines',
            ),
            52 => 
            array (
                'code_departement' => 78,
                'cp' => 78126,
                'id' => 1644,
                'insee' => 78033,
                'name' => 'AULNAY SUR MAULDRE',
                'slug' => 'yvelines',
            ),
            53 => 
            array (
                'code_departement' => 78,
                'cp' => 78770,
                'id' => 1645,
                'insee' => 78034,
                'name' => 'AUTEUIL',
                'slug' => 'yvelines',
            ),
            54 => 
            array (
                'code_departement' => 78,
                'cp' => 78580,
                'id' => 1646,
                'insee' => 78049,
                'name' => 'BAZEMONT',
                'slug' => 'yvelines',
            ),
            55 => 
            array (
                'code_departement' => 78,
                'cp' => 78660,
                'id' => 1647,
                'insee' => 78071,
                'name' => 'BOINVILLE LE GAILLARD',
                'slug' => 'yvelines',
            ),
            56 => 
            array (
                'code_departement' => 78,
                'cp' => 78910,
                'id' => 1648,
                'insee' => 78076,
                'name' => 'BOISSETS',
                'slug' => 'yvelines',
            ),
            57 => 
            array (
                'code_departement' => 78,
                'cp' => 78610,
                'id' => 1649,
                'insee' => 78108,
                'name' => 'LES BREVIAIRES',
                'slug' => 'yvelines',
            ),
            58 => 
            array (
                'code_departement' => 78,
                'cp' => 78830,
                'id' => 1650,
                'insee' => 78120,
                'name' => 'BULLION',
                'slug' => 'yvelines',
            ),
            59 => 
            array (
                'code_departement' => 78,
                'cp' => 78117,
                'id' => 1651,
                'insee' => 78143,
                'name' => 'CHATEAUFORT',
                'slug' => 'yvelines',
            ),
            60 => 
            array (
                'code_departement' => 78,
                'cp' => 78910,
                'id' => 1652,
                'insee' => 78163,
                'name' => 'CIVRY LA FORET',
                'slug' => 'yvelines',
            ),
            61 => 
            array (
                'code_departement' => 78,
                'cp' => 78113,
                'id' => 1653,
                'insee' => 78171,
                'name' => 'CONDE SUR VESGRE',
                'slug' => 'yvelines',
            ),
            62 => 
            array (
                'code_departement' => 78,
                'cp' => 78810,
                'id' => 1655,
                'insee' => 78196,
                'name' => 'DAVRON',
                'slug' => 'yvelines',
            ),
            63 => 
            array (
                'code_departement' => 78,
                'cp' => 78125,
                'id' => 1656,
                'insee' => 78209,
                'name' => 'EMANCE',
                'slug' => 'yvelines',
            ),
            64 => 
            array (
                'code_departement' => 78,
                'cp' => 78680,
                'id' => 1657,
                'insee' => 78217,
                'name' => 'EPONE',
                'slug' => 'yvelines',
            ),
            65 => 
            array (
                'code_departement' => 78,
                'cp' => 78620,
                'id' => 1659,
                'insee' => 78224,
                'name' => 'L ETANG LA VILLE',
                'slug' => 'yvelines',
            ),
            66 => 
            array (
                'code_departement' => 78,
                'cp' => 78410,
                'id' => 1660,
                'insee' => 78230,
                'name' => 'LA FALAISE',
                'slug' => 'yvelines',
            ),
            67 => 
            array (
                'code_departement' => 78,
                'cp' => 78810,
                'id' => 1661,
                'insee' => 78233,
                'name' => 'FEUCHEROLLES',
                'slug' => 'yvelines',
            ),
            68 => 
            array (
                'code_departement' => 78,
                'cp' => 78440,
                'id' => 1662,
                'insee' => 78246,
                'name' => 'FONTENAY ST PERE',
                'slug' => 'yvelines',
            ),
            69 => 
            array (
                'code_departement' => 78,
                'cp' => 78950,
                'id' => 1663,
                'insee' => 78263,
                'name' => 'GAMBAIS',
                'slug' => 'yvelines',
            ),
            70 => 
            array (
                'code_departement' => 78,
                'cp' => 78890,
                'id' => 1664,
                'insee' => 78265,
                'name' => 'GARANCIERES',
                'slug' => 'yvelines',
            ),
            71 => 
            array (
                'code_departement' => 78,
                'cp' => 78930,
                'id' => 1665,
                'insee' => 78281,
                'name' => 'GOUSSONVILLE',
                'slug' => 'yvelines',
            ),
            72 => 
            array (
                'code_departement' => 78,
                'cp' => 78440,
                'id' => 1666,
                'insee' => 78296,
                'name' => 'GUITRANCOURT',
                'slug' => 'yvelines',
            ),
            73 => 
            array (
                'code_departement' => 78,
                'cp' => 78250,
                'id' => 1668,
                'insee' => 78299,
                'name' => 'HARDRICOURT',
                'slug' => 'yvelines',
            ),
            74 => 
            array (
                'code_departement' => 78,
                'cp' => 78125,
                'id' => 1669,
                'insee' => 78307,
                'name' => 'HERMERAY',
                'slug' => 'yvelines',
            ),
            75 => 
            array (
                'code_departement' => 78,
                'cp' => 78440,
                'id' => 1670,
                'insee' => 78329,
                'name' => 'LAINVILLE EN VEXIN',
                'slug' => 'yvelines',
            ),
            76 => 
            array (
                'code_departement' => 78,
                'cp' => 78270,
                'id' => 1671,
                'insee' => 78337,
                'name' => 'LIMETZ VILLEZ',
                'slug' => 'yvelines',
            ),
            77 => 
            array (
                'code_departement' => 78,
                'cp' => 78600,
                'id' => 1672,
                'insee' => 78358,
                'name' => 'MAISONS LAFFITTE',
                'slug' => 'yvelines',
            ),
            78 => 
            array (
                'code_departement' => 78,
                'cp' => 78250,
                'id' => 1674,
                'insee' => 78401,
                'name' => 'MEULAN EN YVELINES',
                'slug' => 'yvelines',
            ),
            79 => 
            array (
                'code_departement' => 78,
                'cp' => 78970,
                'id' => 1675,
                'insee' => 78402,
                'name' => 'MEZIERES SUR SEINE',
                'slug' => 'yvelines',
            ),
            80 => 
            array (
                'code_departement' => 78,
                'cp' => 78940,
                'id' => 1676,
                'insee' => 78404,
                'name' => 'MILLEMONT',
                'slug' => 'yvelines',
            ),
            81 => 
            array (
                'code_departement' => 78,
                'cp' => 78124,
                'id' => 1677,
                'insee' => 78415,
                'name' => 'MONTAINVILLE',
                'slug' => 'yvelines',
            ),
            82 => 
            array (
                'code_departement' => 78,
                'cp' => 78130,
                'id' => 1678,
                'insee' => 78440,
                'name' => 'LES MUREAUX',
                'slug' => 'yvelines',
            ),
            83 => 
            array (
                'code_departement' => 78,
                'cp' => 78660,
                'id' => 1679,
                'insee' => 78472,
                'name' => 'ORSONVILLE',
                'slug' => 'yvelines',
            ),
            84 => 
            array (
                'code_departement' => 78,
                'cp' => 78230,
                'id' => 1680,
                'insee' => 78481,
                'name' => 'LE PECQ',
                'slug' => 'yvelines',
            ),
            85 => 
            array (
                'code_departement' => 78,
                'cp' => 78125,
                'id' => 1681,
                'insee' => 78497,
                'name' => 'POIGNY LA FORET',
                'slug' => 'yvelines',
            ),
            86 => 
            array (
                'code_departement' => 78,
                'cp' => 78120,
                'id' => 1683,
                'insee' => 78517,
                'name' => 'RAMBOUILLET',
                'slug' => 'yvelines',
            ),
            87 => 
            array (
                'code_departement' => 78,
                'cp' => 78270,
                'id' => 1684,
                'insee' => 78528,
                'name' => 'ROLLEBOISE',
                'slug' => 'yvelines',
            ),
            88 => 
            array (
                'code_departement' => 78,
                'cp' => 78720,
                'id' => 1685,
                'insee' => 78548,
                'name' => 'ST FORGET',
                'slug' => 'yvelines',
            ),
            89 => 
            array (
                'code_departement' => 78,
                'cp' => 78640,
                'id' => 1686,
                'insee' => 78550,
                'name' => 'ST GERMAIN DE LA GRANGE',
                'slug' => 'yvelines',
            ),
            90 => 
            array (
                'code_departement' => 78,
                'cp' => 78730,
                'id' => 1687,
                'insee' => 78569,
                'name' => 'STE MESME',
                'slug' => 'yvelines',
            ),
            91 => 
            array (
                'code_departement' => 78,
                'cp' => 78720,
                'id' => 1688,
                'insee' => 78590,
                'name' => 'SENLISSE',
                'slug' => 'yvelines',
            ),
            92 => 
            array (
                'code_departement' => 78,
                'cp' => 78113,
                'id' => 1689,
                'insee' => 78606,
                'name' => 'LE TARTRE GAUDRAN',
                'slug' => 'yvelines',
            ),
            93 => 
            array (
                'code_departement' => 78,
                'cp' => 78250,
                'id' => 1690,
                'insee' => 78609,
                'name' => 'TESSANCOURT SUR AUBETTE',
                'slug' => 'yvelines',
            ),
            94 => 
            array (
                'code_departement' => 78,
                'cp' => 78117,
                'id' => 1691,
                'insee' => 78620,
                'name' => 'TOUSSUS LE NOBLE',
                'slug' => 'yvelines',
            ),
            95 => 
            array (
                'code_departement' => 78,
                'cp' => 78140,
                'id' => 1692,
                'insee' => 78640,
                'name' => 'VELIZY VILLACOUBLAY',
                'slug' => 'yvelines',
            ),
            96 => 
            array (
                'code_departement' => 78,
                'cp' => 78490,
                'id' => 1693,
                'insee' => 78653,
                'name' => 'VICQ',
                'slug' => 'yvelines',
            ),
            97 => 
            array (
                'code_departement' => 78,
                'cp' => 78640,
                'id' => 1694,
                'insee' => 78683,
                'name' => 'VILLIERS ST FREDERIC',
                'slug' => 'yvelines',
            ),
        ));
        
        
    }
}