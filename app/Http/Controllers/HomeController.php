<?php

namespace App\Http\Controllers;

use App\Etat;
use App\Ville;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(){
        $villes= Ville::orderBy('insee', 'asc')->get();

        $annonces=DB::table('annonces')->join('categories', 'annonces.categorie_id', '=', 'categories.id')
                                        ->join('etat', 'annonces.etat_id', '=', 'etat.id')
                                        ->join('ville', 'annonces.ville_insee', '=', 'ville.insee')
                                        ->select('annonces.*', 'categories.name AS categorie_name', 'categories.slug', 'etat.name AS etat_name', 'ville.name AS ville_name')
                                        ->orderBy('created_at', 'desc')
                                        ->limit(6)
                                        ->get();

        return view('home')->with('villes', $villes)
                            ->with('annonces', $annonces);
    }
}
