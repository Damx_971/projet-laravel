<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAnnoncesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titre' => 'required|min:3',
            'categorie_id' => 'required',
            'etat_id' => 'required',
            'description' => 'required|min:10',
            'prix' => 'required',
            'photo' => 'required|image',
            'ville_insee' => 'required',
        ];
    }
}
