<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Annonce extends Model
{
    protected $table = "annonces";
    protected $fillable = ['titre', 'categorie_id', 'etat_id', 'description', 'prix', 'photo', 'ville_insee', 'user_id'];
    public $timestamps = true;

    public function categorie()
    {
        return $this->hasOne('App\Category', 'id', 'categorie_id');
    }

    public function etat()
    {
        return $this->hasOne('App\Etat', 'id', 'etat_id');
    }

    public function ville()
    {
        return $this->hasOne('App\Ville', 'insee', 'ville_insee');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
