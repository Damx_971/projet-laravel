<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnonces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annonces', function (Blueprint $table) {
            $table->id();
            $table->string('titre');
            $table->unsignedBigInteger('categorie_id')->nullable();
            $table->foreign('categorie_id')
                ->references('id')
                ->on('categories')
                ->onDelete('SET NULL');
            $table->unsignedBigInteger('etat_id')->nullable();
            $table->foreign('etat_id')
                ->references('id')
                ->on('etat')
                ->onDelete('SET NULL');
            $table->text('description');
            $table->float('prix');
            $table->string('photo')->nullable();;
            $table->unsignedBigInteger('ville_insee')->nullable();
            $table->foreign('ville_insee')
                ->references('insee')
                ->on('ville')
                ->onDelete('SET NULL');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annonces');
    }
}
