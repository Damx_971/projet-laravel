<?php

use Illuminate\Database\Seeder;

class AnnoncesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('annonces')->delete();
        
        \DB::table('annonces')->insert(array (
            0 => 
            array (
                'categorie_id' => 1,
                'created_at' => '2020-10-20 16:30:05',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.',
                'etat_id' => 2,
                'id' => 1,
                'photo' => 'photo/Uxi4lDFZhpYKgyS4cmwPhSAX9NupMwBWg9ICWcAD.jpeg',
                'prix' => 12.0,
                'titre' => 'Annonce 1',
                'updated_at' => '2020-10-20 16:30:05',
                'user_id' => 1,
                'ville_insee' => 9004,
            ),
            1 => 
            array (
                'categorie_id' => 2,
                'created_at' => '2020-10-20 16:30:48',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.',
                'etat_id' => 3,
                'id' => 2,
                'photo' => 'photo/HQ26tJY2zd1MKN76FF9Q2BwiXPqZxeI6dfAJA9cp.jpeg',
                'prix' => 24.0,
                'titre' => 'Annonce 2',
                'updated_at' => '2020-10-20 16:30:48',
                'user_id' => 1,
                'ville_insee' => 75103,
            ),
            2 => 
            array (
                'categorie_id' => 4,
                'created_at' => '2020-10-20 16:32:05',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.',
                'etat_id' => 3,
                'id' => 3,
                'photo' => 'photo/MYogio4W2gfkcbRVG7F3QrvE585MHg2SExivDzW5.jpeg',
                'prix' => 44.0,
                'titre' => 'Annonce 3',
                'updated_at' => '2020-10-20 16:32:05',
                'user_id' => 1,
                'ville_insee' => 92040,
            ),
            3 => 
            array (
                'categorie_id' => 1,
                'created_at' => '2020-10-20 16:32:47',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.',
                'etat_id' => 3,
                'id' => 4,
                'photo' => 'photo/yacsAU6eQLv9w2IIoU8ADkfMa3uIPXkEVyBdxaCD.jpeg',
                'prix' => 44.0,
                'titre' => 'Annonce 4',
                'updated_at' => '2020-10-20 16:32:47',
                'user_id' => 1,
                'ville_insee' => 78490,
            ),
        ));
        
        
    }
}