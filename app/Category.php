<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";
    protected $fillable = ['id', 'name', 'slug'];
    public $timestamps = false;

    public function annonce()
    {
        return $this->hasMany('App\Annonce', 'categorie_id', 'id');
    }

}
