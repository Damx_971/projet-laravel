@extends('layouts.base')

@section('title') Bienvenue sur NetShop @endsection

@section('content')

<!-- dernieres annonces -->
<h6>Dernières annonces :</h6>
<div class="row">
@if (count($annonces) === 0)
  <p style="text-align: center; width: -webkit-fill-available">Aucune résulat !</p>
@else
  @foreach($annonces as $annonce)
    <div class="col-sm-6">
      <a  href="{{route('detailsAnnonce', ['slug' => $annonce->slug, 'id' => $annonce->id])}}">
      <div class="card">
        <img class="card-img-top" src="{{asset('storage/'.$annonce->photo)}}" alt="{{$annonce->titre}}">
        <div class="card-body">
          <h5 class="card-title">{{$annonce->titre}}</h5>
          <h6 class="card-title">{{$annonce->categorie_name}}</h6>
          <h6 class="card-title">{{$annonce->prix}} €</h6>

        </div>
      </div>
      </a>
    </div>
  @endforeach
@endif
</div>

<!-- carte idf -->
<div id="text-box-1746203881" class="text-box banner-layer x50 md-x50 lg-x50 y85 md-y85 lg-y85 res-text">
  <div class="text dark">
    <div class="text-inner text-center">
      <div class="imgmap-frontend-image">
        <div id="mapster_wrap_0" style="display: block; position: relative; padding: 0px; width: 330px; height: 330px;">
          <img class="mapster_el" src="{{asset('map.svg')}}" style="width: 330px; height: 330px; opacity: 1;">
          <canvas width="330" height="330" class="mapster_el" style="position: absolute; left: 0px; top: 0px; padding: 0px; border: 0px;"></canvas>
          <canvas width="330" height="330" class="mapster_el" style="position: absolute; left: 0px; top: 0px; padding: 0px; border: 0px; opacity: 0.05;"></canvas>
          <img src="{{asset('map.svg')}}" usemap="#imgmap-1284-1" id="imagemap-1284-1" style="border: 0px; position: absolute; left: 0px; top: 0px; padding: 0px; opacity: 0;">
      </div>
        <map name="imgmap-1284-1">
          <area href="{{route('search_departement', 'val_de_marne')}}" data-toggle="tooltip" data-placement="top" title="Val-de-Marne" container='body' data-fill-color="fefefe" data-fill-opacity="0.3" data-stroke-color="fefefe" data-stroke-opacity="0.8" data-stroke-width="2" data-mapkey="area-1292" shape="poly" coords="136,134,139,134,145,134,147,136,154,134,158,128,169,134,169,141,167,143,167,152,158,141,150,145,136,143">
          <area href="{{route('search_departement', 'seine_saint_denis')}}" data-toggle="tooltip" data-placement="top" title="Seine-Saint-Denis" data-fill-color="fefefe" data-fill-opacity="0.3" data-stroke-color="fefefe" data-stroke-opacity="0.8" data-stroke-width="2" data-mapkey="area-1291" shape="poly" coords="136,103,136,108,147,112,150,119,156,119,167,125,165,110,169,108,167,101,163,95,150,103">
          <area href="{{route('search_departement', 'paris')}}" data-toggle="tooltip" data-placement="top" title="Paris" data-fill-color="fefefe" data-fill-opacity="0.3" data-stroke-color="fefefe" data-stroke-opacity="0.8" data-stroke-width="2" data-mapkey="area-1290" shape="poly" coords="128,119,136,112,141,114,145,123,150,125,150,123,152,123,152,128,141,130,136,128,130,125">
          <area href="{{route('search_departement', 'hauts_de_seine')}}" data-toggle="tooltip" data-placement="top" title="Hauts-de-Seine" data-fill-color="fefefe" data-fill-opacity="0.3" data-stroke-color="fefefe" data-stroke-opacity="0.8" data-stroke-width="2" data-mapkey="area-1289" shape="poly" coords="130,103,132,110,121,121,130,130,132,134,130,141,117,130,112,119,123,110,130,103">
          <area href="{{route('search_departement', 'yvelines')}}" data-toggle="tooltip" data-placement="top" title="Yvelines" data-fill-color="fefefe" data-fill-opacity="0.3" data-stroke-color="fefefe" data-stroke-opacity="0.8" data-stroke-width="2" data-mapkey="area-1288" shape="poly" coords="114,136,108,128,106,114,117,106,108,92,88,95,70,86,55,86,40,79,22,84,37,136,46,143,42,154,64,178,64,191,77,198,79,187,75,178,88,178,95,167,88,158">
          <area href="{{route('search_departement', 'essonne')}}" data-toggle="tooltip" data-placement="top" title="Essonne" data-fill-color="fefefe" data-fill-opacity="0.3" data-stroke-color="fefefe" data-stroke-opacity="0.8" data-stroke-width="2" data-mapkey="area-1287" shape="poly" coords="123,224,125,229,145,229,158,211,156,187,165,161,156,150,150,154,136,152,121,143,101,163,106,165,95,183,86,185,90,191,84,205,86,211,90,216,88,231,99,235">
          <area href="{{route('search_departement', 'seine_et_marne')}}" data-toggle="tooltip" data-placement="top" title="Seine-et-Marne" data-fill-color="fefefe" data-fill-opacity="0.3" data-stroke-color="fefefe" data-stroke-opacity="0.8" data-stroke-width="2" data-mapkey="area-1286" shape="poly" coords="169,81,185,75,231,77,240,73,246,75,246,86,290,121,282,141,288,150,288,158,297,172,282,209,233,218,229,227,229,242,205,264,161,266,167,253,154,244,154,231,174,216,163,187,180,141,172,119,178,101,169,90">
          <area href="{{route('search_departement', 'val_doise')}}" data-toggle="tooltip" data-placement="top" title="Val-d'Oise" data-fill-color="fefefe" data-fill-opacity="0.3" data-stroke-color="fefefe" data-stroke-opacity="0.8" data-stroke-width="2" data-mapkey="area-1285" shape="poly" coords="55,51,57,57,90,62,101,53,112,59,158,70,163,77,163,86,150,95,132,95,123,101,106,86,90,86,70,77,55,79,44,73">
        </map>
      </div>
    </div>
  </div>

  <!-- Liste des villes -->
  <div class="form-group col-md-9">
  <form id="form_ville" action="{{route('search')}}" method="get">
    <select class="form-control" id="exampleFormControlSelect1" name="ville" onChange="document.getElementById('form_ville').submit();">
      <option disabled selected hidden>Ville</option>
      @foreach($villes as $ville)
      {
        <option value="{{$ville->insee}}">{{$ville->name}} ({{$ville->cp}})</option>
      }
      @endforeach
    </select>
  </div>
  </form>
</div>

@endsection
