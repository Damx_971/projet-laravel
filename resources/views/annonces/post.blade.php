@extends('layouts.base')

@section('title') ajouter une annonce @endsection

@section('content')
<form action="{{route('storeAnnonce')}}" method="POST" enctype="multipart/form-data">
  @csrf
    @if(session()->has('success'))
      <div class="alert alert-success" role="alert">
          {{ session()->get('success') }}
      </div>
    @endif
    <br>
  <div class="form-group">
    <label for="titre">Titre :</label>
    <input type="text" name="titre" class="form-control" id="titre" required>
      @error('titre')
        <span class="alert alert-danger" role="alert">
            {{ $message }}
        </span>
      @enderror
  </div>

  <div class="form-group">
    <label for="categorie_id">Catégorie :</label>
    <select name="categorie_id" class="form-control" id="categorie_id" required>
      <option value="" disabled selected hidden></option>
      @foreach($categories as $category)
      {
        <option value="{{$category->id}}">{{$category->name}}</option>
      }
      @endforeach
    </select>
      @error('categorie_id')
        <span class="alert alert-danger" role="alert">
            {{ $message }}
        </span>
      @enderror
  </div>

  <div class="form-group">
    <label for="etat_id">Etat :</label>
    <select name="etat_id" class="form-control" id="etat_id" required>
      <option value="" disabled selected hidden></option>
      @foreach($etats as $etat)
      {
        <option value="{{$etat->id}}">{{$etat->name}}</option>
      }
      @endforeach
    </select>
      @error('etat_id')
        <span class="alert alert-danger" role="alert">
            {{ $message }}
        </span>
      @enderror
  </div>

  <div class="form-group">
    <label for="description">Description :</label>
    <textarea name="description" class="form-control" id="description" rows="3" required></textarea>
      @error('description')
        <span class="alert alert-danger" role="alert">
            {{ $message }}
        </span>
      @enderror
  </div>

  <div class="form-group">
    <label for="prix">Prix :</label>
    <input type="number" name="prix" class="form-control" id="prix" required>
      @error('prix')
        <span class="alert alert-danger" role="alert">
            {{ $message }}
        </span>
      @enderror
  </div>

  <div class="form-group">
    <label for="photo">Ajouter une photo :</label>
    <input type="file" name="photo"  accept="image/*" class="form-control-file" id="photo" required>
      @error('photo')
        <span class="alert alert-danger" role="alert">
            {{ $message }}
        </span>
      @enderror
  </div>

  <div class="form-group">
    <label for="ville_insee">Ville :</label>
    <select name="ville_insee" class="form-control" id="ville_insee" required>
      <option value="" disabled selected hidden></option>
      @foreach($villes as $ville)
      {
        <option value="{{$ville->insee}}">{{$ville->name}} ({{$ville->cp}})</option>
      }
      @endforeach
    </select>
      @error('ville_insee')
        <span class="alert alert-danger" role="alert">
            {{ $message }}
        </span>
      @enderror
  </div>

 <div class="text-center"> 
    <button type="submit" class="btn btn-primary">Déposer l'annonce</button>
 </div>
</form>


@endsection
