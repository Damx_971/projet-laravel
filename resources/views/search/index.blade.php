@extends('layouts.base')

@section('title') ajouter une annonce @endsection

@section('content')
<!-- Formulaire de recherche -->
<form action="{{route('search')}}" method="get">
  @csrf
  <div class="form-row">
    <div class="form-group col-md-7">
      <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Que recherchez-vous ?" name="search">
    </div>
    <div class="form-group col-md-5">
          <select class="form-control" id="exampleFormControlSelect1" name="categorie">
              <option disabled selected hidden>Catégorie</option>
              @foreach($categories as $category)
              {
                <option value="{{$category->id}}">{{$category->name}}</option>
              }
              @endforeach
          </select>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-3">
        <select class="form-control" id="exampleFormControlSelect1" name="etat">
        <option disabled selected hidden>Etat</option>
        @foreach($etats as $etat)
        {
          <option value="{{$etat->id}}">{{$etat->name}}</option>
        }
        @endforeach
        </select>
    </div>

    <div class="form-group col-md-2">
        <input type="number" class="form-control" id="exampleFormControlInput1" placeholder="Prix min" name="prix_min">
    </div>
    <div class="form-group col-md-2">
        <input type="number" class="form-control" id="exampleFormControlInput1" placeholder="Prix max" name="prix_max">
    </div>

    <div class="form-group col-md-5">
        <select class="form-control" id="exampleFormControlSelect1" name="ville">
            <option disabled selected hidden>Ville</option>
            @foreach($villes as $ville)
            {
              <option value="{{$ville->insee}}">{{$ville->name}} ({{$ville->cp}})</option>
            }
            @endforeach
    </select>
        </select>
    </div>

  </div>

  <div class="text-center"> 
    <button type="submit" class="btn btn-primary">Rechercher</button>
  </div>
</form>
          </br>

<!-- Résultat de la recherche -->
<div class="row">
@if (count($annonces) === 0)
  <p style="text-align: center; width: -webkit-fill-available">Aucune résulat !</p>
@else
  @foreach($annonces as $annonce)
    <div class="col-sm-6 mb-3">
      <div class="card">
      <a  href="{{route('detailsAnnonce', ['slug' => $annonce->slug, 'id' => $annonce->id])}}">
        <div class="card-body">
        <img class="card-img-top mb-3" src="{{asset('storage/'.$annonce->photo)}}">
          <h5 class="card-title text-center">{{$annonce->titre}}</h5>
          <h6 class="card-title ">Catégorie : {{$annonce->categorie_name}}</h6>
          <h6 class="card-title ">Etat : {{$annonce->etat_name}}</h6>
          <h6 class="card-title ">Ville : {{$annonce->ville_name}}</h6>
          <h5 class="card-title text-center ">{{$annonce->prix}} €</h5>
        </div>
      </a>
      </div>
    </div>
  @endforeach
@endif
</div>

@endsection
